package br.com.configurations.error;

import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
public class ExceptionAdvice {

    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ResponseBody
    public ResponseEntity<br.com.configurations.error.ErrorResponse> handleException(Exception exception) {
        exception.printStackTrace();

        ResponseStatus statusAnnotation = AnnotationUtils.findAnnotation(exception.getClass(), ResponseStatus.class);
        HttpStatus code = statusAnnotation != null ? statusAnnotation.value() : HttpStatus.INTERNAL_SERVER_ERROR;

        String message = exception.getLocalizedMessage();
        if (message == null) { message = ""; }

        switch (exception.getClass().toString()) {
            case "ImgDAO": {

                break;
            }
        }

        DocUrl urlAnnotation = AnnotationUtils.findAnnotation(exception.getClass(), DocUrl.class);
        String url = urlAnnotation != null ? urlAnnotation.value() : "";

        return new ResponseEntity<>(new br.com.configurations.error.ErrorResponse(code.value(), message, url), null, code);
    }

//    @ExceptionHandler(Exception.class)
//    public String handleAllException(){
//        return "index";
//    }

}
