package br.com.configurations.error;

public class ErrorResponse {
    public int code;
    public String message;
    public String url;

    public ErrorResponse(int code, String message, String url) {
        this.code = code;
        this.message = message;
        this.url = url;
    }
}
