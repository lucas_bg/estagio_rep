package br.com.configurations;

import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class AutorizadorInterceptor extends HandlerInterceptorAdapter {

    private static final Logger log = Logger.getLogger("AutorizadorInterceptor");

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response,
                             Object controller) throws Exception {

        String userAgent = request.getHeader("User-Agent");
        if (userAgent != null) {
            if (userAgent.contains("Android")
                    || userAgent.contains("android")
                    || userAgent.contains("ANDROID")) {
                String uri = request.getRequestURI();
                if (uri.contains("instituicao/getImg")) {
                    return true;
                }
            }
        }

        String uri = request.getRequestURI();
        if (uri.endsWith("index") ||
                uri.equals("/") ||
                uri.endsWith("cadastrar") ||
                uri.endsWith("loginn") ||
                uri.endsWith("privacidadeETermos") ||
                uri.endsWith("sobre") ||
                uri.endsWith("contato") ||
                uri.contains("resources")) {
            return true;
        }

        if (request.getSession().getAttribute("usuarioAtivo") != null) {
            return true;
        }

        response.sendRedirect("index");
        return false;
    }

}