package br.com.configurations;

import com.google.appengine.api.images.ImagesService;
import com.google.appengine.api.memcache.AsyncMemcacheService;
import com.google.appengine.api.memcache.MemcacheService;
import com.google.appengine.tools.cloudstorage.GcsService;
import com.google.appengine.tools.cloudstorage.GcsServiceFactory;
import com.google.appengine.tools.cloudstorage.RetryParams;
import com.googlecode.spring.appengine.api.factory.AsyncMemcacheServiceFactoryBean;
import com.googlecode.spring.appengine.api.factory.ImagesServiceFactoryBean;
import com.googlecode.spring.appengine.api.factory.MemcacheServiceFactoryBean;
import com.googlecode.spring.appengine.cache.memcache.MemcacheCache;

import org.gmr.web.multipart.GMultipartResolver;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.boot.autoconfigure.web.DispatcherServletAutoConfiguration;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.ByteArrayHttpMessageConverter;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.multipart.MultipartResolver;
import org.springframework.web.servlet.DispatcherServlet;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

import java.util.ArrayList;
import java.util.List;

import br.com.configurations.cache.MemcacheCacheManager;

@Configuration
@ComponentScan(basePackages = "br.com")
@EnableAutoConfiguration(exclude = {HibernateJpaAutoConfiguration.class, DataSourceAutoConfiguration.class})
@EnableCaching
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @Bean(name = DispatcherServletAutoConfiguration.DEFAULT_DISPATCHER_SERVLET_BEAN_NAME)
    public DispatcherServlet dispatcherServlet() {
        DispatcherServlet dispatcherServlet = new DispatcherServlet();
        dispatcherServlet.setPublishEvents(false);

        return dispatcherServlet;
    }

    public void configureMessageConverters(List<HttpMessageConverter<?>> converters) {
        converters.add(byteArrayHttpMessageConverter());
    }

    @Bean
    public BCryptPasswordEncoder bCryptPasswordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    public ByteArrayHttpMessageConverter byteArrayHttpMessageConverter() {
        ByteArrayHttpMessageConverter arrayHttpMessageConverter = new ByteArrayHttpMessageConverter();
        arrayHttpMessageConverter.setSupportedMediaTypes(getSupportedMediaTypes());
        return arrayHttpMessageConverter;
    }

    private List<MediaType> getSupportedMediaTypes() {
        List<MediaType> list = new ArrayList<>();
        list.add(MediaType.IMAGE_JPEG);
        list.add(MediaType.IMAGE_PNG);
        list.add(MediaType.APPLICATION_OCTET_STREAM);
        return list;
    }

//    //https://code.google.com/p/gmultipart/
//    @Bean(name = "multipartResolver")
//    public GMultipartResolver multipartResolver() {
//        GMultipartResolver resolver = new GMultipartResolver();
//        resolver.setMaxUploadSize(100000);
//
//        return resolver;
//    }

    @Bean(name = "cacheManager")
    public CacheManager cacheManager() {
        return new MemcacheCacheManager();
    }

    @Bean(name = "applicationContextProvder")
    public ApplicationContextProvider applicationContextProvder() {
        return new ApplicationContextProvider();
    }

    @Bean(name = "cache")
    public Cache cache() throws Exception {
        return new MemcacheCache(memcacheService());
    }

    @Bean(name = "memcacheService")
    public MemcacheService memcacheService() throws Exception {
        return memcacheServiceFactoryBean().getObject();
    }

    @Bean(name = "memcacheServiceFactory")
    public MemcacheServiceFactoryBean memcacheServiceFactoryBean() {
        return new MemcacheServiceFactoryBean();
    }

    @Bean(name = "asyncMemcacheService")
    public AsyncMemcacheService asyncMemcacheService() throws Exception {
        return asyncMemcacheServiceFactoryBean().getObject();
    }

    @Bean(name = "asyncMemcacheServiceFactoryBean")
    public AsyncMemcacheServiceFactoryBean asyncMemcacheServiceFactoryBean() {
        return new AsyncMemcacheServiceFactoryBean();
    }

    @Bean(name = "gcsService")
    public GcsService gcsService() {
        return GcsServiceFactory.createGcsService(RetryParams.getDefaultInstance());
    }

    @Bean(name = "imagesService")
    public ImagesService imagesService() throws Exception {
        return imagesServiceFactoryBean().getObject();
    }

    @Bean(name = "imagesServiceFactoryBean")
    public ImagesServiceFactoryBean imagesServiceFactoryBean() {
        return new ImagesServiceFactoryBean();
    }

    @Bean(name = "internalResourceViewResolver")
    public InternalResourceViewResolver internalResourceViewResolver() {
        InternalResourceViewResolver resolver = new InternalResourceViewResolver();
        resolver.setPrefix("/WEB-INF/views/");
        resolver.setSuffix(".jsp");
        return resolver;
    }

    @Bean(name = "multipartResolver")
    public MultipartResolver multipartResolver() {
        return new GMultipartResolver();
    }

}
