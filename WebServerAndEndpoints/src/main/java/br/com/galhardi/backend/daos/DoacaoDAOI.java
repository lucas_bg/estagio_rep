package br.com.galhardi.backend.daos;

import java.util.List;

import br.com.galhardi.backend.models.Doacao;

public interface DoacaoDAOI extends DAO<Doacao> {

    void realizarD(Integer id);

    List<Doacao> doacoesRealizadasPorUsuarioD(Integer id);

}
