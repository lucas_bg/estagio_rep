package br.com.galhardi.backend.endpoints;

import com.google.api.server.spi.config.Api;
import com.google.api.server.spi.config.ApiMethod;
import com.google.api.server.spi.config.ApiNamespace;
import com.google.api.server.spi.config.Named;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.util.List;
import java.util.logging.Logger;

import br.com.configurations.ApplicationContextProvider;
import br.com.galhardi.backend.models.Response;
import br.com.galhardi.backend.models.Voluntario;
import br.com.galhardi.backend.services.VoluntarioManager;
import br.com.galhardi.backend.services.VoluntarioManagerI;

@Api(name = "voluntarioEndpoint", version = "v1",
        namespace = @ApiNamespace(ownerDomain = "api.voluntarios.galhardi.com.br",
                ownerName = "api.voluntarios.galhardi.com.br",
                packagePath = ""))
public class VoluntarioEndpoint {

    private static final Logger log = Logger.getLogger("VoluntarioEndpoint");

    @Qualifier(value = "VoluntarioManagerS")
    VoluntarioManagerI voluntarioMNG;

    private BCryptPasswordEncoder bCryptPasswordEncoder;

    public VoluntarioEndpoint() {
        ApplicationContextProvider applicationContextProvider = new ApplicationContextProvider();
        voluntarioMNG = applicationContextProvider.getApplicationContext().getBean(VoluntarioManager.class);
        bCryptPasswordEncoder = applicationContextProvider.getApplicationContext().getBean(BCryptPasswordEncoder.class);
    }

    @ApiMethod(name = "insertVoluntario")
    public void createE(Voluntario obj) {
        try {
            voluntarioMNG.create(obj);
        } catch (Exception e) {
            log.severe("Exception no insertVoluntario");
        }
    }

    @ApiMethod(name = "getVoluntario")
    public Voluntario readE(@Named("id") Integer id) {
        try {
            return voluntarioMNG.read(id);
        } catch (Exception e) {
            log.severe("Exception no getVoluntario");
            return null;
        }
    }

    @ApiMethod(name = "updateVoluntario")
    public Response updateE(Voluntario obj) {
        Response response = new Response();
        try {
            response.setBooleanResp(voluntarioMNG.update(obj));
        } catch (Exception e) {
            log.severe("Exception no updateVoluntario");
            response.setBooleanResp(false);
            response.setOriginalException(e.getMessage());
            response.setErrorStrMsg("Exception no updateVoluntario");
        }
        return response;
    }

    @ApiMethod(name = "removeVoluntario")
    public void deleteE(@Named("id") Integer id) {
        try {
            voluntarioMNG.delete(id);
        } catch (Exception e) {
            log.severe("Exception no removeVoluntario");
        }
    }

    @ApiMethod(name = "listVoluntario")
    public List<Voluntario> listE() {
        try {
            return voluntarioMNG.list();
        } catch (Exception e) {
            log.severe("Exception no listVoluntario");
            return null;
        }
    }

    @ApiMethod(name = "authenticateVoluntario")
    public Voluntario authenticateVoluntario(@Named("email") String email, @Named("senha") String senha) {
        try {
            Voluntario user = voluntarioMNG.readUserByEmail(email);
            if (user == null) {
                return null;
            }
            if (bCryptPasswordEncoder.matches(senha, user.getSenha())) {
                return user;
            } else {
                return null;
            }
        } catch (Exception e) {
            log.severe("Exception no authenticateVoluntario");
            return null;
        }
    }

    @ApiMethod(name = "setGoogleId")
    public Response setGoogleId(@Named("email") String email, @Named("gId") String gId) {
        Response response = new Response();
        try {
            response.setBooleanResp(voluntarioMNG.setGoogleId(email, gId));
        } catch (Exception e) {
            log.severe("Exception no setGoogleId");
            response.setBooleanResp(false);
            response.setOriginalException(e.getMessage());
            response.setErrorStrMsg("Exception no setGoogleId");
        }
        return response;
    }

    @ApiMethod(name = "insertNewVolunteer")
    public Response insertNewVolunteer(Voluntario obj) {
        Response response = new Response();
        try {
            response.setBooleanResp(voluntarioMNG.insertNewVolunteer(obj));
        } catch (Exception e) {
            log.severe("Exception no insertNewVolunteer");
            response.setBooleanResp(false);
            response.setOriginalException(e.getMessage());
            response.setErrorStrMsg("Exception no insertNewVolunteer");
        }
        return response;
    }

    @ApiMethod(name = "deleteUserByEmail")
    public Response deleteUserByEmail(@Named("email") String email, @Named("senha") String senha) {
        Response response = new Response();
        try {
            Voluntario user = voluntarioMNG.readUserByEmail(email);
            if (user != null) {
                if (bCryptPasswordEncoder.matches(senha, user.getSenha())) {
                    response.setBooleanResp(voluntarioMNG.deleteUserByEmail(email, senha, user.getId()));
                }
            }
        } catch (Exception e) {
            log.severe("Exception no deleteUserByEmail");
            response.setBooleanResp(false);
            response.setOriginalException(e.getMessage());
            response.setErrorStrMsg("Exception no deleteUserByEmail");
        }
        return response;
    }

    @ApiMethod(name = "readUserbyEmail")
    public Voluntario readUserByEmailE(@Named("email") String email) {
        try {
            return voluntarioMNG.readUserByEmail(email);
        } catch (Exception e) {
            log.severe("Exception no readUserbyEmail");
            return null;
        }
    }

}
