package br.com.galhardi.backend.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import br.com.galhardi.backend.models.Doacao;
import br.com.galhardi.backend.models.Enums.TipoDoacao;
import br.com.galhardi.backend.models.Instituicao;
import br.com.galhardi.backend.models.Voluntario;
import br.com.galhardi.backend.services.AtividadeManagerI;
import br.com.galhardi.backend.services.DoacaoManagerI;
import br.com.galhardi.backend.services.Manager;
import br.com.galhardi.backend.services.VoluntarioManagerI;

@Controller
@RequestMapping("/perfil")
public class PerfilController {

    private static final Logger log = Logger.getLogger("HomeController");

    @Autowired
    @Qualifier(value = "VoluntarioManagerS")
    VoluntarioManagerI voluntarioMNG;

    @Autowired
    @Qualifier(value = "DoacaoManagerS")
    DoacaoManagerI doacaoMNG;

    @Autowired
    @Qualifier(value = "InstituicaoManagerS")
    Manager<Instituicao> instManager;

    @Autowired
    @Qualifier(value = "AtividadeManagerS")
    AtividadeManagerI atvManager;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @RequestMapping(method = RequestMethod.GET)
    public ModelAndView perfilInicio(HttpSession session) throws Exception {
        try {
        ModelAndView mav = new ModelAndView("perfilInicio");
        Voluntario user = (Voluntario) session.getAttribute("usuarioAtivo");
        List<Doacao> lista = doacaoMNG.doacoesRealizadasPorUsuario(user.getId());
        List<Doacao> lista1 = new ArrayList<>();
        List<Doacao> lista2 = new ArrayList<>();
        for (Doacao el : lista) {
            if (el.getTipo().equals(TipoDoacao.OFERTA))
                lista1.add(el);
            else
                lista2.add(el);
        }
        mav.addObject("listaOFERTAS", lista1);
        mav.addObject("listaPEDIDOS", lista2);
        mav.addObject("listaVolReqAss", voluntarioMNG.findVolsReqs());
        mav.addObject("listaInsts", instManager.list());

        if (user.getInstAss() == null) {
            mav.addObject("idMinhaInst", -1);
            mav.addObject("listaAtvsAguardandoAprov", null);
        } else {
            mav.addObject("idMinhaInst", user.getInstAss().getId());
            mav.addObject("listaAtvsAguardandoAprov",
                    atvManager.listAtividadesNaoAprovadas(
                            user.getInstAss().getId()));
            if (user.isRepInst()) {
                mav.addObject("listaMinhasAtividades",
                        atvManager.listaAtividadesMinhaInst(
                                user.getInstAss().getId()
                        ));
            } else if (user.isMemInst()) {
                mav.addObject("listaMinhasAtividades",
                        atvManager.listaAtividadesEuCriei(
                                user.getId()
                        ));
            }
        }

            return mav;
        } catch (Exception e) {
            throw new Exception("ERRO");
        }
    }

    @RequestMapping(value = "/atualizarCadastro", method = RequestMethod.GET)
    public String formParaAtualizar() {
        return "atualizaCadastro";
    }

    @RequestMapping(value = "/deletarConta", method = RequestMethod.POST)
    public String deletarConta(String senha, HttpSession session) throws Exception {
        Voluntario user = (Voluntario) session.getAttribute("usuarioAtivo");
        Voluntario user2 = voluntarioMNG.readUserByEmail(user.getEmail());

        if (user == null || user2 == null) {
            throw new Exception("Erro ao deletar conta.");
        }

        if (bCryptPasswordEncoder.matches(senha, user2.getSenha())) {
            if (voluntarioMNG.deleteUserByEmail(user2.getEmail(), senha, user2.getId())) {
                session.invalidate();
                return "redirect:/index";
            } else {
                throw new Exception("Erro ao deletar conta.");
            }
        } else
            throw new Exception("Causa provavel: senha incorreta.");
    }

    @RequestMapping("/atualizar")
    public String atualizar(Voluntario voluntario, Integer idd, String passwordConfirm,
                            HttpSession session) throws Exception {
        if (!voluntario.getSenha().equals(passwordConfirm))
            throw new Exception("Causa provavel: confirmacao de senha incorreta.");

        voluntario.setId(idd);
        if (voluntarioMNG.update(voluntario)) {
            Voluntario v = voluntarioMNG.read(idd);
            session.setAttribute("usuarioAtivo", v);
            return "redirect:/index";
        } else
            throw new Exception("ao atualizar usuario");
    }

    @ExceptionHandler(Exception.class)
    public ModelAndView handleError(HttpServletRequest req, Exception ex) {
        log.severe(req.getMethod() + " - Request: " + req.getRequestURI() + " raised " + ex);

        ex.printStackTrace();

        ModelAndView mav = new ModelAndView("erro");
        mav.addObject("exception", ex);
        mav.addObject("uri", req.getRequestURI());

        String strs[] = req.getRequestURI().split("\\?");
        String str = strs[0];

        if (ex.toString().contains("Causa")) {
            String o = ex.toString();
            String o1 = o.replaceAll("java", "");
            String o2 = o1.replaceAll("lang", "");
            String o3 = o2.replaceAll("Exception", "");
            String o4 = o3.replaceAll("\\.", "");
            String o5 = o4.replaceFirst(":", "");

            mav.addObject("causaMsg", o5);
        }

        switch (str) {
            case "/perfil/atualizar": {
                mav.addObject("errorMsg", "Erro ao atualizar.");
                break;
            }
            case "/perfil/deletarConta": {
                mav.addObject("errorMsg", "Erro ao deletar conta.");
                break;
            }
            default: {
                mav.addObject("errorMsg", "Erro.");
                break;
            }
        }
        return mav;
    }

}
