package br.com.galhardi.backend.models;

import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import br.com.galhardi.backend.models.Enums.TipoDoacao;

@Entity
public class Doacao {

    @ManyToOne
    private Voluntario criador;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(nullable = false, length = 800)
    private String descricao;

    @Column(nullable = false)
    private String titulo;

    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private Date data_criacao;

    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private Date data_realizada;

    @Column(nullable = false)
    private boolean realizada;

    @Column(nullable = false)
    private boolean ativa;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private TipoDoacao tipo;

    public Voluntario getCriador() {
        return criador;
    }

    public void setCriador(Voluntario criador) {
        this.criador = criador;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Date getData_criacao() {
        return data_criacao;
    }

    public void setData_criacao(Date data_criacao) {
        this.data_criacao = data_criacao;
    }

    public Date getData_realizada() {
        return data_realizada;
    }

    public void setData_realizada(Date data_realizada) {
        this.data_realizada = data_realizada;
    }

    public boolean isRealizada() {
        return realizada;
    }

    public void setRealizada(boolean realizada) {
        this.realizada = realizada;
    }

    public boolean isAtiva() {
        return ativa;
    }

    public void setAtiva(boolean ativa) {
        this.ativa = ativa;
    }

    public TipoDoacao getTipo() {
        return tipo;
    }

    public void setTipo(TipoDoacao tipo) {
        this.tipo = tipo;
    }
}
