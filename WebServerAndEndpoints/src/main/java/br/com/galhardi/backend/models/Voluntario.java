package br.com.galhardi.backend.models;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class Voluntario implements Serializable {

//    // GOOGLE FIELDS
//    @Column(unique = true)
//    private String id_GOOGLE;
//    private String link_foto;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(nullable = false)
    private Date dataNasc;

    @Column(nullable = false)
    private String nome;

    @Column(nullable = false)
    private String sNome;

    private String telefone;

    @Column(nullable = false)
    private String celular;

    @Column(unique = true, nullable = false)
    private String email;

    @Column(nullable = false)
    private boolean isMemInst;

    @ManyToOne
    private Instituicao instAss;

    @Column(nullable = false)
    private boolean isRepInst;

    @Column(nullable = false)
    private String senha;

    public Instituicao getInstAss() {
        return instAss;
    }

    public void setInstAss(Instituicao instAss) {
        this.instAss = instAss;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getDataNasc() {
        return dataNasc;
    }

    public void setDataNasc(Date dataNasc) {
        this.dataNasc = dataNasc;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getsNome() {
        return sNome;
    }

    public void setsNome(String sNome) {
        this.sNome = sNome;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public String getCelular() {
        return celular;
    }

    public void setCelular(String celular) {
        this.celular = celular;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean isMemInst() {
        return isMemInst;
    }

    public void setMemInst(boolean memInst) {
        isMemInst = memInst;
    }

    public void setIsMemInst(boolean isMemInst) {
        this.isMemInst = isMemInst;
    }

    public boolean isRepInst() {
        return isRepInst;
    }

    public void setRepInst(boolean repInst) {
        isRepInst = repInst;
    }

    public void setIsRepInst(boolean isRepInst) {
        this.isRepInst = isRepInst;
    }
}
