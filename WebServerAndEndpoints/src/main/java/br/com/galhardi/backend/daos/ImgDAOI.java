package br.com.galhardi.backend.daos;

import br.com.galhardi.backend.models.MyImage;

public interface ImgDAOI {

    boolean armazenaD(MyImage obj);

    MyImage recuperaD(Integer id);

    boolean deletaD(Integer id);

}
