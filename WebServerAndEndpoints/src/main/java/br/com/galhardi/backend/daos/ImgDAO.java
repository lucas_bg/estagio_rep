package br.com.galhardi.backend.daos;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.logging.Logger;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import br.com.galhardi.backend.models.MyImage;

@Repository(value = "ImgDAOR")
@Transactional
public class ImgDAO implements ImgDAOI {

    private static final Logger log = Logger.getLogger("ImgDAO");

    @PersistenceContext
    private EntityManager manager;

    @Override
    public boolean armazenaD(MyImage obj) {
        manager.persist(obj);
        return true;
    }

    @Override
    public MyImage recuperaD(Integer id) {
        String consulta = "SELECT i FROM MyImage i WHERE i.id_instituicao = :paramID";
        TypedQuery<MyImage> query = manager.createQuery(consulta, MyImage.class);
        query.setParameter("paramID", id);
        MyImage i;
        try {
            i = query.getSingleResult();
        } catch (NoResultException ex) {
            i = null;
        }
        return i;
    }

    @Override
    public boolean deletaD(Integer id) {
        String deleteQuery = "DELETE MyImage m WHERE m.id_instituicao = :paramIdInstituicao";
        Query query = manager.createQuery(deleteQuery);
        query.setParameter("paramIdInstituicao", id);
        if (query.executeUpdate() == 1) {
            log.info("SUCESSO AO DELETAR IMAGEM NO DAO");
            return true;
        } else {
            log.severe("ERRO AO DELETAR IMAGEM (BANCO)");
            return false;
        }
    }

}
