package br.com.galhardi.backend.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import java.util.Date;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import br.com.galhardi.backend.models.Doacao;
import br.com.galhardi.backend.models.Voluntario;
import br.com.galhardi.backend.services.DoacaoManagerI;
import br.com.galhardi.backend.services.VoluntarioManagerI;

@Controller
@RequestMapping("/doacao")
public class DoacaoController {

    private static final Logger log = Logger.getLogger("DoacaoController");

    @Autowired
    @Qualifier(value = "DoacaoManagerS")
    DoacaoManagerI manager;

    @Autowired
    @Qualifier(value = "VoluntarioManagerS")
    VoluntarioManagerI voluntarioMNG;

    @RequestMapping(method = RequestMethod.GET)
    public ModelAndView listDoacoes() {
        ModelAndView modelAndView = new ModelAndView("doacao/listDoacao");
        modelAndView.addObject("doacoesList", manager.list());
        return modelAndView;
    }

    @RequestMapping(value = "/delete", method = RequestMethod.GET)
    public String deleteDoacao(Integer id) {
        manager.delete(id);
        return "redirect:/doacao";
    }

    @RequestMapping("/insertForm")
    public ModelAndView formCadastroDoacao() {
        return new ModelAndView("doacao/formCadastroDoacao");
    }

    @RequestMapping(method = RequestMethod.POST)
    public String insertEUpdateDoacao(Doacao doacao, HttpSession session) {
        Voluntario user = (Voluntario) session.getAttribute("usuarioAtivo");
        Voluntario user2 = voluntarioMNG.readUserByEmail(user.getEmail());

        doacao.setCriador(user2);
        doacao.setAtiva(true);
        doacao.setData_criacao(new Date());
        doacao.setRealizada(false);

        manager.create(doacao);

        return "redirect:/doacao";
    }

    @RequestMapping("/realizar")
    public String realizarDoacao(Integer id) {
        manager.realizar(id);

        return "redirect:/doacao";
    }

    @ExceptionHandler(Exception.class)
    public ModelAndView handleError(HttpServletRequest req, Exception ex) {
        log.severe(req.getMethod() + " - Request: " + req.getRequestURI() + " raised " + ex);

        ex.printStackTrace();

        ModelAndView mav = new ModelAndView("erro");
        mav.addObject("exception", ex);
        mav.addObject("uri", req.getRequestURI());

        String strs[] = req.getRequestURI().split("\\?");
        String str = strs[0];

        if (ex.toString().contains("Causa")) {
            String o = ex.toString();
            String o1 = o.replaceAll("java", "");
            String o2 = o1.replaceAll("lang", "");
            String o3 = o2.replaceAll("Exception", "");
            String o4 = o3.replaceAll("\\.", "");
            String o5 = o4.replaceFirst(":", "");

            mav.addObject("causaMsg", o5);
        }

        switch (str) {
            case "/doacao/get": {
                mav.addObject("errorMsg", "Erro ao recuperar doação específica.");
                break;
            }
            case "/doacao/delete": {
                mav.addObject("errorMsg", "Erro ao deletar doação específica.");
                break;
            }
            case "/doacao/realizar": {
                mav.addObject("errorMsg", "Erro ao REALIZAR doação específica.");
                break;
            }
            case "/doacao": {
                if (req.getMethod().equals("POST")) {
                    mav.addObject("errorMsg", "Algum campo incorreto ou campo" +
                            " único já cadastrado, tente novamente.");
                } else if (req.getMethod().equals("GET")) {
                    mav.addObject("errorMsg", "Erro na listagem das doações.");
                }
                break;
            }
            default: {
                mav.addObject("errorMsg", "Erro.");
                break;
            }
        }
        return mav;
    }

}
