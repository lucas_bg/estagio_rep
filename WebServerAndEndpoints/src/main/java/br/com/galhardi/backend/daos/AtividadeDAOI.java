package br.com.galhardi.backend.daos;

import java.util.List;

import br.com.galhardi.backend.models.Atividade;

public interface AtividadeDAOI extends DAO<Atividade> {

    List<Atividade> listAtividadesNaoAprovadasD(Integer instId);

    boolean aprovarAtividadeD(Integer id);

    boolean reprovarAtividadeD(Integer id);

    List<Atividade> listaAtividadesMinhaInstD(Integer instId);

    List<Atividade> listaAtividadesEuCrieiD(Integer memId);

}
