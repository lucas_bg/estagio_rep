package br.com.galhardi.backend.models;

public class Response {

    private boolean booleanResp;

    private Integer intField;

    private String errorStrMsg;

    private String originalException;

    public String getOriginalException() {
        return originalException;
    }

    public void setOriginalException(String originalException) {
        this.originalException = originalException;
    }

    public String getErrorStrMsg() {
        return errorStrMsg;
    }

    public void setErrorStrMsg(String errorStrMsg) {
        this.errorStrMsg = errorStrMsg;
    }

    public Integer getIntField() {
        return intField;
    }

    public void setIntField(Integer intField) {
        this.intField = intField;
    }

    public boolean isBooleanResp() {
        return booleanResp;
    }

    public void setBooleanResp(boolean booleanResp) {
        this.booleanResp = booleanResp;
    }
}
