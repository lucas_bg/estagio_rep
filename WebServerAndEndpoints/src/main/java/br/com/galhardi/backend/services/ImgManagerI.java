package br.com.galhardi.backend.services;

import br.com.galhardi.backend.models.MyImage;

public interface ImgManagerI {

    boolean armazena(MyImage obj);

    MyImage recupera(Integer id);

    boolean deleta(Integer id);

}
