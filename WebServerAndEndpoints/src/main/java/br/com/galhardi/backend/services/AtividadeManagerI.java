package br.com.galhardi.backend.services;

import java.util.List;

import br.com.galhardi.backend.models.Atividade;

public interface AtividadeManagerI extends Manager<Atividade> {

    List<Atividade> listAtividadesNaoAprovadas(Integer instId);

    boolean aprovarAtividade(Integer id);

    boolean reprovarAtividade(Integer id);

    List<Atividade> listaAtividadesMinhaInst(Integer instId);

    List<Atividade> listaAtividadesEuCriei(Integer memId);

}
