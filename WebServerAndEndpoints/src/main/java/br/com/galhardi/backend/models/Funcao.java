package br.com.galhardi.backend.models;

import java.util.List;

import javax.persistence.DiscriminatorValue;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

import br.com.galhardi.backend.models.Enums.DiasDaSemana;

@Entity
@DiscriminatorValue("Funcao")
public class Funcao extends Atividade {

    private int duracao_media;

    @ElementCollection(targetClass = DiasDaSemana.class)
    @Enumerated(EnumType.STRING)
    private List<DiasDaSemana> dias_poss;

    public int getDuracao_media() {
        return duracao_media;
    }

    public void setDuracao_media(int duracao_media) {
        this.duracao_media = duracao_media;
    }

    public List<DiasDaSemana> getDias_poss() {
        return dias_poss;
    }

    public void setDias_poss(List<DiasDaSemana> dias_poss) {
        this.dias_poss = dias_poss;
    }
}
