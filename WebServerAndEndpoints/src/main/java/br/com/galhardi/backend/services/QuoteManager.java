package br.com.galhardi.backend.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;

import br.com.galhardi.backend.daos.DAO;
import br.com.galhardi.backend.models.Quote;

@Service(value = "QuoteManagerS")
public class QuoteManager implements Manager<Quote> {

    @Autowired
    @Qualifier(value = "QuoteDAOR")
    DAO<Quote> dao;

    public Integer create(Quote obj) {
        return dao.createD(obj);
    }

    public Quote read(Integer id) {
        return dao.readD(id);
    }

    public boolean update(Quote obj) {
        return dao.updateD(obj);
    }

    public void delete(Integer id) {
        dao.deleteD(id);
    }

    public List<Quote> list() {
        return dao.listD();
    }

}
