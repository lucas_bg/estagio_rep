package br.com.galhardi.backend.models;

import org.hibernate.annotations.GenericGenerator;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Transient;

@Entity
public class Instituicao implements Serializable {

    @Id
    @GeneratedValue(generator = "increment")
    @GenericGenerator(name = "increment", strategy = "increment")
    private Integer id;

    @Column(nullable = false)
    private String nome;

    @Column(nullable = false, length = 800)
    private String apresentacao;

    @Column(nullable = false, length = 6000)
    private String descricao_detalhada;

    @Column(nullable = false)
    private int numMem;

    @Column(nullable = false)
    private String telefone;

    @Column(nullable = false)
    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private Date data_fund;

    @Column(nullable = false)
    private int numFunc;

    private String endereco;

    @Column(unique = true)
    private String email;

    @Column(unique = true)
    private String website;

    @Column(unique = true)
    private String facebook;

    @Column(unique = true)
    private String retornar;

    @Transient
    private byte[] imgStrTEMP;

    @Transient
    private Integer userIdTEMP;

    private boolean aprovada;

    public Integer getUserIdTEMP() {
        return userIdTEMP;
    }

    public void setUserIdTEMP(Integer userIdTEMP) {
        this.userIdTEMP = userIdTEMP;
    }

    public boolean isAprovada() {
        return aprovada;
    }

    public void setAprovada(boolean aprovada) {
        this.aprovada = aprovada;
    }

    public String getDescricao_detalhada() {
        return descricao_detalhada;
    }

    public void setDescricao_detalhada(String descricao_detalhada) {
        this.descricao_detalhada = descricao_detalhada;
    }

    public byte[] getImgStrTEMP() {
        return imgStrTEMP;
    }

    public void setImgStrTEMP(byte[] imgStrTEMP) {
        this.imgStrTEMP = imgStrTEMP;
    }

    public Date getData_fund() {
        return data_fund;
    }

    public void setData_fund(Date data_fund) {
        this.data_fund = data_fund;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getApresentacao() {
        return apresentacao;
    }

    public void setApresentacao(String apresentacao) {
        this.apresentacao = apresentacao;
    }

    public int getNumMem() {
        return numMem;
    }

    public void setNumMem(int numMem) {
        this.numMem = numMem;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public int getNumFunc() {
        return numFunc;
    }

    public void setNumFunc(int numFunc) {
        this.numFunc = numFunc;
    }

    public String getEndereco() {
        return endereco;
    }

    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getFacebook() {
        return facebook;
    }

    public void setFacebook(String facebook) {
        this.facebook = facebook;
    }

    public String getRetornar() {
        return retornar;
    }

    public void setRetornar(String retornar) {
        this.retornar = retornar;
    }
}
