package br.com.galhardi.backend.controllers;

import org.apache.commons.io.IOUtils;
import org.gmr.web.multipart.GMultipartFile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.io.Resource;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import br.com.configurations.ApplicationContextProvider;
import br.com.galhardi.backend.models.Instituicao;
import br.com.galhardi.backend.models.MyImage;
import br.com.galhardi.backend.models.Voluntario;
import br.com.galhardi.backend.services.ImgManagerI;
import br.com.galhardi.backend.services.Manager;
import br.com.galhardi.backend.services.VoluntarioManagerI;

@Controller
@RequestMapping("/instituicao")
public class InstituicaoController {

    private static final Logger log = Logger.getLogger("InstituicaoController");

    @Autowired
    @Qualifier(value = "InstituicaoManagerS")
    Manager<Instituicao> manager;

    @Autowired
    @Qualifier(value = "ImgManagerS")
    ImgManagerI imgManager;

    @Autowired
    @Qualifier(value = "VoluntarioManagerS")
    VoluntarioManagerI voluntarioMNG;

    private Instituicao prepareFieldsBeforeInsert(Instituicao i) {
        if (i.getRetornar().contains("http") || i.getRetornar().contains("HTTP")) {
            String[] temp = i.getRetornar().split("//");
            i.setRetornar(temp[1]);
        }
        if (i.getFacebook().contains("http") || i.getFacebook().contains("HTTP")) {
            String[] temp = i.getFacebook().split("//");
            i.setFacebook(temp[1]);
        }
        if (i.getWebsite().contains("http") || i.getWebsite().contains("HTTP")) {
            String[] temp = i.getWebsite().split("//");
            i.setWebsite(temp[1]);
        }
        if (i.getEndereco().equals(""))
            i.setEndereco(null);
        if (i.getEmail().equals(""))
            i.setEmail(null);
        if (i.getWebsite().equals(""))
            i.setWebsite(null);
        if (i.getFacebook().equals(""))
            i.setFacebook(null);
        if (i.getRetornar().equals(""))
            i.setRetornar(null);
        return i;
    }

    private byte[] prepareDefaultImage() {
        ApplicationContextProvider applicationContextProvider = new ApplicationContextProvider();
        Resource r = applicationContextProvider.getApplicationContext()
                .getResource("WEB-INF/classes/files/default_img.jpg");
        File f;
        FileInputStream fileInput;
        byte[] imgToReturn = null;
        try {
            f = r.getFile();
            fileInput = new FileInputStream(f);
            imgToReturn = IOUtils.toByteArray(fileInput);
        } catch (IOException e) {
            log.severe("Por algum motivo a default image nao deu certo...");
        }
        return imgToReturn;
    }

    @RequestMapping(method = RequestMethod.GET)
    public ModelAndView listInstituicoes() {
        ModelAndView modelAndView = new ModelAndView("instituicao/listInstituicao");
        modelAndView.addObject("instituicoesList", manager.list());
        return modelAndView;
    }

    @RequestMapping(value = "/get", method = RequestMethod.GET)
    public ModelAndView getInstituicao(Integer id) {
        ModelAndView modelAndView = new ModelAndView("instituicao/instituicaoDetail");
        modelAndView.addObject("inst", manager.read(id));
        return modelAndView;
    }

    @ResponseBody
    @RequestMapping(value = "/getImg", method = RequestMethod.GET,
            produces = MediaType.IMAGE_JPEG_VALUE)
    public byte[] getInstImg(Integer id) throws IOException {
        MyImage myImage = imgManager.recupera(id);
        if (myImage != null) {
            if (myImage.getImagem() != null) {
                if (myImage.getImagem().length == 0) {
                    return prepareDefaultImage();
                }
            }
            return myImage.getImagem();
        } else {
            return null;
        }
    }

    @RequestMapping(value = "/delete", method = RequestMethod.GET)
    public String deleteInstituicao(Integer id, HttpSession session) throws Exception {
        Voluntario user = (Voluntario) session.getAttribute("usuarioAtivo");
        Voluntario user2 = voluntarioMNG.readUserByEmail(user.getEmail());
        Instituicao inst = manager.read(id);

        if (user.getInstAss() == null)
            throw new Exception("Causa provavel: você não tem permissão para isso.");

        //ALTERA1
        if (!(user.getInstAss().getId().equals(inst.getId()) && user.isRepInst()))
//        if (!inst.getRepresentante().getId().equals(user.getId()))
            throw new Exception("Causa provavel: você não tem permissão para isso.");

        voluntarioMNG.setMembroERepEAssInst(false, false, null, user2.getId());
        manager.delete(id);
        imgManager.deleta(id);
        return "redirect:/instituicao";
    }

    @RequestMapping("/insertForm")
    public ModelAndView formCadastroInst() {
        ModelAndView modelAndView = new ModelAndView("instituicao/formCadastroInst");
        modelAndView.addObject("updateMode", false);
        return modelAndView;
    }

    @RequestMapping("/updateForm")
    public ModelAndView formCadastroInst2(Integer id) {
        ModelAndView modelAndView = new ModelAndView("instituicao/formCadastroInst");
        modelAndView.addObject("inst", manager.read(id));
        modelAndView.addObject("updateMode", true);
        return modelAndView;
    }

    @RequestMapping(method = RequestMethod.POST, headers = "content-type=multipart/form-data")
    public ModelAndView insertInst(@RequestParam GMultipartFile imagee,
                                   Instituicao inst, @RequestParam Integer idd,
                                   HttpSession session) {

        Instituicao inst2 = prepareFieldsBeforeInsert(inst);

        MyImage myImage = new MyImage();
        myImage.setImagem(imagee.getBytes());
        if (idd == -1) {
            Integer instituicaoId = manager.create(inst2);
            myImage.setId_instituicao(instituicaoId);
        } else {
            inst2.setId(idd);
            manager.update(inst2);
            imgManager.deleta(idd);
            myImage.setId_instituicao(idd);
        }
        imgManager.armazena(myImage);

        Voluntario user = (Voluntario) session.getAttribute("usuarioAtivo");
        voluntarioMNG.setMembroERepEAssInst(true, true, inst2, user.getId());

        session.setAttribute("usuarioAtivo", voluntarioMNG.read(user.getId()));

        ModelAndView mav = new ModelAndView("msg");
        mav.addObject("msg", "A instituição foi cadastrada com sucesso!" +
                " Está aguardando aprovação do Administrador.");
        return mav;
    }

    @RequestMapping(value = "/associa", method = RequestMethod.POST)
    public String associaMembro(String select_inst, HttpSession session) throws Exception {
        Voluntario user = (Voluntario) session.getAttribute("usuarioAtivo");
        Integer inst_id = Integer.valueOf(select_inst);
        log.info("OOOO:" + inst_id + 10 + "k");

        if (voluntarioMNG.associarAInst(user.getId(), inst_id)) {
            session.invalidate();
            return "index";
        } else
            throw new Exception("ERRO");
    }

    @RequestMapping("/aprovar")
    public String aprovaMembro(Integer id) throws Exception {
        if (voluntarioMNG.aprovaReqAssVol(id))
            return "redirect:/perfil";
        else
            throw new Exception("ERRO");
    }

    @RequestMapping("/reprovar")
    public String reprovaMembro(Integer id) throws Exception {
        try {
            voluntarioMNG.setMembroERepEAssInst(false, false, null, id);
            return "redirect:/perfil";
        } catch (Exception e) {
            throw new Exception("ERRO");
        }
    }

    @ExceptionHandler(Exception.class)
    public ModelAndView handleError(HttpServletRequest req, Exception ex) {
        log.severe(req.getMethod() + " - Request: " + req.getRequestURI() + " raised " + ex);

        ex.printStackTrace();

        ModelAndView mav = new ModelAndView("erro");
        mav.addObject("exception", ex);
        mav.addObject("uri", req.getRequestURI());

        String strs[] = req.getRequestURI().split("\\?");
        String str = strs[0];

        if (ex.toString().contains("Causa")) {
            String o = ex.toString();
            String o1 = o.replaceAll("java", "");
            String o2 = o1.replaceAll("lang", "");
            String o3 = o2.replaceAll("Exception", "");
            String o4 = o3.replaceAll("\\.", "");
            String o5 = o4.replaceFirst(":", "");

            mav.addObject("causaMsg", o5);
        }

        switch (str) {
            case "/instituicao/get": {
                mav.addObject("errorMsg", "Erro ao recuperar instituição específica.");
                break;
            }
            case "/instituicao/delete": {
                mav.addObject("errorMsg", "Erro ao deletar instituição específica.");
                break;
            }
            case "/instituicao": {
                if (req.getMethod().equals("POST")) {
                    if (ex.toString().contains("org.hibernate.exception.GenericJDBCException")) {
                        mav.addObject("errorMsg", "Provavelmente a imagem é maior que 500Kb" +
                                ", tente uma imagem menor.");
                    } else {
                        mav.addObject("errorMsg", "Algum campo incorreto ou campo único já" +
                                " cadastrado, tente novamente.");
                    }
                } else if (req.getMethod().equals("GET")) {
                    mav.addObject("errorMsg", "Erro na listagem das instituições.");
                }
                break;
            }
            default: {
                mav.addObject("errorMsg", "Erro.");
                break;
            }
        }
        return mav;
    }

}
