package br.com.galhardi.backend.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.logging.Logger;

import br.com.galhardi.backend.daos.ImgDAOI;
import br.com.galhardi.backend.models.MyImage;

@Service(value = "ImgManagerS")
public class ImgManager implements ImgManagerI {

    private static final Logger log = Logger.getLogger("ImgManager");

    @Autowired
    @Qualifier(value = "ImgDAOR")
    ImgDAOI dao;

    @Override
    public boolean armazena(MyImage obj) {
        return dao.armazenaD(obj);
    }

    @Override
    public MyImage recupera(Integer id) {
        try {
            return dao.recuperaD(id);
        } catch (Exception e) {
            log.severe("Erro no recupera Imagem.");
            return null;
        }
    }

    @Override
    public boolean deleta(Integer id) {
        return dao.deletaD(id);
    }

}
