package br.com.galhardi.backend.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import br.com.galhardi.backend.models.Voluntario;
import br.com.galhardi.backend.services.VoluntarioManagerI;

@Controller
public class LoginVolController {

    private static final Logger log = Logger.getLogger("LoginVolController");

    @Autowired
    @Qualifier(value = "VoluntarioManagerS")
    VoluntarioManagerI voluntarioMNG;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @RequestMapping("/")
    public String index(HttpSession session) {
        if (session.getAttribute("usuarioAtivo") != null)
            return "indexLogado";
        else
            return "index";
    }

    @RequestMapping("/index")
    public String indexx(HttpSession session) {
        if (session.getAttribute("usuarioAtivo") != null)
            return "indexLogado";
        else
            return "index";
    }

    @RequestMapping("/admPanel")
    public ModelAndView admAccess(HttpSession session) {
        boolean ii = (boolean) session.getAttribute("admSession");
        ModelAndView mav;
        if (ii) {
            mav = new ModelAndView("admPage");
            mav.addObject("instList", voluntarioMNG.admGetInstsNotAprov());
            return mav;
        } else {
            mav = new ModelAndView("index");
            return mav;
        }
    }

    @RequestMapping("/privacidadeETermos")
    public String privacidadeETermos() {
        return "privacidadeETermos";
    }

    @RequestMapping("/sobre")
    public String sobre() {
        return "sobre";
    }

    @RequestMapping("/contato")
    public String contato() {
        return "contato";
    }

    @RequestMapping(value = "/aproveInst", method = RequestMethod.GET)
    public String aproveInstituicao(Integer id, HttpSession session) {
        if ((boolean) session.getAttribute("admSession")) {
            voluntarioMNG.admAprovarInst(id);
            return "redirect:/admPanel";
        } else
            return "index";
    }

    @RequestMapping(value = "/loginn", method = RequestMethod.POST)
    public String loginn(String username, String password, HttpSession session) throws Exception {
        if (username.equals("lucas.b.galhardi@gmail.com")
                && password.equals("sOiQkkLLa@safAA_22lkds")) {
            session.setAttribute("admSession", true);
            session.setAttribute("usuarioAtivo", new Voluntario());
            return "redirect:/admPanel";
        }

        Voluntario user = voluntarioMNG.readUserByEmail(username);

        if (user == null)
            throw new Exception("Causa provavel: email nao cadastrado.");

        if (bCryptPasswordEncoder.matches(password, user.getSenha())) {
            session.setAttribute("usuarioAtivo", user);
            return "redirect:/index";
        } else
            throw new Exception("Causa provavel: senha incorreta.");
    }

    @RequestMapping("/logout")
    public String logout(HttpSession session) {
        session.invalidate();
        return "index";
    }

    @RequestMapping("/cadastrar")
    public String cadastro(Voluntario voluntario, String passwordConfirm) throws Exception {
        if (!voluntario.getSenha().equals(passwordConfirm))
            throw new Exception("Causa provavel: confirmacao de senha incorreta.");

        voluntario.setIsRepInst(false);
        voluntario.setIsMemInst(false);
        voluntario.setInstAss(null);

        boolean r = voluntarioMNG.insertNewVolunteer(voluntario);

        if (!r)
            throw new Exception("ao cadastrar novo usuario");

        return "index";
    }

    @ExceptionHandler(Exception.class)
    public ModelAndView handleError(HttpServletRequest req, Exception ex) {
        log.severe(req.getMethod() + " - Request: " + req.getRequestURI() + " raised " + ex);

        ex.printStackTrace();

        ModelAndView mav = new ModelAndView("erro");
        mav.addObject("exception", ex);
        mav.addObject("uri", req.getRequestURI());

        String strs[] = req.getRequestURI().split("\\?");
        String str = strs[0];

        if (ex.toString().contains("Causa")) {
            String o = ex.toString();
            String o1 = o.replaceAll("java", "");
            String o2 = o1.replaceAll("lang", "");
            String o3 = o2.replaceAll("Exception", "");
            String o4 = o3.replaceAll("\\.", "");
            String o5 = o4.replaceFirst(":", "");

            mav.addObject("causaMsg", o5);
        }

        switch (str) {
            case "/loginn": {
                mav.addObject("errorMsg", "Erro ao realizar o login.");
                break;
            }
            case "/logout": {
                mav.addObject("errorMsg", "Erro ao realizar o logout.");
                break;
            }
            case "/cadastrar": {
                mav.addObject("errorMsg", "Erro ao se cadastrar.");
                break;
            }
            default: {
                mav.addObject("errorMsg", "Erro.");
                break;
            }
        }
        return mav;
    }

}
