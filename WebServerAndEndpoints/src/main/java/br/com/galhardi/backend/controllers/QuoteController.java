package br.com.galhardi.backend.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import br.com.galhardi.backend.models.Quote;
import br.com.galhardi.backend.services.Manager;

@Controller
@RequestMapping("/quotes")
public class QuoteController {

    @Autowired
    @Qualifier(value = "QuoteManagerS")
    Manager<Quote> quoteMNG;

    @RequestMapping("/insertV")
    public String telaCreate() {
        return "quotes/form";
    }

    @RequestMapping("/updateV")
    public String telaUpdate() {
        return "quotes/formUp";
    }

    @RequestMapping(method = RequestMethod.POST)
    public String insertQuote(Quote q, RedirectAttributes redirectAttributes) {
        System.out.println("Salvando o produto q: " + q.getWhat() + q.getWho());
        quoteMNG.create(q);
        redirectAttributes.addFlashAttribute("success", "produto cadastrado com sucesso");
        return "redirect:/quotes";
    }

    @RequestMapping(method = RequestMethod.GET)
    public ModelAndView listQuotes() {
        ModelAndView modelAndView = new ModelAndView("quotes/list");
        modelAndView.addObject("quotes", quoteMNG.list());
        return modelAndView;
    }

    @RequestMapping(value = "/get", method = RequestMethod.GET)
    public ModelAndView getQuote(Integer id) {
        ModelAndView modelAndView = new ModelAndView("quotes/singleQuote");
        modelAndView.addObject("quote", quoteMNG.read(id));
        return modelAndView;
    }

    @RequestMapping(value = "/delete", method = RequestMethod.POST)
    public String deleteQuote(Integer id, RedirectAttributes redirectAttributes) {
        quoteMNG.delete(id);
        redirectAttributes.addFlashAttribute("successDelete", "produto deletado com sucesso");
        return "redirect:/quotes";
    }

    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public String updateQuote(Quote quote, RedirectAttributes redirectAttributes) {
        quoteMNG.update(quote);
        redirectAttributes.addFlashAttribute("successUpdate", "produto alterado com sucesso");
        return "redirect:/quotes";
    }

}
