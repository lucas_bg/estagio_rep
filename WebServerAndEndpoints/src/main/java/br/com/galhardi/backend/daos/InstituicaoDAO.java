package br.com.galhardi.backend.daos;

import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.logging.Logger;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import br.com.galhardi.backend.models.Instituicao;

@Repository(value = "InstituicaoDAOR")
@Transactional
public class InstituicaoDAO implements DAO<Instituicao> {

    private static final Logger log = Logger.getLogger("InstituicaoDAO");

    @PersistenceContext
    private EntityManager manager;

    public Integer createD(Instituicao obj) {
        obj.setAprovada(false);
        manager.persist(obj);
        manager.flush();
        return obj.getId();
    }

    public Instituicao readD(Integer id) {
        return manager.find(Instituicao.class, id);
    }

    public boolean updateD(Instituicao obj) {
        Instituicao i = manager.find(Instituicao.class, obj.getId());
        boolean isAprovada = i.isAprovada();
        BeanUtils.copyProperties(obj, i);
        i.setAprovada(isAprovada);
        manager.merge(i);
        manager.flush();
        return true;
    }

    public void deleteD(Integer id) {
        String update1 = "UPDATE Voluntario v SET v.instAss = NULL, " +
                "v.isMemInst = FALSE, v.isRepInst = FALSE " +
                "WHERE v.instAss.id = :paramIdInstituicao";
        Query query4 = manager.createQuery(update1);
        query4.setParameter("paramIdInstituicao", id);
        try {
            query4.executeUpdate();
        } catch (Exception e) {
            return;
        }

        manager.remove(manager.find(Instituicao.class, id));
    }

    public List<Instituicao> listD() {
        return manager.createQuery("SELECT i from Instituicao i WHERE i.aprovada = TRUE", Instituicao.class).getResultList();
    }

}
