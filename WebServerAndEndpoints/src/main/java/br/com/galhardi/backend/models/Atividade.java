package br.com.galhardi.backend.models;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.ManyToOne;

import br.com.galhardi.backend.models.Enums.CategoriaAtiv;
import br.com.galhardi.backend.models.Enums.StatusAtiv;

@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "tipo_classe")
public class Atividade {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private StatusAtiv status;

    @Column(nullable = false)
    private String titulo;

    @Column(nullable = false, length = 800)
    private String apresentacao;

    @Column(nullable = false, length = 6000)
    private String descricao_detalhada;

    private int idade_min;

    private int num_volun_atual;

    private int num_volun_limite;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private CategoriaAtiv categoria;

    @ManyToOne
    private Instituicao inst_pertc;

    @ManyToOne
    private Voluntario criador;

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getApresentacao() {
        return apresentacao;
    }

    public void setApresentacao(String apresentacao) {
        this.apresentacao = apresentacao;
    }

    public String getDescricao_detalhada() {
        return descricao_detalhada;
    }

    public void setDescricao_detalhada(String descricao_detalhada) {
        this.descricao_detalhada = descricao_detalhada;
    }

    public Voluntario getCriador() {
        return criador;
    }

    public void setCriador(Voluntario criador) {
        this.criador = criador;
    }

    public Instituicao getInst_pertc() {
        return inst_pertc;
    }

    public void setInst_pertc(Instituicao inst_pertc) {
        this.inst_pertc = inst_pertc;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public StatusAtiv getStatus() {
        return status;
    }

    public void setStatus(StatusAtiv status) {
        this.status = status;
    }

    public int getIdade_min() {
        return idade_min;
    }

    public void setIdade_min(int idade_min) {
        this.idade_min = idade_min;
    }

    public int getNum_volun_atual() {
        return num_volun_atual;
    }

    public void setNum_volun_atual(int num_volun_atual) {
        this.num_volun_atual = num_volun_atual;
    }

    public int getNum_volun_limite() {
        return num_volun_limite;
    }

    public void setNum_volun_limite(int num_volun_limite) {
        this.num_volun_limite = num_volun_limite;
    }

    public CategoriaAtiv getCategoria() {
        return categoria;
    }

    public void setCategoria(CategoriaAtiv categoria) {
        this.categoria = categoria;
    }
}
