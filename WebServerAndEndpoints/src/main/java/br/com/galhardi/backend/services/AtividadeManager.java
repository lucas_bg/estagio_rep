package br.com.galhardi.backend.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.logging.Logger;

import br.com.galhardi.backend.daos.AtividadeDAOI;
import br.com.galhardi.backend.models.Atividade;

@Service(value = "AtividadeManagerS")
public class AtividadeManager implements AtividadeManagerI {

    private static final Logger log = Logger.getLogger("AtividadeManager");

    @Autowired
    @Qualifier(value = "AtividadeDAOR")
    AtividadeDAOI dao;

    public Integer create(Atividade obj) {
        return dao.createD(obj);
    }

    public Atividade read(Integer id) {
        return dao.readD(id);
    }

    public boolean update(Atividade obj) {
        return dao.updateD(obj);
    }

    public void delete(Integer id) {
        dao.deleteD(id);
    }

    public List<Atividade> list() {
        return dao.listD();
    }

    @Override
    public List<Atividade> listAtividadesNaoAprovadas(Integer instId
    ) {
        return dao.listAtividadesNaoAprovadasD(instId);
    }

    @Override
    public boolean aprovarAtividade(Integer id) {
        return dao.aprovarAtividadeD(id);
    }

    @Override
    public boolean reprovarAtividade(Integer id) {
        return dao.reprovarAtividadeD(id);
    }

    @Override
    public List<Atividade> listaAtividadesMinhaInst(Integer instId) {
        return dao.listaAtividadesMinhaInstD(instId);
    }

    @Override
    public List<Atividade> listaAtividadesEuCriei(Integer memId) {
        return dao.listaAtividadesEuCrieiD(memId);
    }
}
