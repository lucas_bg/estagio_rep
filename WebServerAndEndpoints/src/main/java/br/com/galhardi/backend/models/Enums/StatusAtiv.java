package br.com.galhardi.backend.models.Enums;

public enum StatusAtiv {
    NAO_ANALISADA,
    APROVADA,
    REPROVADA
}
