package br.com.galhardi.backend.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import br.com.galhardi.backend.models.Atividade;
import br.com.galhardi.backend.models.Enums.DiasDaSemana;
import br.com.galhardi.backend.models.Enums.StatusAtiv;
import br.com.galhardi.backend.models.Evento;
import br.com.galhardi.backend.models.Funcao;
import br.com.galhardi.backend.models.Instituicao;
import br.com.galhardi.backend.models.Projeto;
import br.com.galhardi.backend.models.Voluntario;
import br.com.galhardi.backend.services.AtividadeManagerI;
import br.com.galhardi.backend.services.VoluntarioManagerI;

@Controller
@RequestMapping("/atividade")
public class AtividadeController {

    private static final Logger log = Logger.getLogger("AtividadeController");

    @Autowired
    @Qualifier(value = "AtividadeManagerS")
    AtividadeManagerI manager;

    @Autowired
    @Qualifier(value = "VoluntarioManagerS")
    VoluntarioManagerI voluntarioMNG;

    @RequestMapping(method = RequestMethod.GET)
    public ModelAndView listAtividade() {
        ModelAndView mav = new ModelAndView("atividade/listAtividade");
        mav.addObject("listaAtivs", manager.list());
        return mav;
    }

    @RequestMapping(value = "/get", method = RequestMethod.GET)
    public ModelAndView getAtividade(Integer id) {
        ModelAndView modelAndView = new ModelAndView("atividade/atividadeDetail");
        modelAndView.addObject("atv", manager.read(id));
        return modelAndView;
    }

    @RequestMapping("/insertForm")
    public ModelAndView formCadastroAtividade() {
        return new ModelAndView("atividade/formCadastroAtv");
    }

    @RequestMapping("/newEvento")
    public ModelAndView cadastrarEvento(Evento evento, String tipo_options,
                                        HttpSession session) {
        ModelAndView mav = new ModelAndView("msg");
        Voluntario user = (Voluntario) session.getAttribute("usuarioAtivo");

        Voluntario volTEMP = new Voluntario();
        volTEMP.setId(user.getId());
        Instituicao instTEMP = new Instituicao();
        instTEMP.setId(user.getInstAss().getId());
        evento.setCriador(volTEMP);
        evento.setInst_pertc(instTEMP);
        if (user.isRepInst())
            evento.setStatus(StatusAtiv.APROVADA);
        else if (user.isMemInst())
            evento.setStatus(StatusAtiv.NAO_ANALISADA);

        manager.create(evento);

        mav.addObject("msg", "Evento cadastrado com sucesso");
        return mav;
    }

    @RequestMapping("/newFuncao")
    public ModelAndView cadastrarFuncao(Funcao funcao, String tipo_options,
                                        HttpSession session,
                                        String segunda,
                                        String terca,
                                        String quarta,
                                        String quinta,
                                        String sexta,
                                        String sabado,
                                        String domingo) {
        ModelAndView mav = new ModelAndView("msg");
        Voluntario user2 = (Voluntario) session.getAttribute("usuarioAtivo");
        Voluntario user = voluntarioMNG.readUserByEmail(user2.getEmail());

        List<DiasDaSemana> list = new ArrayList<>();
        if (segunda != null)
            list.add(DiasDaSemana.SEGUNDA);
        if (terca != null)
            list.add(DiasDaSemana.TERCA);
        if (quarta != null)
            list.add(DiasDaSemana.QUARTA);
        if (quinta != null)
            list.add(DiasDaSemana.QUINTA);
        if (sexta != null)
            list.add(DiasDaSemana.SEXTA);
        if (sabado != null)
            list.add(DiasDaSemana.SABADO);
        if (domingo != null)
            list.add(DiasDaSemana.DOMINGO);
        funcao.setDias_poss(list);

        Voluntario volTEMP = new Voluntario();
        volTEMP.setId(user.getId());
        Instituicao instTEMP = new Instituicao();
        instTEMP.setId(user.getInstAss().getId());
        funcao.setCriador(volTEMP);
        funcao.setInst_pertc(instTEMP);
        if (user.isRepInst())
            funcao.setStatus(StatusAtiv.APROVADA);
        else if (user.isMemInst())
            funcao.setStatus(StatusAtiv.NAO_ANALISADA);

        manager.create(funcao);

        mav.addObject("msg", "Função cadastrada com sucesso");
        return mav;
    }

    @RequestMapping("/newProjeto")
    public ModelAndView cadastrarEvento(Projeto projeto, String tipo_options,
                                        HttpSession session,
                                        String segunda_p,
                                        String terca_p,
                                        String quarta_p,
                                        String quinta_p,
                                        String sexta_p,
                                        String sabado_p,
                                        String domingo_p) {
        ModelAndView mav = new ModelAndView("msg");
        Voluntario user = (Voluntario) session.getAttribute("usuarioAtivo");

        List<DiasDaSemana> list = new ArrayList<>();
        if (segunda_p != null)
            list.add(DiasDaSemana.SEGUNDA);
        if (terca_p != null)
            list.add(DiasDaSemana.TERCA);
        if (quarta_p != null)
            list.add(DiasDaSemana.QUARTA);
        if (quinta_p != null)
            list.add(DiasDaSemana.QUINTA);
        if (sexta_p != null)
            list.add(DiasDaSemana.SEXTA);
        if (sabado_p != null)
            list.add(DiasDaSemana.SABADO);
        if (domingo_p != null)
            list.add(DiasDaSemana.DOMINGO);
        projeto.setDias_ocorr(list);

        Voluntario volTEMP = new Voluntario();
        volTEMP.setId(user.getId());
        Instituicao instTEMP = new Instituicao();
        instTEMP.setId(user.getInstAss().getId());
        projeto.setCriador(volTEMP);
        projeto.setInst_pertc(instTEMP);
        if (user.isRepInst())
            projeto.setStatus(StatusAtiv.APROVADA);
        else if (user.isMemInst())
            projeto.setStatus(StatusAtiv.NAO_ANALISADA);

        manager.create(projeto);

        mav.addObject("msg", "Projeto cadastrado com sucesso");
        return mav;
    }

    @RequestMapping("/delete")
    public String deletarAtividade(Integer id, HttpSession session) throws Exception {
        Voluntario user = (Voluntario) session.getAttribute("usuarioAtivo");
        Atividade atv = manager.read(id);
        if (!user.getInstAss().getId().equals(atv.getInst_pertc().getId()))
            throw new Exception("ERRO");
        else
            manager.delete(id);

        return "redirect:/perfil";
    }

    @RequestMapping("/aprovar")
    public String aprovaAtividade(Integer id) throws Exception {
        if (manager.aprovarAtividade(id))
            return "redirect:/perfil";
        else
            throw new Exception("ERRO");
    }

    @RequestMapping("/reprovar")
    public String reprovaAtividade(Integer id) throws Exception {
        if (manager.reprovarAtividade(id))
            return "redirect:/perfil";
        else
            throw new Exception("ERRO");
    }

    @ExceptionHandler(Exception.class)
    public ModelAndView handleError(HttpServletRequest req, Exception ex) {
        log.severe(req.getMethod() + " - Request: " + req.getRequestURI() + " raised " + ex);

        ex.printStackTrace();

        ModelAndView mav = new ModelAndView("erro");
        mav.addObject("exception", ex);
        mav.addObject("uri", req.getRequestURI());

        String strs[] = req.getRequestURI().split("\\?");
        String str = strs[0];

        if (ex.toString().contains("Causa")) {
            String o = ex.toString();
            String o1 = o.replaceAll("java", "");
            String o2 = o1.replaceAll("lang", "");
            String o3 = o2.replaceAll("Exception", "");
            String o4 = o3.replaceAll("\\.", "");
            String o5 = o4.replaceFirst(":", "");

            mav.addObject("causaMsg", o5);
        }

        switch (str) {
            default: {
                mav.addObject("errorMsg", "Erro.");
                break;
            }
        }
        return mav;
    }

}
