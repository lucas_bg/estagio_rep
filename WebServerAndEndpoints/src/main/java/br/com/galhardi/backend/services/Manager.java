package br.com.galhardi.backend.services;

import java.util.List;

public interface Manager<T> {

    Integer create(T obj);

    T read(Integer id);

    boolean update(T obj);

    void delete(Integer id);

    List<T> list();

}
