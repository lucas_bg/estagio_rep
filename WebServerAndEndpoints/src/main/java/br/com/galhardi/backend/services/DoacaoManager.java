package br.com.galhardi.backend.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.logging.Logger;

import br.com.galhardi.backend.daos.DoacaoDAOI;
import br.com.galhardi.backend.models.Doacao;

@Service(value = "DoacaoManagerS")
public class DoacaoManager implements DoacaoManagerI {

    private static final Logger log = Logger.getLogger("DoacaoManager");

    @Autowired
    @Qualifier(value = "DoacaoDAOR")
    DoacaoDAOI dao;

    public Integer create(Doacao obj) {
        return dao.createD(obj);
    }

    public Doacao read(Integer id) {
        return dao.readD(id);
    }

    public boolean update(Doacao obj) {
        return dao.updateD(obj);
    }

    public void delete(Integer id) {
        dao.deleteD(id);
    }

    public List<Doacao> list() {
        return dao.listD();
    }

    @Override
    public void realizar(Integer id) {
        dao.realizarD(id);
    }

    public List<Doacao> doacoesRealizadasPorUsuario(Integer id) {
        return dao.doacoesRealizadasPorUsuarioD(id);
    }

}
