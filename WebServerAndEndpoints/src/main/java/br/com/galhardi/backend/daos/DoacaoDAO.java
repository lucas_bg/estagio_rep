package br.com.galhardi.backend.daos;

import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import br.com.galhardi.backend.models.Doacao;

@Repository(value = "DoacaoDAOR")
@Transactional
public class DoacaoDAO implements DoacaoDAOI {

    private static final Logger log = Logger.getLogger("DoacaoDAO");

    @PersistenceContext
    private EntityManager manager;

    @Override
    public Integer createD(Doacao obj) {
        manager.persist(obj);
        manager.flush();
        return obj.getId();
    }

    @Override
    public Doacao readD(Integer id) {
        return manager.find(Doacao.class, id);
    }

    @Override
    public boolean updateD(Doacao obj) {
        try {
            Doacao d = manager.find(Doacao.class, obj.getId());
            BeanUtils.copyProperties(obj, d);
            manager.merge(obj);
            manager.flush();
            return true;
        } catch (Exception exc) {
            return false;
        }
    }

    @Override
    public void deleteD(Integer id) {
        Doacao doacao = manager.find(Doacao.class, id);
        manager.remove(doacao);
        manager.flush();
    }

    @Override
    public List<Doacao> listD() {
        return manager.createQuery(
                "SELECT d FROM Doacao d WHERE d.ativa = TRUE ORDER BY d.data_criacao DESC",
                Doacao.class)
                .getResultList();
    }

    @Override
    public void realizarD(Integer id) {
        Doacao doacao = manager.find(Doacao.class, id);

        doacao.setRealizada(true);
        doacao.setData_realizada(new Date());
        doacao.setAtiva(false);

        manager.merge(doacao);
        manager.flush();
    }

    @Override
    public List<Doacao> doacoesRealizadasPorUsuarioD(Integer id) {
        String consulta = "SELECT d FROM Doacao d WHERE d.realizada = TRUE AND " +
                "d.criador.id = :paramId ORDER BY d.data_criacao DESC";
        TypedQuery<Doacao> query = manager.createQuery(consulta, Doacao.class);
        query.setParameter("paramId", id);

        return query.getResultList();
    }

}
