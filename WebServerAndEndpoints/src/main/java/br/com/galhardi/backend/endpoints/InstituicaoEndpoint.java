package br.com.galhardi.backend.endpoints;

import com.google.api.server.spi.config.Api;
import com.google.api.server.spi.config.ApiMethod;
import com.google.api.server.spi.config.ApiNamespace;
import com.google.api.server.spi.config.Named;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationContext;

import java.util.List;

import br.com.configurations.ApplicationContextProvider;
import br.com.galhardi.backend.models.Instituicao;
import br.com.galhardi.backend.models.MyImage;
import br.com.galhardi.backend.models.Response;
import br.com.galhardi.backend.models.Voluntario;
import br.com.galhardi.backend.services.ImgManager;
import br.com.galhardi.backend.services.ImgManagerI;
import br.com.galhardi.backend.services.InstituicaoManager;
import br.com.galhardi.backend.services.Manager;
import br.com.galhardi.backend.services.VoluntarioManager;
import br.com.galhardi.backend.services.VoluntarioManagerI;

@Api(name = "instituicaoEndpoint", version = "v1",
        namespace = @ApiNamespace(ownerDomain = "api.instituicao.galhardi.com.br",
                ownerName = "api.instituicaos.galhardi.com.br",
                packagePath = ""))
public class InstituicaoEndpoint {

    @Qualifier(value = "InstituicaoManagerS")
    Manager<Instituicao> instituicaoMNG;

    @Qualifier(value = "ImgManagerS")
    ImgManagerI imageMNG;

    @Qualifier(value = "VoluntarioManagerS")
    VoluntarioManagerI voluntarioMNG;

    public InstituicaoEndpoint() {
        ApplicationContextProvider applicationContextProvider = new ApplicationContextProvider();
        ApplicationContext appCtxt = applicationContextProvider.getApplicationContext();
        instituicaoMNG = appCtxt.getBean(InstituicaoManager.class);
        imageMNG = appCtxt.getBean(ImgManager.class);
        voluntarioMNG = appCtxt.getBean(VoluntarioManager.class);
    }

    @ApiMethod(name = "insertInstituicao")
    public Voluntario createE(Instituicao obj) {
        MyImage myImage = new MyImage();
        myImage.setImagem(obj.getImgStrTEMP());
        obj.setImgStrTEMP(null);
        Integer id_inst;
        try {
            id_inst = instituicaoMNG.create(obj);
            myImage.setId_instituicao(id_inst);
            try {
                imageMNG.armazena(myImage);

                voluntarioMNG.setMembroERepEAssInst(true, true, obj, obj.getUserIdTEMP());

                return voluntarioMNG.read(obj.getUserIdTEMP());
            } catch (Exception e) {
                return null;
            }
        } catch (Exception e) {
            return null;
        }
    }

    @ApiMethod(name = "updateInstituicao")
    public Response updateE(Instituicao obj) {
        MyImage myImage = new MyImage();
        myImage.setImagem(obj.getImgStrTEMP());
        obj.setImgStrTEMP(null);
        Integer id_inst = obj.getId();
        Response response = new Response();
        try {
            instituicaoMNG.update(obj);
            try {
                imageMNG.deleta(id_inst);
                myImage.setId_instituicao(id_inst);
                imageMNG.armazena(myImage);
                response.setBooleanResp(true);
                return response;
            } catch (Exception e) {
                response.setBooleanResp(false);
                response.setOriginalException(e.getMessage());
                response.setErrorStrMsg("InsertInstitutionImageException");
                return response;
            }
        } catch (Exception e) {
            response.setBooleanResp(false);
            response.setOriginalException(e.getMessage());
            response.setErrorStrMsg("InsertInstitutionException");
            return response;
        }
    }

    @ApiMethod(name = "removeInstituicao")
    public Response deleteE(@Named("id") Integer id) {
        Response response = new Response();
        try {
            imageMNG.deleta(id);
            instituicaoMNG.delete(id);
            response.setBooleanResp(true);
        } catch (Exception e) {
            response.setBooleanResp(false);
            response.setOriginalException(e.getMessage());
            response.setErrorStrMsg("DeleteInstitutionException");
        }
        return response;
    }

    @ApiMethod(name = "listInstituicao")
    public List<Instituicao> listE() {
        try {
            return instituicaoMNG.list();
        } catch (Exception e) {
            return null;
        }
    }

}
