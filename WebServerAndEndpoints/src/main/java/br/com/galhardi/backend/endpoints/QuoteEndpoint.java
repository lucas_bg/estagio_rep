package br.com.galhardi.backend.endpoints;

import com.google.api.server.spi.config.Api;
import com.google.api.server.spi.config.ApiMethod;
import com.google.api.server.spi.config.ApiNamespace;
import com.google.api.server.spi.config.Named;

import org.springframework.beans.factory.annotation.Qualifier;

import java.util.List;

import br.com.configurations.ApplicationContextProvider;
import br.com.galhardi.backend.models.Quote;
import br.com.galhardi.backend.services.Manager;
import br.com.galhardi.backend.services.QuoteManager;

@Api(name = "quoteEndpoint", version = "v1",
        namespace = @ApiNamespace(ownerDomain = "api.quotes.galhardi.com.br",
                ownerName = "api.quotes.galhardi.com.br",
                packagePath = ""))
public class QuoteEndpoint {

    @Qualifier(value = "QuoteManagerS")
    Manager<Quote> quoteMNG;

    public QuoteEndpoint() {
        ApplicationContextProvider applicationContextProvider = new ApplicationContextProvider();
        quoteMNG = applicationContextProvider.getApplicationContext().getBean(QuoteManager.class);
    }

    @ApiMethod(name = "insertQuote")
    public void createE(Quote obj) {
        quoteMNG.create(obj);
    }

    @ApiMethod(name = "getQuote")
    public Quote readE(@Named("id") Integer id) {
        return quoteMNG.read(id);
    }

    @ApiMethod(name = "updateQuote")
    public void updateE(Quote obj) {
        quoteMNG.update(obj);
    }

    @ApiMethod(name = "removeQuote")
    public void deleteE(@Named("id") Integer id) {
        quoteMNG.delete(id);
    }

    @ApiMethod(name = "listQuote")
    public List<Quote> listE() {
        return quoteMNG.list();
    }

}
