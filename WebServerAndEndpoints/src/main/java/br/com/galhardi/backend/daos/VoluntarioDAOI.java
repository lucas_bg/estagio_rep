package br.com.galhardi.backend.daos;

import java.util.List;

import br.com.galhardi.backend.models.Instituicao;
import br.com.galhardi.backend.models.Voluntario;

public interface VoluntarioDAOI extends DAO<Voluntario> {

    boolean authenticateUserD(String email, String password, boolean onlyEmail);

    boolean setGoogleIdD(String email, String gId);

    boolean insertNewVolunteerD(Voluntario v);

    boolean deleteUserByEmailD(String email, String password, Integer id);

    Voluntario readUserByEmailD(String email);

    void setMembroERepEAssInstD(boolean m, boolean r, Instituicao a, Integer id);

    List<Instituicao> admGetInstsNotAprovD();

    boolean admAprovarInstD(Integer idInst);

    boolean associarAInstD(Integer userId, Integer instId);

    List<Voluntario> findVolsReqsD();

    boolean aprovaReqAssVolD(Integer idVol);

    boolean reprovaReqAssVolD(Integer idVol);

}
