package br.com.galhardi.backend.services;

import java.util.List;

import br.com.galhardi.backend.models.Instituicao;
import br.com.galhardi.backend.models.Voluntario;

public interface VoluntarioManagerI extends Manager<Voluntario> {

    boolean authenticateUser(String email, String password, boolean onlyEmail);

    boolean setGoogleId(String email, String gId);

    boolean insertNewVolunteer(Voluntario v);

    boolean deleteUserByEmail(String email, String password, Integer id);

    Voluntario readUserByEmail(String email);

    void setMembroERepEAssInst(boolean m, boolean r, Instituicao a, Integer id);

    List<Instituicao> admGetInstsNotAprov();

    boolean admAprovarInst(Integer idInst);

    boolean associarAInst(Integer userId, Integer instId);

    List<Voluntario> findVolsReqs();

    boolean aprovaReqAssVol(Integer idVol);

    boolean reprovaReqAssVol(Integer idVol);

}
