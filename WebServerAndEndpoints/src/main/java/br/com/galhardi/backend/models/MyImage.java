package br.com.galhardi.backend.models;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class MyImage {

    @Id
    @GeneratedValue(generator = "increment")
    @GenericGenerator(name = "increment", strategy = "increment")
    private Integer id;

    @Column(columnDefinition = "mediumblob")
    private byte[] imagem;

    private Integer id_instituicao;

    public Integer getId_instituicao() {
        return id_instituicao;
    }

    public void setId_instituicao(Integer id_instituicao) {
        this.id_instituicao = id_instituicao;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public byte[] getImagem() {
        return imagem;
    }

    public void setImagem(byte[] imagem) {
        this.imagem = imagem;
    }
}
