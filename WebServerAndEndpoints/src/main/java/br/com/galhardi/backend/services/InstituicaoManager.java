package br.com.galhardi.backend.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.logging.Logger;

import br.com.galhardi.backend.daos.DAO;
import br.com.galhardi.backend.models.Instituicao;

@Service(value = "InstituicaoManagerS")
public class InstituicaoManager implements Manager<Instituicao> {

    private static final Logger log = Logger.getLogger("InstituicaoManager");

    @Autowired
    @Qualifier(value = "InstituicaoDAOR")
    DAO<Instituicao> dao;

    public Integer create(Instituicao obj) {
        return dao.createD(obj);
    }

    public Instituicao read(Integer id) {
        return dao.readD(id);
    }

    public boolean update(Instituicao obj) {
        return dao.updateD(obj);
    }

    public void delete(Integer id) {
        dao.deleteD(id);
    }

    public List<Instituicao> list() {
        return dao.listD();
    }

}
