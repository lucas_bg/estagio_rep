package br.com.galhardi.backend.daos;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import br.com.galhardi.backend.models.Quote;

@Repository(value = "QuoteDAOR")
@Transactional
public class QuoteDAO implements DAO<Quote> {

    @PersistenceContext
    private EntityManager manager;

    public Integer createD(Quote obj) {
        manager.persist(obj);
        manager.flush();
        return obj.getId();
    }

    public Quote readD(Integer id) {
        return manager.find(Quote.class, id);
    }

    public boolean updateD(Quote obj) {
        manager.merge(obj);
        return true;
    }

    public void deleteD(Integer id) {
        Quote quote = manager.find(Quote.class, id);
        manager.remove(quote);
    }

    public List<Quote> listD() {
        return manager.createQuery(
            "SELECT q FROM Quote q",
            Quote.class)
            .getResultList();
    }

}
