package br.com.galhardi.backend.models.Enums;

public enum CategoriaAtiv {
    IDOSOS,
    CRIANCAS,
    MEIO_AMBIENTE,
    ADOLESCENTES,
    FE,
    GERAL
}
