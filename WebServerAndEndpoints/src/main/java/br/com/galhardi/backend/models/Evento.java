package br.com.galhardi.backend.models;

import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("Evento")
public class Evento extends Atividade {

    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private Date diaEHora;

    private int duracao;

    public Date getDiaEHora() {
        return diaEHora;
    }

    public void setDiaEHora(Date diaEHora) {
        this.diaEHora = diaEHora;
    }

    public int getDuracao() {
        return duracao;
    }

    public void setDuracao(int duracao) {
        this.duracao = duracao;
    }
}
