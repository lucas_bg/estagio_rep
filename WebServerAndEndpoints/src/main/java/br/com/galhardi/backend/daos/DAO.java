package br.com.galhardi.backend.daos;

import java.util.List;

public interface DAO<T> {

    Integer createD(T obj);

    T readD(Integer id);

    boolean updateD(T obj);

    void deleteD(Integer id);

    List<T> listD();

}