package br.com.galhardi.backend.services;

import java.util.List;

import br.com.galhardi.backend.models.Doacao;

public interface DoacaoManagerI extends Manager<Doacao> {

    void realizar(Integer id);

    List<Doacao> doacoesRealizadasPorUsuario(Integer id);

}
