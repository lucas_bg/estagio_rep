package br.com.galhardi.backend.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.logging.Logger;

import br.com.galhardi.backend.daos.VoluntarioDAOI;
import br.com.galhardi.backend.models.Instituicao;
import br.com.galhardi.backend.models.Voluntario;

@Service(value = "VoluntarioManagerS")
public class VoluntarioManager implements VoluntarioManagerI {

    private static final Logger log = Logger.getLogger("VoluntarioManager");

    @Autowired
    @Qualifier(value = "VoluntarioDAOR")
    VoluntarioDAOI dao;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    public Integer create(Voluntario obj) {
        obj.setSenha(bCryptPasswordEncoder.encode(obj.getSenha()));
        return dao.createD(obj);
    }

    public Voluntario read(Integer id) {
        return dao.readD(id);
    }

    public boolean update(Voluntario obj) {
        obj.setSenha(bCryptPasswordEncoder.encode(obj.getSenha()));
        return dao.updateD(obj);
    }

    public void delete(Integer id) {
        dao.deleteD(id);
    }

    public List<Voluntario> list() {
        return dao.listD();
    }

    @Override
    public boolean authenticateUser(String email, String password, boolean onlyEmail) {
        return dao.authenticateUserD(email, password, onlyEmail);
    }

    @Override
    public boolean setGoogleId(String email, String gId) {
        return dao.setGoogleIdD(email, gId);
    }

    @Override
    public boolean insertNewVolunteer(Voluntario v) {
        v.setSenha(bCryptPasswordEncoder.encode(v.getSenha()));
        return dao.insertNewVolunteerD(v);
    }

    @Override
    public boolean deleteUserByEmail(String email, String password, Integer id) {
        return dao.deleteUserByEmailD(email, password, id);
    }

    @Override
    public Voluntario readUserByEmail(String email) {
        return dao.readUserByEmailD(email);
    }

    @Override
    public void setMembroERepEAssInst(boolean m, boolean r, Instituicao a, Integer id) {
        dao.setMembroERepEAssInstD(m, r, a, id);
    }

    @Override
    public List<Instituicao> admGetInstsNotAprov() {
        return dao.admGetInstsNotAprovD();
    }

    @Override
    public boolean admAprovarInst(Integer idInst) {
        return dao.admAprovarInstD(idInst);
    }

    @Override
    public boolean associarAInst(Integer userId, Integer instId) {
        return dao.associarAInstD(userId, instId);
    }

    @Override
    public List<Voluntario> findVolsReqs() {
        return dao.findVolsReqsD();
    }

    @Override
    public boolean aprovaReqAssVol(Integer idVol) {
        return dao.aprovaReqAssVolD(idVol);
    }

    @Override
    public boolean reprovaReqAssVol(Integer idVol) {
        return dao.reprovaReqAssVolD(idVol);
    }

}
