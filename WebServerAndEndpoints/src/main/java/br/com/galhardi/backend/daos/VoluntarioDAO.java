package br.com.galhardi.backend.daos;

import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.logging.Logger;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import br.com.galhardi.backend.models.Instituicao;
import br.com.galhardi.backend.models.Voluntario;

@Repository(value = "VoluntarioDAOR")
@Transactional
public class VoluntarioDAO implements VoluntarioDAOI {

    private static final Logger log = Logger.getLogger("VoluntarioDAO");

    @PersistenceContext
    private EntityManager manager;

    public Integer createD(Voluntario obj) {
        manager.persist(obj);
        manager.flush();
        return obj.getId();
    }

    public Voluntario readD(Integer id) {
        return manager.find(Voluntario.class, id);
    }

    public boolean updateD(Voluntario obj) {
        try {
            Voluntario v = manager.find(Voluntario.class, obj.getId());
            Instituicao i1 = null;
            boolean isMem = v.isMemInst();
            boolean isRep = v.isRepInst();
            if (v.getInstAss() != null) {
                i1 = new Instituicao();
                Instituicao i2 = v.getInstAss();
                BeanUtils.copyProperties(i2, i1);
            }
            BeanUtils.copyProperties(obj, v);
            v.setIsRepInst(isRep);
            v.setIsMemInst(isMem);
            v.setInstAss(i1);
            manager.merge(v);
            manager.flush();
            return true;
        } catch (Exception exc) {
            return false;
        }
    }

    public void deleteD(Integer id) {
        Voluntario voluntario = manager.find(Voluntario.class, id);
        manager.remove(voluntario);
    }

    public List<Voluntario> listD() {
        return manager.createQuery(
                "SELECT v FROM Voluntario v",
                Voluntario.class)
                .getResultList();
    }

    @Override
    public boolean authenticateUserD(String email, String password, boolean onlyEmail) {
        String consulta = "SELECT v FROM Voluntario v WHERE v.email = :em";
        TypedQuery<Voluntario> query = manager.createQuery(consulta, Voluntario.class);
        query.setParameter("em", email);
        Voluntario v;
        try {
            v = query.getSingleResult();
        } catch (NoResultException ex) {
            v = null;
        }
        if (onlyEmail) {
            return v != null;
        } else {
            if (v != null) {
                return v.getEmail().equals(email) && v.getSenha().equals(password);
            } else {
                return false;
            }
        }
    }

    @Override
    public boolean setGoogleIdD(String email, String gId) {
        String updateQuery = "UPDATE Voluntario v SET v.id_GOOGLE = :paramGId WHERE v.email = :paramEmail";
        Query query = manager.createQuery(updateQuery);
        query.setParameter("paramGId", gId);
        query.setParameter("paramEmail", email);
        if (query.executeUpdate() == 1) {
            log.info("SUCESSO AO UPDATE GOOGLE ID");
            return true;
        } else {
            log.severe("ERROR2 UPDATING GOOGLE ID");
            return false;
        }
    }

    @Override
    public boolean insertNewVolunteerD(Voluntario v) {
        manager.persist(v);
        return true;
    }

    @Override
    public boolean deleteUserByEmailD(String email, String password, Integer user_id) {
//ALTERA3
//        Integer instituicao_id = idMinhaInstD(user_id);
        Voluntario vol = manager.find(Voluntario.class, user_id);

        //FUTURE excluir as atividades tambem
        if (vol.getInstAss() != null && vol.isRepInst()) {
            Integer instituicao_id = vol.getInstAss().getId();

            String update1 = "UPDATE Voluntario v SET v.instAss = NULL, " +
                    "v.isMemInst = FALSE, v.isRepInst = FALSE " +
                    "WHERE v.instAss.id = :paramIdInstituicao";
            Query query4 = manager.createQuery(update1);
            query4.setParameter("paramIdInstituicao", instituicao_id);
            try {
                query4.executeUpdate();
            } catch (Exception e) {
                return false;
            }

            String delete2 = "DELETE FROM Instituicao i WHERE i.id = :paramId";
            String delete3 = "DELETE MyImage m WHERE m.id_instituicao = :paramIdInstituicao";
            Query query2 = manager.createQuery(delete2);
            query2.setParameter("paramId", instituicao_id);
            Query query3 = manager.createQuery(delete3);
            query3.setParameter("paramIdInstituicao", instituicao_id);
            try {
                query2.executeUpdate();
                query3.executeUpdate();
            } catch (Exception e) {
                return false;
            }
        }

        String delete1 = "DELETE FROM Doacao d WHERE d.criador.id = :paramId";
        Query query1 = manager.createQuery(delete1);
        query1.setParameter("paramId", user_id);
        try {
            query1.executeUpdate();
        } catch (Exception e) {
            return false;
        }

        String consulta = "SELECT v FROM Voluntario v WHERE v.email = :em";
        TypedQuery<Voluntario> query = manager.createQuery(consulta, Voluntario.class);
        query.setParameter("em", email);
        Voluntario v;
        try {
            v = query.getSingleResult();
        } catch (NoResultException ex) {
            v = null;
        }
        if (v != null) {
            try {
                manager.remove(v);
                manager.flush();
                return true;
            } catch (Exception e) {
                return false;
            }
        } else {
            return false;
        }
    }

    @Override
    public Voluntario readUserByEmailD(String email) {
        String consulta = "SELECT v FROM Voluntario v WHERE v.email = :em";
        TypedQuery<Voluntario> query = manager.createQuery(consulta, Voluntario.class);
        query.setParameter("em", email);
        Voluntario v;
        try {
            v = query.getSingleResult();
        } catch (NoResultException ex) {
            v = null;
        }
        return v;
    }

    @Override
    public void setMembroERepEAssInstD(boolean m, boolean r, Instituicao a, Integer id) {
        Voluntario user = manager.find(Voluntario.class, id);
        user.setIsMemInst(m);
        user.setIsRepInst(r);
        Instituicao inst;
        if (a != null)
            inst = manager.find(Instituicao.class, a.getId());
        else
            inst = null;

        user.setInstAss(inst);

        manager.merge(user);
        manager.flush();
    }

    public List<Instituicao> admGetInstsNotAprovD() {
        return manager.createQuery("SELECT i from Instituicao i WHERE i.aprovada = FALSE"
                , Instituicao.class).getResultList();
    }

    public boolean admAprovarInstD(Integer idInst) {
        Query query = manager.createQuery("UPDATE Instituicao i SET " +
                "i.aprovada = TRUE WHERE i.id = :paramIdInst");
        query.setParameter("paramIdInst", idInst);

        try {
            query.executeUpdate();
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    public boolean associarAInstD(Integer userId, Integer instId) {
        try {
            Voluntario user = manager.find(Voluntario.class, userId);
            Instituicao inst = manager.find(Instituicao.class, instId);
            user.setInstAss(inst);
            user.setIsRepInst(false);
            manager.merge(user);
            manager.flush();

            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public List<Voluntario> findVolsReqsD() {
        String consulta = "SELECT v FROM Voluntario v WHERE v.instAss IS NOT NULL AND v.isMemInst = FALSE";
        TypedQuery<Voluntario> query = manager.createQuery(consulta, Voluntario.class);

        try {
            return query.getResultList();
        } catch (Exception e) {
            return null;
        }
    }

    public boolean aprovaReqAssVolD(Integer idVol) {
        Query query = manager.createQuery("UPDATE Voluntario v SET " +
                "v.isMemInst = TRUE WHERE v.id = :paramIdVol");
        query.setParameter("paramIdVol", idVol);

        try {
            query.executeUpdate();
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    public boolean reprovaReqAssVolD(Integer idVol) {
        Query query = manager.createQuery("UPDATE Voluntario v SET " +
                "v.instAss = NULL WHERE v.id = :paramIdVol");
        query.setParameter("paramIdVol", idVol);
        setMembroERepEAssInstD(false, false, null, idVol);

        try {
            query.executeUpdate();
        } catch (Exception e) {
            return false;
        }
        return true;
    }

}
