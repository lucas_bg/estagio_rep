package br.com.galhardi.backend.daos;

import org.hibernate.Hibernate;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import br.com.galhardi.backend.models.Atividade;
import br.com.galhardi.backend.models.Enums.StatusAtiv;
import br.com.galhardi.backend.models.Evento;
import br.com.galhardi.backend.models.Funcao;
import br.com.galhardi.backend.models.Instituicao;
import br.com.galhardi.backend.models.Projeto;
import br.com.galhardi.backend.models.Voluntario;

@Repository(value = "AtividadeDAOR")
@Transactional
public class AtividadeDAO implements AtividadeDAOI {

    private static final Logger log = Logger.getLogger("AtividadeDAO");

    @PersistenceContext
    private EntityManager manager;

    @Override
    public Integer createD(Atividade obj) {
        Voluntario v = manager.find(Voluntario.class, obj.getCriador().getId());
        Instituicao i = manager.find(Instituicao.class, obj.getInst_pertc().getId());
        obj.setInst_pertc(i);
        obj.setCriador(v);
        manager.persist(obj);
        manager.flush();
        return obj.getId();
    }

    @Override
    public Atividade readD(Integer id) {
        Atividade atv = manager.find(Atividade.class, id);

        if (atv instanceof Projeto) {
            Projeto proj = (Projeto) atv;
            Hibernate.initialize(proj.getDias_ocorr());
            return proj;
        } else if (atv instanceof Funcao) {
            Funcao func = (Funcao) atv;
            Hibernate.initialize(func.getDias_poss());
            return func;
        }

        return atv;
    }

    @Override
    public boolean updateD(Atividade obj) {
        try {
            Atividade d = manager.find(Atividade.class, obj.getId());
            BeanUtils.copyProperties(obj, d);
            manager.merge(obj);
            manager.flush();
            return true;
        } catch (Exception exc) {
            return false;
        }
    }

    @Override
    public void deleteD(Integer id) {
        Atividade atividade = manager.find(Atividade.class, id);
        manager.remove(atividade);
        manager.flush();
    }

    @Override
    public List<Atividade> listD() {
        String query = "SELECT a FROM Atividade a WHERE a.status = :paramStt";
        TypedQuery<Atividade> query1 = manager.createQuery(query, Atividade.class);
        query1.setParameter("paramStt", StatusAtiv.APROVADA);

        List<Atividade> lista = query1.getResultList();
        List<Atividade> listaa = new ArrayList<>();

        for (Atividade a : lista) {
            if (a instanceof Evento) {
                Evento e = (Evento) a;
                if (e.getDiaEHora().after(new Date())) {
                    listaa.add(e);
                }
            } else if (a instanceof Projeto) {
                Projeto p = (Projeto) a;
                if (p.getDt_inicio().after(new Date())) {
                    listaa.add(p);
                }
            } else {
                listaa.add(a);
            }
        }

        return listaa;
    }

    @Override
    public List<Atividade> listAtividadesNaoAprovadasD(Integer instId) {
        String query = "SELECT a FROM Atividade a WHERE a.status = :paramStt" +
                " AND a.inst_pertc.id = :paramInstId";
        TypedQuery<Atividade> query1 = manager.createQuery(query, Atividade.class);
        query1.setParameter("paramStt", StatusAtiv.NAO_ANALISADA);
        query1.setParameter("paramInstId", instId);

        return query1.getResultList();
    }

    @Override
    public boolean aprovarAtividadeD(Integer id) {
        try {
            Atividade atv = manager.find(Atividade.class, id);
            atv.setStatus(StatusAtiv.APROVADA);
            manager.merge(atv);
            manager.flush();
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @Override
    public boolean reprovarAtividadeD(Integer id) {
        try {
            Atividade atv = manager.find(Atividade.class, id);
            atv.setStatus(StatusAtiv.REPROVADA);
            manager.merge(atv);
            manager.flush();
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @Override
    public List<Atividade> listaAtividadesMinhaInstD(Integer instId) {
        String query = "SELECT a FROM Atividade a" +
                " WHERE a.inst_pertc.id = :paramInstId";
        TypedQuery<Atividade> query1 = manager.createQuery(query, Atividade.class);
        query1.setParameter("paramInstId", instId);

        return query1.getResultList();
    }

    @Override
    public List<Atividade> listaAtividadesEuCrieiD(Integer memId) {
        String query = "SELECT a FROM Atividade a" +
                " WHERE a.criador.id = :paramMemId";
        TypedQuery<Atividade> query1 = manager.createQuery(query, Atividade.class);
        query1.setParameter("paramMemId", memId);

        return query1.getResultList();
    }
}
