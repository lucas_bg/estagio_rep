package br.com.galhardi.backend.models;

import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;
import java.util.List;

import javax.persistence.DiscriminatorValue;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

import br.com.galhardi.backend.models.Enums.DiasDaSemana;

@Entity
@DiscriminatorValue("Projeto")
public class Projeto extends Atividade {

    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private Date dt_inicio;

    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private Date dt_termino;

    @ElementCollection(targetClass = DiasDaSemana.class)
    @Enumerated(EnumType.STRING)
    private List<DiasDaSemana> dias_ocorr;

    public Date getDt_inicio() {
        return dt_inicio;
    }

    public void setDt_inicio(Date dt_inicio) {
        this.dt_inicio = dt_inicio;
    }

    public Date getDt_termino() {
        return dt_termino;
    }

    public void setDt_termino(Date dt_termino) {
        this.dt_termino = dt_termino;
    }

    public List<DiasDaSemana> getDias_ocorr() {
        return dias_ocorr;
    }

    public void setDias_ocorr(List<DiasDaSemana> dias_ocorr) {
        this.dias_ocorr = dias_ocorr;
    }
}
