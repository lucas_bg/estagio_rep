<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="description" content="A front-end template that helps you build fast, modern mobile web apps.">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0">

        <link rel="shortcut icon" href="/img/favicon.ico">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:regular,bold,italic,thin,light,bolditalic,black,medium&amp;lang=en">
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
        <link rel="stylesheet" href="/css/material.css">
        <link rel="stylesheet" href="/css/styles.css">

        <title>Angelos ADM</title>
    </head>

    <body class="mdl-demo mdl-color--grey-200 mdl-color-text--grey-700 mdl-base">
        <div class="mdl-layout mdl-js-layout mdl-layout--fixed-header">

            <c:import url="/WEB-INF/views/header.jsp" />

            <main class="mdl-layout__content">
                <!--///////////////////////////////////////-->
                <!--////////////////CONTENT////////////////-->
                <!--///////////////////////////////////////-->
                <div class="text-for-profile">
                Instituições não aprovadas
                <table class="mdl-data-table mdl-js-data-table mdl-shadow--2dp d-tables">
                  <thead>
                    <tr>
                      <th class="mdl-data-table__cell--non-numeric">Nome</th>
                      <th class="mdl-data-table__cell--non-numeric">Aprovar</th>
                    </tr>
                  </thead>
                  <tbody>
                    <c:forEach items="${instList}" var="inst">
                        <tr>
                          <td class="mdl-data-table__cell--non-numeric d-desc">${inst.nome}</td>
                          <td class="mdl-data-table__cell--non-numeric">
                            <a href="/aproveInst?id=${inst.id}">Aprovar</a>
                          </td>
                        </tr>
                    </c:forEach>
                  </tbody>
                </table>
                </div>
                <!--///////////////////////////////////////-->
                <!--////////////////CONTENT////////////////-->
                <!--///////////////////////////////////////-->
            </main>

            <c:import url="/WEB-INF/views/footer.jsp" />

        </div>
        <script src="https://code.getmdl.io/1.2.1/material.min.js"></script>
    </body>
</html>
