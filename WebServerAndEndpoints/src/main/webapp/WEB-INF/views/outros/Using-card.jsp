<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="description" content="A front-end template that helps you build fast, modern mobile web apps.">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0">

        <link rel="shortcut icon" href="/img/favicon.ico">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:regular,bold,italic,thin,light,bolditalic,black,medium&amp;lang=en">
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
        <link rel="stylesheet" href="/css/material.css">
        <link rel="stylesheet" href="/css/styles.css">

        <title>Angelos Home</title>
    </head>

    <body class="mdl-demo mdl-color--grey-200 mdl-color-text--grey-700 mdl-base">
        <div class="mdl-layout mdl-js-layout mdl-layout--fixed-header">

            <c:import url="header.jsp" />

            <main class="mdl-layout__content">
                    <!--///////////////////////////////////////-->
                    <!--////////////////CONTENT////////////////-->
                    <!--///////////////////////////////////////-->
                    <section class="section--center mdl-grid mdl-grid--no-spacing mdl-shadow--2dp">
                        <div class="mdl-card mdl-cell mdl-cell--12-col">
                            <div class="mdl-card__supporting-text mdl-grid mdl-grid--no-spacing">

                                <h4 class="mdl-cell mdl-cell--12-col">Londrina Pazeando</h4>

                                <div class="section__circle-container mdl-cell mdl-cell--4-col">
                                    <img class="section__circle-container__circle " src="img/logo_pazeando.jpg" alt="logo instituicao">
                                </div>

                                <div class="section__text mdl-cell mdl-cell--8-col">
                                    <h5>O Movimento Pela Paz e Não-Violência - Londrina Pazeando - 
                                        Trabalha para desenvolver uma Cultura de Paz e Não-Violência.</h5>
                                    O Movimento Pela Paz e Não-Violência, mais conhecido como Londrina Pazeando, 
                                    é uma organização que foi criada para desenvolver uma Cultura de Paz e Não-Violência. 
                                    É uma instituição sem fins lucrativos com independência administrativa e 
                                    financeira, como organização da sociedade civil de interesse público, 
                                    regendo-se por um estatuto baseado na Lei Federal nº 9.790/99 e decreto 
                                    federal nº 3.100/99 - OSCIP.
                                </div>

                            </div>

                            <div class="mdl-card__actions">
                                <a href="#" class="mdl-button">Leia Mais</a>
                            </div>
                        </div>

                        <button class="mdl-button mdl-js-button mdl-js-ripple-effect mdl-button--icon" id="btn2">
                            <i class="material-icons">more_vert</i>
                        </button>
                        <ul class="mdl-menu mdl-js-menu mdl-menu--bottom-right" for="btn2">
                            <li class="mdl-menu__item">Lorem</li>
                            <li class="mdl-menu__item" disabled>Ipsum</li>
                            <li class="mdl-menu__item">Dolor</li>
                        </ul>

                    </section>

                    <section class="section--center mdl-grid mdl-grid--no-spacing mdl-shadow--2dp">
                        <div class="mdl-card mdl-cell mdl-cell--12-col">
                            <div class="mdl-card__supporting-text mdl-grid mdl-grid--no-spacing">

                                <h4 class="mdl-cell mdl-cell--12-col">Londrina Pazeando</h4>

                                <div class="section__circle-container mdl-cell mdl-cell--4-col">
                                    <img class="section__circle-container__circle " src="img/logo_pazeando.jpg" alt="logo instituicao">
                                </div>

                                <div class="section__text mdl-cell mdl-cell--8-col">
                                    <h5>O Movimento Pela Paz e Não-Violência - Londrina Pazeando - 
                                        Trabalha para desenvolver uma Cultura de Paz e Não-Violência.</h5>
                                    O Movimento Pela Paz e Não-Violência, mais conhecido como Londrina Pazeando, 
                                    é uma organização que foi criada para desenvolver uma Cultura de Paz e Não-Violência. 
                                    É uma instituição sem fins lucrativos com independência administrativa e 
                                    financeira, como organização da sociedade civil de interesse público, 
                                    regendo-se por um estatuto baseado na Lei Federal nº 9.790/99 e decreto 
                                    federal nº 3.100/99 - OSCIP.
                                </div>

                            </div>

                            <div class="mdl-card__actions">
                                <a href="#" class="mdl-button">Leia Mais</a>
                            </div>
                        </div>

                        <button class="mdl-button mdl-js-button mdl-js-ripple-effect mdl-button--icon" id="btn3">
                            <i class="material-icons">more_vert</i>
                        </button>
                        <ul class="mdl-menu mdl-js-menu mdl-menu--bottom-right" for="btn3">
                            <li class="mdl-menu__item">Lorem</li>
                            <li class="mdl-menu__item" disabled>Ipsum</li>
                            <li class="mdl-menu__item">Dolor</li>
                        </ul>

                    </section>

                    <section class="section--center mdl-grid mdl-grid--no-spacing mdl-shadow--2dp">
                        <div class="mdl-card mdl-cell mdl-cell--12-col">
                            <div class="mdl-card__supporting-text mdl-grid mdl-grid--no-spacing">

                                <h4 class="mdl-cell mdl-cell--12-col">Londrina Pazeando</h4>

                                <div class="section__circle-container mdl-cell mdl-cell--4-col">
                                    <img class="section__circle-container__circle " src="img/logo_pazeando.jpg" alt="logo instituicao">
                                </div>

                                <div class="section__text mdl-cell mdl-cell--8-col">
                                    <h5>O Movimento Pela Paz e Não-Violência - Londrina Pazeando - 
                                        Trabalha para desenvolver uma Cultura de Paz e Não-Violência.</h5>
                                    O Movimento Pela Paz e Não-Violência, mais conhecido como Londrina Pazeando, 
                                    é uma organização que foi criada para desenvolver uma Cultura de Paz e Não-Violência. 
                                    É uma instituição sem fins lucrativos com independência administrativa e 
                                    financeira, como organização da sociedade civil de interesse público, 
                                    regendo-se por um estatuto baseado na Lei Federal nº 9.790/99 e decreto 
                                    federal nº 3.100/99 - OSCIP.
                                </div>

                            </div>

                            <div class="mdl-card__actions">
                                <a href="#" class="mdl-button">Leia Mais</a>
                            </div>
                        </div>

                        <button class="mdl-button mdl-js-button mdl-js-ripple-effect mdl-button--icon" id="btn4">
                            <i class="material-icons">more_vert</i>
                        </button>
                        <ul class="mdl-menu mdl-js-menu mdl-menu--bottom-right" for="btn4">
                            <li class="mdl-menu__item">Lorem</li>
                            <li class="mdl-menu__item" disabled>Ipsum</li>
                            <li class="mdl-menu__item">Dolor</li>
                        </ul>

                    </section>

                    <c:forEach items="${listaInsts}" var="inst">
                        <section class="section--center mdl-grid mdl-grid--no-spacing mdl-shadow--2dp">
                            <div class="mdl-card mdl-cell mdl-cell--12-col">
                                <div class="mdl-card__supporting-text mdl-grid mdl-grid--no-spacing">

                                    <h4 class="mdl-cell mdl-cell--12-col">${inst.nome}</h4>

                                    <div class="section__circle-container mdl-cell mdl-cell--4-col">
                                        <img class="section__circle-container__circle " src="img/logo_pazeando.jpg" alt="logo instituicao">
                                    </div>

                                    <div class="section__text mdl-cell mdl-cell--8-col">
                                        <h5>O Movimento Pela Paz e Não-Violência - Londrina Pazeando - 
                                            Trabalha para desenvolver uma Cultura de Paz e Não-Violência.</h5>
                                        ${inst.apresentacao}
                                    </div>

                                </div>

                                <div class="mdl-card__actions">
                                    <a href="#" class="mdl-button">Leia Mais</a>
                                </div>
                            </div>

                            <button class="mdl-button mdl-js-button mdl-js-ripple-effect mdl-button--icon" id="${inst.id}">
                                <i class="material-icons">more_vert</i>
                            </button>
                            <ul class="mdl-menu mdl-js-menu mdl-menu--bottom-right" for="${inst.id}">
                                <li class="mdl-menu__item">Lorem</li>
                                <li class="mdl-menu__item" disabled>Ipsum</li>
                                <li class="mdl-menu__item">Dolor</li>
                            </ul>

                        </section>
                    </c:forEach>
                    <!--///////////////////////////////////////-->
                    <!--////////////////CONTENT////////////////-->
                    <!--///////////////////////////////////////-->
            </main>

            <c:import url="footer.jsp" />

        </div>
        <a href="/">
            <button class="mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect mdl-button--colored mdl-shadow--4dp mdl-color--accent add-button">
                <i class="material-icons" role="presentation">add</i>
                <span class="visuallyhidden">Add</span>
            </button>
        </a>
        <script src="https://code.getmdl.io/1.2.1/material.min.js"></script>
    </body>
</html>
