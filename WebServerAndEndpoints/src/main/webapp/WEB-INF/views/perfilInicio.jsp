<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="description" content="A front-end template that helps you build fast, modern mobile web apps.">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0">

        <link rel="shortcut icon" href="/img/favicon.ico">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:regular,bold,italic,thin,light,bolditalic,black,medium&amp;lang=en">
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
        <link rel="stylesheet" href="/css/material.css">
        <link rel="stylesheet" href="/css/styles.css">

        <title>Angelos Home</title>
    </head>

    <body class="mdl-demo mdl-color--grey-200 mdl-color-text--grey-700 mdl-base">
        <div class="mdl-layout mdl-js-layout mdl-layout--fixed-header">

            <c:import url="/WEB-INF/views/header.jsp" />

            <main class="mdl-layout__content">
                <!--///////////////////////////////////////-->
                <!--////////////////CONTENT////////////////-->
                <!--///////////////////////////////////////-->
                <div class="all-space-perfil">
                <div class="left-side">
                    <div class="text-for-profile">
                    Meus Pedidos de Doação
                    <table class="mdl-data-table mdl-js-data-table mdl-shadow--2dp d-tables">
                      <thead>
                        <tr>
                          <th class="mdl-data-table__cell--non-numeric">Título</th>
                          <th class="mdl-data-table__cell--non-numeric">Descrição</th>
                          <th class="mdl-data-table__cell--non-numeric">Data criação</th>
                          <th class="mdl-data-table__cell--non-numeric">Data realizada</th>
                        </tr>
                      </thead>
                      <tbody>
                        <c:forEach items="${listaPEDIDOS}" var="p">
                            <tr>
                            <fmt:formatDate pattern="dd/MM/yyyy" value="${p.data_criacao}" var="strDate1" />
                            <fmt:formatDate pattern="dd/MM/yyyy" value="${p.data_realizada}" var="strDate2" />
                              <td class="mdl-data-table__cell--non-numeric d-desc">${p.titulo}</td>
                              <td class="mdl-data-table__cell--non-numeric d-desc">${p.descricao}</td>
                              <td class="mdl-data-table__cell--non-numeric">${strDate1}</td>
                              <td class="mdl-data-table__cell--non-numeric">${strDate2}</td>
                            </tr>
                        </c:forEach>
                      </tbody>
                    </table>
                    </div>
                    <div class="text-for-profile">
                    Minhas Ofertas de Doação
                    <table class="mdl-data-table mdl-js-data-table mdl-shadow--2dp d-tables">
                      <thead>
                        <tr>
                          <th class="mdl-data-table__cell--non-numeric">Título</th>
                          <th class="mdl-data-table__cell--non-numeric">Descrição</th>
                          <th class="mdl-data-table__cell--non-numeric">Data criação</th>
                          <th class="mdl-data-table__cell--non-numeric">Data realizada</th>
                        </tr>
                      </thead>
                      <tbody>
                        <c:forEach items="${listaOFERTAS}" var="o">
                            <tr>
                            <fmt:formatDate pattern="dd/MM/yyyy" value="${o.data_criacao}" var="strDate3" />
                            <fmt:formatDate pattern="dd/MM/yyyy" value="${o.data_realizada}" var="strDate4" />
                              <td class="mdl-data-table__cell--non-numeric d-desc">${o.titulo}</td>
                              <td class="mdl-data-table__cell--non-numeric d-desc">${o.descricao}</td>
                              <td class="mdl-data-table__cell--non-numeric">${strDate3}</td>
                              <td class="mdl-data-table__cell--non-numeric">${strDate4}</td>
                            </tr>
                        </c:forEach>
                      </tbody>
                    </table>
                    </div>

                    <c:if test="${usuarioAtivo.repInst}">
                        <div class="text-for-profile">
                            Solicitações de associação de membros
                            <table class="mdl-data-table mdl-js-data-table mdl-shadow--2dp d-tables">
                            <thead>
                              <tr>
                                <th class="mdl-data-table__cell--non-numeric">Nome</th>
                                <th class="mdl-data-table__cell--non-numeric">Aprovar</th>
                                <th class="mdl-data-table__cell--non-numeric">Reprovar</th>
                              </tr>
                            </thead>
                            <tbody>
                              <c:forEach items="${listaVolReqAss}" var="v">
                                  <tr>
                                    <td class="mdl-data-table__cell--non-numeric d-desc">${v.nome}</td>
                                    <td class="mdl-data-table__cell--non-numeric d-desc">
                                      <a href="/instituicao/aprovar?id=${v.id}">Aprovar</a>
                                    </td>
                                    <td class="mdl-data-table__cell--non-numeric d-desc">
                                      <a href="/instituicao/reprovar?id=${v.id}">Reprovar</a>
                                    </td>
                                  </tr>
                              </c:forEach>
                            </tbody>
                          </table>
                        </div>
                        <div class="text-for-profile">
                            Solicitações de novas atividades
                            <table class="mdl-data-table mdl-js-data-table mdl-shadow--2dp d-tables">
                            <thead>
                              <tr>
                                <th class="mdl-data-table__cell--non-numeric">Título</th>
                                <th class="mdl-data-table__cell--non-numeric">Criador</th>
                                <th class="mdl-data-table__cell--non-numeric">Ler mais</th>
                                <th class="mdl-data-table__cell--non-numeric">Aprovar</th>
                                <th class="mdl-data-table__cell--non-numeric">Reprovar</th>
                              </tr>
                            </thead>
                            <tbody>
                              <c:forEach items="${listaAtvsAguardandoAprov}" var="a">
                                  <tr>
                                    <td class="mdl-data-table__cell--non-numeric d-desc">${a.titulo}</td>
                                    <td class="mdl-data-table__cell--non-numeric d-desc">${a.criador.nome}</td>
                                    <td class="mdl-data-table__cell--non-numeric d-desc">
                                      <a href="/atividade/get?id=${a.id}">Ler Mais</a>
                                    </td>
                                    <td class="mdl-data-table__cell--non-numeric d-desc">
                                      <a href="/atividade/aprovar?id=${a.id}">Aprovar</a>
                                    </td>
                                    <td class="mdl-data-table__cell--non-numeric d-desc">
                                      <a href="/atividade/reprovar?id=${a.id}">Reprovar</a>
                                    </td>
                                  </tr>
                              </c:forEach>
                            </tbody>
                          </table>
                        </div>
                    </c:if>
                    <c:if test="${usuarioAtivo.repInst}">
                    	<div class="text-for-profile">
                            Atividades da minha instituicao
                            <table class="mdl-data-table mdl-js-data-table mdl-shadow--2dp d-tables">
                            <thead>
                              <tr>
                                <th class="mdl-data-table__cell--non-numeric">Título</th>
                                <th class="mdl-data-table__cell--non-numeric">Criador</th>
                                <th class="mdl-data-table__cell--non-numeric">Ler mais</th>
                                <th class="mdl-data-table__cell--non-numeric">Deletar</th>
                              </tr>
                            </thead>
                            <tbody>
                              <c:forEach items="${listaMinhasAtividades}" var="a">
                                  <tr>
                                    <td class="mdl-data-table__cell--non-numeric d-desc">${a.titulo}</td>
                                    <td class="mdl-data-table__cell--non-numeric d-desc">${a.criador.nome}</td>
                                    <td class="mdl-data-table__cell--non-numeric d-desc">
                                      <a href="/atividade/get?id=${a.id}">Ler Mais</a>
                                    </td>
                                    <td class="mdl-data-table__cell--non-numeric d-desc">
                                      <a href="/atividade/delete?id=${a.id}">Deletar</a>
                                    </td>
                                  </tr>
                              </c:forEach>
                            </tbody>
                          </table>
                        </div>
                    </c:if>
                    <c:if test="${usuarioAtivo.memInst}">
                      <div class="text-for-profile">
                            Minhas atividades
                            <table class="mdl-data-table mdl-js-data-table mdl-shadow--2dp d-tables">
                            <thead>
                              <tr>
                                <th class="mdl-data-table__cell--non-numeric">Título</th>
                                <th class="mdl-data-table__cell--non-numeric">Ler mais</th>
                                <th class="mdl-data-table__cell--non-numeric">Deletar</th>
                              </tr>
                            </thead>
                            <tbody>
                              <c:forEach items="${listaMinhasAtividades}" var="a">
                                  <tr>
                                    <td class="mdl-data-table__cell--non-numeric d-desc">${a.titulo}</td>
                                    <td class="mdl-data-table__cell--non-numeric d-desc">
                                      <a href="/atividade/get?id=${a.id}">Ler Mais</a>
                                    </td>
                                    <td class="mdl-data-table__cell--non-numeric d-desc">
                                      <a href="/atividade/delete?id=${a.id}">Deletar</a>
                                    </td>
                                  </tr>
                              </c:forEach>
                            </tbody>
                          </table>
                        </div>
                    </c:if>
                </div>

                <div class="right-side">
                    <c:if test="${usuarioAtivo.repInst}">
                        <div class="text-for-profile">
                            Veja como sua instituição aparece para todos!
                            <div>
                            <a href="/instituicao/get?id=${idMinhaInst}" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect profile-page-buttons mdl-color--primary">Ver</a>
                            </div>
                            <div class="text-for-profile2">
                            Delete sua instituição
                            <div>
                            <a href="/instituicao/delete?id=${idMinhaInst}" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect profile-page-buttons mdl-color--primary">Deletar</a>
                            </div>
                            </div>
                            <div class="text-for-profile2">
                            Atualize sua instituição
                            <div>
                            <a href="/instituicao/updateForm?id=${idMinhaInst}" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect profile-page-buttons mdl-color--primary">Atualizar</a>
                            </div>
                            </div>
                        </div>
                    </c:if>

                    <div class="text-for-profile">
                        Quer ver ou alterar algum dado pessoal de seu cadastro?
                        <div>
                        <a href="/perfil/atualizarCadastro" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect profile-page-buttons mdl-color--primary">Ver/Atualizar</a>
                        </div>
                    </div>

                    <div class="text-for-profile">
                        Deseja excluir sua conta Angelos? Confirme sua senha abaixo:
                    </div>

                    <form method="post" action="/perfil/deletarConta">
                        <div>
                        <label for="senha">Senha: </label>
                        <input type="password" name="senha" id="senha"/>
                        </div>

                        <button class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect profile-page-buttons mdl-color--primary">
                            Deletar
                        </button>
                    </form>

                    <c:if test="${!(usuarioAtivo.repInst)}">
                    <div class="text-for-profile">
                        É representante de alguma instituição? Cadastre-a aqui!

                        <div>
                        <a href="/instituicao/insertForm" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect profile-page-buttons mdl-color--primary">Cadastrar Instituição</a>
                        </div>
                    </div>
                    </c:if>

                    <c:if test="${(!usuarioAtivo.memInst) && (usuarioAtivo.instAss == null)}">
                    <div class="text-for-profile">
                        É membro de alguma instituição? Solicite a sua associação para o representante!

                        <form method="post" action="/instituicao/associa">
                            <div>
                            <select class="d-tables" name="select_inst" id="select_inst" required size="1">
                              <c:forEach items="${listaInsts}" var="i">
                                <option value="${i.id}">${i.nome}</option>
                              </c:forEach>
                            </select>
                            </div>

                            <button class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect profile-page-buttons mdl-color--primary">
                                Solicitar associação
                            </button>
                        </form>
                    </div>
                    </c:if>
                </div>
                <!--///////////////////////////////////////-->
                <!--////////////////CONTENT////////////////-->
                <!--///////////////////////////////////////-->
            </main>

            <c:import url="/WEB-INF/views/footer.jsp" />

        </div>
        <script src="https://code.getmdl.io/1.2.1/material.min.js"></script>
    </body>
</html>
