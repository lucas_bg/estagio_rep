<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="description" content="A front-end template that helps you build fast, modern mobile web apps.">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0">

        <link rel="shortcut icon" href="/img/favicon.ico">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:regular,bold,italic,thin,light,bolditalic,black,medium&amp;lang=en">
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
        <link rel="stylesheet" href="/css/material.css">
        <link rel="stylesheet" href="/css/styles.css">

        <title>Angelos Home</title>
    </head>

    <body class="mdl-demo mdl-color--grey-200 mdl-color-text--grey-700 mdl-base">
        <div class="mdl-layout mdl-js-layout mdl-layout--fixed-header">

            <c:import url="/WEB-INF/views/header.jsp" />

            <main class="mdl-layout__content">
                <!--///////////////////////////////////////-->
                <!--////////////////CONTENT////////////////-->
                <!--///////////////////////////////////////-->
                
                <section class="section--center mdl-grid mdl-grid--no-spacing mdl-shadow--2dp mdl-color--grey-50">

                <h3 class="h3-upd-cad-tit">Atualizar Cadastro</h3>

                <form class="common-form" method="post" action="/perfil/atualizar">
                    <!--/// ${usuarioAtivo.id}///-->
                    <div class="mdl-cell mdl-cell--12-col mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                        <label class="mdl-color-text--grey-700 mdl-textfield__label" for="email">Email *</label>
                        <input class="input-field-custom mdl-textfield__input" type="email" name="email" id="email" required value="${usuarioAtivo.email}">
                    </div>
                    <div class="mdl-cell mdl-cell--12-col mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                        <label class="mdl-color-text--grey-700 mdl-textfield__label" for="senha">Senha *</label>
                        <input class="input-field-custom mdl-textfield__input" type="password" name="senha" id="senha" required >
                    </div>
                    <div class="mdl-cell mdl-cell--12-col mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                        <label class="mdl-color-text--grey-700 mdl-textfield__label" for="passwordConfirm">Confirme a Senha *</label>
                        <input class="input-field-custom mdl-textfield__input" type="password" name="passwordConfirm" id="passwordConfirm" required ">
                    </div>
                    <fmt:formatDate pattern="dd/MM/yyyy" value="${usuarioAtivo.dataNasc}" var="strDate" />
                    <div class="mdl-cell mdl-cell--12-col mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                        <label class="mdl-color-text--grey-700 mdl-textfield__label" for="dataNasc">Data de Nascimento (dd/mm/aaaa) *</label>
                        <input class="input-field-custom mdl-textfield__input" type="text" name="dataNasc" id="dataNasc" pattern="(0[1-9]|1[0-9]|2[0-9]|3[01])/(0[1-9]|1[012])/[0-9]{4}" required  value="${strDate}">
                        <span class="mdl-textfield__error">Data em formato errado!</span>
                    </div>
                    <div class="mdl-cell mdl-cell--12-col mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                        <label class="mdl-color-text--grey-700 mdl-textfield__label" for="nome">Nome *</label>
                        <input class="input-field-custom mdl-textfield__input" type="text" name="nome" id="nome" required  value="${usuarioAtivo.nome}">
                    </div>
                    <div class="mdl-cell mdl-cell--12-col mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                        <label class="mdl-color-text--grey-700 mdl-textfield__label" for="sNome">Sobrenome *</label>
                        <input class="input-field-custom mdl-textfield__input" type="text" name="sNome" id="sNome" required  value="${usuarioAtivo.sNome}">
                    </div>
                    <div class="mdl-cell mdl-cell--12-col mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                        <label class="mdl-color-text--grey-700 mdl-textfield__label" for="telefone">Telefone</label>
                        <input class="input-field-custom mdl-textfield__input" type="tel" name="telefone" id="telefone" value="${usuarioAtivo.telefone}">
                    </div>
                    <div class="mdl-cell mdl-cell--12-col mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                        <label class="mdl-color-text--grey-700 mdl-textfield__label" for="celular">Celular *</label>
                        <input class="input-field-custom mdl-textfield__input" type="tel" name="celular" id="celular" required  value="${usuarioAtivo.celular}">
                    </div>
                    <input type="hidden" name="idd" id="idd" value="${usuarioAtivo.id}">
                    <button class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect profile-page-buttons mdl-color--primary">
                        Atualizar
                    </button>
                </form>    
                </section>
                
                <!--///////////////////////////////////////-->
                <!--////////////////CONTENT////////////////-->
                <!--///////////////////////////////////////-->
            </main>

            <c:import url="/WEB-INF/views/footer.jsp" />

        </div>
        <script src="https://code.getmdl.io/1.2.1/material.min.js"></script>
    </body>
</html>
