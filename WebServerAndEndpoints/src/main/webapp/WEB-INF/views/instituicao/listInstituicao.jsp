<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="description" content="A front-end template that helps you build fast, modern mobile web apps.">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0">

        <link rel="shortcut icon" href="/img/favicon.ico">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:regular,bold,italic,thin,light,bolditalic,black,medium&amp;lang=en">
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
        <link rel="stylesheet" href="/css/material.css">
        <link rel="stylesheet" href="/css/styles.css">

        <title>Angelos Instituição</title>
    </head>

    <body class="mdl-demo mdl-color--grey-200 mdl-color-text--grey-700 mdl-base">
        <div class="mdl-layout mdl-js-layout mdl-layout--fixed-header">

            <c:import url="/WEB-INF/views/header.jsp" />

            <main class="mdl-layout__content">
                    <!--///////////////////////////////////////-->
                    <!--////////////////CONTENT////////////////-->
                    <!--///////////////////////////////////////-->
                    <c:forEach items="${instituicoesList}" var="inst">
                        <section class="section--center mdl-grid mdl-grid--no-spacing mdl-shadow--2dp" id="containerGrande${inst.id}">
                            <div class="mdl-card mdl-cell mdl-cell--12-col">
                                <div class="mdl-card__supporting-text mdl-grid mdl-grid--no-spacing">

                                    <h4 class="mdl-cell mdl-cell--12-col">${inst.nome}</h4>

                                    <div class="section__circle-container mdl-cell mdl-cell--4-col">
                                        <img class="section__circle-container__circle " src="/instituicao/getImg?id=${inst.id}" alt="      Logo">
                                    </div>

                                    <div class="section__text mdl-cell mdl-cell--8-col">
                                        <div class="field-value2">${inst.apresentacao}</div>
                                    </div>

                                </div>

                                <div class="mdl-card__actions">
                                    <a href="/instituicao/get?id=${inst.id}" class="mdl-button"> Leia Mais </a>
                                    <c:if test="${inst.id == usuarioAtivo.instAss.id && usuarioAtivo.repInst}">
                                    <a href="/instituicao/delete?id=${inst.id}" class="mdl-button"> Deletar </a>
                                    <a href="/instituicao/updateForm?id=${inst.id}" class="mdl-button"> Atualizar </a>
                                    </c:if>
                                </div>
                            </div>
                        </section>
                    </c:forEach>
                    <!--///////////////////////////////////////-->
                    <!--////////////////CONTENT////////////////-->
                    <!--///////////////////////////////////////-->
            </main>

            <c:import url="/WEB-INF/views/footer.jsp" />

        </div>
        <script src="https://code.getmdl.io/1.2.1/material.min.js"></script>
    </body>
</html>
