<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<html>
    <head>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>

        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="description" content="A front-end template that helps you build fast, modern mobile web apps.">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0">

        <link rel="shortcut icon" href="/img/favicon.ico">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:regular,bold,italic,thin,light,bolditalic,black,medium&amp;lang=en">
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
        <link rel="stylesheet" href="/css/material.css">
        <link rel="stylesheet" href="/css/styles.css">

        <title>Angelos Instituição</title>
    </head>

    <body class="mdl-demo mdl-color--grey-200 mdl-color-text--grey-700 mdl-base">
        <div class="mdl-layout mdl-js-layout mdl-layout--fixed-header">

            <c:import url="/WEB-INF/views/header.jsp" />

            <main class="mdl-layout__content">
                <!--///////////////////////////////////////-->
                <!--////////////////CONTENT////////////////-->
                <!--///////////////////////////////////////-->
                <section class="section--center mdl-grid mdl-grid--no-spacing mdl-shadow--2dp mdl-color--grey-50">

                <c:choose>
                  <c:when test="${updateMode}">
                    <h3 class="h3-upd-cad-tit">Atializar Instituição</h3>
                  </c:when>
                  <c:otherwise>
                    <h3 class="h3-upd-cad-tit">Cadastro Instituição</h3>
                  </c:otherwise>
                </c:choose>

                    <form class="common-form" method="post" action="/instituicao" enctype="multipart/form-data">

                        <!--///BPOINT 2///-->
                        <div class="mdl-cell mdl-cell--12-col mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                            <label class="mdl-color-text--grey-700 mdl-textfield__label" for="nome">Nome *</label>
                            <input class="input-field-custom mdl-textfield__input" type="text" name="nome" id="nome" required value="${inst.nome}">
                        </div>

                        <div class="mdl-cell mdl-cell--12-col mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                            <label class="mdl-color-text--grey-700 mdl-textfield__label" for="apresentacao">Apresentação (até 500 caracteres) *</label>
                            <textarea class="mdl-textfield__input" type="text" rows= "6" name="apresentacao" id="apresentacao" maxlength="500" required value="${inst.apresentacao}">${inst.apresentacao}</textarea>
                        </div>

                        <div class="mdl-cell mdl-cell--12-col mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                            <label class="mdl-color-text--grey-700 mdl-textfield__label" for="endereco">Endereço</label>
                            <input class="input-field-custom mdl-textfield__input" type="text" name="endereco" id="endereco" value="${inst.endereco}">
                        </div>

                        <div class="mdl-cell mdl-cell--12-col mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                            <label class="mdl-color-text--grey-700 mdl-textfield__label" for="email">Email</label>
                            <input class="input-field-custom mdl-textfield__input" type="email" name="email" id="email" value="${inst.email}">
                        </div>

                        <div class="mdl-cell mdl-cell--12-col mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                            <label class="mdl-color-text--grey-700 mdl-textfield__label" for="numMem">Número de Membros *</label>
                            <input class="input-field-custom mdl-textfield__input" type="text" name="numMem" id="numMem" pattern="-?[0-9]*(\.[0-9]+)?" required value="${inst.numMem}">
                            <span class="mdl-textfield__error">Insira um número!</span>
                        </div>

                        <div class="mdl-cell mdl-cell--12-col mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                            <label class="mdl-color-text--grey-700 mdl-textfield__label" for="numFunc">Número de Funcionários</label>
                            <input class="input-field-custom mdl-textfield__input" type="text" name="numFunc" id="numFunc" pattern="-?[0-9]*(\.[0-9]+)?" required value="${inst.numFunc}">
                            <span class="mdl-textfield__error">Insira um número!</span>
                        </div>

                        <div class="mdl-cell mdl-cell--12-col mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                            <label class="mdl-color-text--grey-700 mdl-textfield__label" for="descricao_detalhada">Descrição Detalhada (até 5000 caracteres) *</label>
                            <textarea class="mdl-textfield__input" type="text" rows= "12" name="descricao_detalhada" id="descricao_detalhada" maxlength="5000" required> ${inst.descricao_detalhada}</textarea>
                        </div>

                        <div class="mdl-cell mdl-cell--12-col mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                            <label class="mdl-color-text--grey-700 mdl-textfield__label" for="website">Website (www.website.org.br)</label>
                            <input class="input-field-custom mdl-textfield__input" type="text" name="website" id="website" value="${inst.website}">
                        </div>

                        <div class="mdl-cell mdl-cell--12-col mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                            <label class="mdl-color-text--grey-700 mdl-textfield__label" for="facebook">Facebook (www.facebook.com/seufacebook)</label>
                            <input class="input-field-custom mdl-textfield__input" type="text" name="facebook" id="facebook" value="${inst.facebook}">
                        </div>

                        <div class="mdl-cell mdl-cell--12-col mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                            <label class="mdl-color-text--grey-700 mdl-textfield__label" for="retornar">Link Retornar (www.retornar.com.br/seulink)</label>
                            <input class="input-field-custom mdl-textfield__input" type="text" name="retornar" id="retornar" value="${inst.retornar}">
                        </div>

                        <div class="mdl-cell mdl-cell--12-col mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                            <label class="mdl-color-text--grey-700 mdl-textfield__label" for="telefone">Telefone *</label>
                            <input class="input-field-custom mdl-textfield__input" type="tel" name="telefone" id="telefone" required value="${inst.telefone}">
                        </div>

                        <fmt:formatDate pattern="dd/MM/yyyy" value="${inst.data_fund}" var="strDate" />
                        <div class="mdl-cell mdl-cell--12-col mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                            <label class="mdl-color-text--grey-700 mdl-textfield__label" for="data_fund">Data de Fundação (dd/mm/aaaa) *</label>
                            <input class="input-field-custom mdl-textfield__input" type="text" name="data_fund" id="data_fund" pattern="(0[1-9]|1[0-9]|2[0-9]|3[01])/(0[1-9]|1[012])/[0-9]{4}" required value="${strDate}">
                            <span class="mdl-textfield__error">Data em formato errado!</span>
                        </div>

                        <div class="mdl-cell mdl-cell--12-col mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                            <label class="mdl-color-text--grey-700 mdl-textfield__label" for="imagee"></label>
                            <input class="input-field-custom mdl-textfield__input upload-file" type="file" name="imagee" id="imagee" data-max-size="512000">
                        </div>

                        <c:choose>
                          <c:when test="${updateMode}">
                            <input type="hidden" name="idd" id="idd" value="${inst.id}">

                            <button class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect profile-page-buttons mdl-color--primary">
                              Atualizar Instituição
                            </button>
                          </c:when>
                          <c:otherwise>
                            <input type="hidden" name="idd" id="idd" value="-1">

                            <button class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect profile-page-buttons mdl-color--primary">
                              Cadastrar Instituição
                            </button>
                          </c:otherwise>
                        </c:choose>

                        
                    </form>
                        <script>
                            $(function(){
                                var fileInput = $('.upload-file');
                                var maxSize = fileInput.data('max-size');
                                $('.upload-form').submit(function(e){
                                    if(fileInput.get(0).files.length){
                                        var fileSize = fileInput.get(0).files[0].size;
                                        if(fileSize>maxSize){
                                            alert('Por favor, escolha uma imagem de até 500Kb');
                                            return false;
                                        }
                                    }
                                });
                            });
                        </script>
                </section>
                <!--///////////////////////////////////////-->
                <!--////////////////CONTENT////////////////-->
                <!--///////////////////////////////////////-->
            </main>

            <c:import url="/WEB-INF/views/footer.jsp" />

        </div>
        <script src="https://code.getmdl.io/1.2.1/material.min.js"></script>
    </body>
</html>
