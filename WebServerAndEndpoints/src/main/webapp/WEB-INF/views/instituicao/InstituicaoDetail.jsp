<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="description" content="A front-end template that helps you build fast, modern mobile web apps.">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0">

        <link rel="shortcut icon" href="/img/favicon.ico">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:regular,bold,italic,thin,light,bolditalic,black,medium&amp;lang=en">
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
        <link rel="stylesheet" href="/css/material.css">
        <link rel="stylesheet" href="/css/styles.css">

        <title>Angelos Instituição</title>
    </head>

    <body class="mdl-demo mdl-color--grey-200 mdl-color-text--grey-700 mdl-base">
        <div class="mdl-layout mdl-js-layout mdl-layout--fixed-header">

            <c:import url="/WEB-INF/views/header.jsp" />

            <main class="mdl-layout__content">
                <!--///////////////////////////////////////-->
                <!--////////////////CONTENT////////////////-->
                <!--///////////////////////////////////////-->
                <section class="section--center2 mdl-grid mdl-grid--no-spacing mdl-shadow--2dp mdl-color--grey-50">
                    <div class="section__circle-container2 dtl-image mdl-cell mdl-cell--11-col">
                        <img class="section__circle-container2__circle" src="/instituicao/getImg?id=${inst.id}" alt="logo instituicao">
                    </div>
                    <!--///BPOINT 1///-->
                    <div class="mdl-cell mdl-cell--11-col field-value-main"> ${inst.nome} </div>
                    <div class="mdl-cell mdl-cell--11-col field-label"> Endereço: </div>
                    <div class="mdl-cell mdl-cell--11-col field-value"> ${inst.endereco} 
                        <a href="http://www.google.com/maps/?q=${inst.endereco}, Londrina" target="_blank"><img src="/img/gMapIcon.png" alt="icone"></a>
                    </div>
                    <div class="mdl-cell mdl-cell--11-col field-label"> Apresentação: </div>
                    <div class="mdl-cell mdl-cell--11-col field-value"> ${inst.apresentacao} </div>
                    <div class="mdl-cell mdl-cell--11-col field-label"> Email: </div>
                    <div class="mdl-cell mdl-cell--11-col field-value">
                    <a href="mailto:${inst.email}" target="_top">${inst.email}</a>
                     </div>
                    <div class="mdl-cell mdl-cell--11-col field-label"> Número de Membros: </div>
                    <div class="mdl-cell mdl-cell--11-col field-value"> ${inst.numMem} </div>
                    <div class="mdl-cell mdl-cell--11-col field-label"> Número de Funcionários: </div>
                    <div class="mdl-cell mdl-cell--11-col field-value"> ${inst.numFunc} </div>
                    <div class="mdl-cell mdl-cell--11-col field-label"> Descrição Detalhada: </div>
                    <div class="mdl-cell mdl-cell--11-col field-value"> ${inst.descricao_detalhada} </div>
                    <div class="mdl-cell mdl-cell--11-col field-label"> Website: </div>
                    <div class="mdl-cell mdl-cell--11-col field-value"><a href="http://${inst.website}" target="_blank">${inst.website}</a></div>
                    <div class="mdl-cell mdl-cell--11-col field-label"> Facebook: </div>
                    <div class="mdl-cell mdl-cell--11-col field-value"><a href="http://${inst.facebook}" target="_blank">${inst.facebook}</a></div>
                    <div class="mdl-cell mdl-cell--11-col field-label"> Link Retornar: </div>
                    <div class="mdl-cell mdl-cell--11-col field-value"><a href="http://${inst.retornar}" target="_blank">${inst.retornar}</a></div>
                    <div class="mdl-cell mdl-cell--11-col field-label"> Telefone: </div>
                    <div class="mdl-cell mdl-cell--11-col field-value"> ${inst.telefone} </div>
                    <div class="mdl-cell mdl-cell--11-col field-label"> Data de Fundação: </div>
                    <div class="mdl-cell mdl-cell--11-col field-value">
                    <fmt:formatDate pattern="dd/MM/yyyy" value="${inst.data_fund}" />
                    </div>
                </section>
                <!--///////////////////////////////////////-->
                <!--////////////////CONTENT////////////////-->
                <!--///////////////////////////////////////-->
            </main>

            <c:import url="/WEB-INF/views/footer.jsp" />

        </div>
        <script src="https://code.getmdl.io/1.2.1/material.min.js"></script>
    </body>
</html>
