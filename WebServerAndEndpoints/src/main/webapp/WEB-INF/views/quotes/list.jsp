<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
        <link rel="shortcut icon" href="/img/favicon.ico">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
    <div>
        ${success}
    </div>
    <div>
        ${successDelete}
    </div>
    <div>
        ${successUpdate}
    </div>

    <table>
        <tr>
        <td>Id</td>
        <td>Who said</td>
        <td>What he said</td>
        </tr>
        <c:forEach items="${quotes}" var="quote">
            <tr>
                <td> ${quote.id} </td>
                <td> ${quote.who} </td>
                <td> ${quote.what} </td>
            </tr>
        </c:forEach>
    </table>

    <a href="/quotes"> All Quotes </a>
    <a href="/quotes/insertV"> Insert Quote </a>
    <a href="/quotes/updateV"> Update Quote </a>

    <form method="post" action="/quotes/delete">
        Delete Quote
        <div>
        <label for="id">Id</label>
        <input type="text" name="id" id="id"/>
        </div>

        <div>
        <input type="submit" value="Enviar">
        </div>
    </form>

    <form method="get" action="/quotes/get">
        Get Quote
        <div>
        <label for="id">Id</label>
        <input type="text" name="id" id="id"/>
        </div>

        <div>
        <input type="submit" value="Enviar">
        </div>
    </form>

</body>
</html>
