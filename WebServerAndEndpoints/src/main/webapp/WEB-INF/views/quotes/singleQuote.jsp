<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
        <link rel="shortcut icon" href="/img/favicon.ico">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>

    <table>
        <tr>
            <td>Id</td>
            <td>Who said</td>
            <td>What he said</td>
        </tr>
        <tr>
            <td> ${quote.id} </td>
            <td> ${quote.who} </td>
            <td> ${quote.what} </td>
        </tr>
    </table>

</body>
</html>
