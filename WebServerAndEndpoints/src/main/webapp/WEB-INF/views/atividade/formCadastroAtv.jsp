<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<html>
    <head>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>

        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="description" content="A front-end template that helps you build fast, modern mobile web apps.">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0">

        <link rel="shortcut icon" href="/img/favicon.ico">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:regular,bold,italic,thin,light,bolditalic,black,medium&amp;lang=en">
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
        <link rel="stylesheet" href="/css/material.css">
        <link rel="stylesheet" href="/css/styles.css">

        <title>Angelos Atividade</title>
    </head>

    <body class="mdl-demo mdl-color--grey-200 mdl-color-text--grey-700 mdl-base">
        <div class="mdl-layout mdl-js-layout mdl-layout--fixed-header">

            <c:import url="/WEB-INF/views/header.jsp" />

            <main class="mdl-layout__content">
                <!--///////////////////////////////////////-->
                <!--////////////////CONTENT////////////////-->
                <!--///////////////////////////////////////-->
                <section class="section--center mdl-grid mdl-grid--no-spacing mdl-shadow--2dp mdl-color--grey-50">

                <h3 class="h3-upd-cad-tit">Cadastro Atividade</h3>

                <form class="common-form" method="post" action="/atividade" id="ativ-form">

                    <!--///BPOINT 2///-->
                    <div class="mdl-cell mdl-cell--12-col mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                        <label class="mdl-color-text--grey-700 mdl-textfield__label" for="titulo">Título *</label>
                        <input class="input-field-custom mdl-textfield__input" type="text" name="titulo" id="titulo" required>
                    </div>

                    <div class="mdl-cell mdl-cell--12-col mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                        <label class="mdl-color-text--grey-700 mdl-textfield__label" for="apresentacao">Apresentação (até 500 caracteres) *</label>
                        <textarea class="mdl-textfield__input" type="text" rows= "6" name="apresentacao" id="apresentacao" maxlength="500" required></textarea>
                    </div>

                    <div class="mdl-cell mdl-cell--12-col mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                        <label class="mdl-color-text--grey-700 mdl-textfield__label" for="descricao_detalhada">Descrição Detalhada (até 5000 caracteres) *</label>
                        <textarea class="mdl-textfield__input" type="text" rows= "12" name="descricao_detalhada" id="descricao_detalhada" maxlength="5000" required></textarea>
                    </div>

                    <div class="mdl-cell mdl-cell--12-col mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                        <label class="mdl-color-text--grey-700 mdl-textfield__label" for="idade_min">Idade mínima (0 caso padrão)</label>
                        <input class="input-field-custom mdl-textfield__input" type="text" name="idade_min" id="idade_min" pattern="-?[0-9]*(\.[0-9]+)?" required>
                        <span class="mdl-textfield__error">Insira um número!</span>
                    </div>

                    <div class="mdl-cell mdl-cell--12-col mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                        <label class="mdl-color-text--grey-700 mdl-textfield__label" for="num_volun_atual">Número de voluntários atualmente (0 caso padrão)</label>
                        <input class="input-field-custom mdl-textfield__input" type="text" name="num_volun_atual" id="num_volun_atual" pattern="-?[0-9]*(\.[0-9]+)?" required>
                        <span class="mdl-textfield__error">Insira um número!</span>
                    </div>

                    <div class="mdl-cell mdl-cell--12-col mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                        <label class="mdl-color-text--grey-700 mdl-textfield__label" for="num_volun_limite">Número de voluntários limite (0 caso padrão)</label>
                        <input class="input-field-custom mdl-textfield__input" type="text" name="num_volun_limite" id="num_volun_limite" pattern="-?[0-9]*(\.[0-9]+)?" required>
                        <span class="mdl-textfield__error">Insira um número!</span>
                    </div>

                    <div class="mdl-cell mdl-cell--12-col">
                        Categoria   
                        <select class="d-tablesss" name="categoria" id="categoria" required size="1">
                            <option value="IDOSOS">Idosos</option>
                            <option value="CRIANCAS">Crianças</option>
                            <option value="MEIO_AMBIENTE">Meio Ambiente</option>
                            <option value="ADOLESCENTES">Adolescentes</option>
                            <option value="FE">Fé</option>
                            <option value="GERAL">Geral</option>
                        </select>
                    </div>

                    <div class="mdl-cell mdl-cell--12-col">
                        Tipo   
                    <label class="mdl-radio mdl-js-radio mdl-js-ripple-effect" for="option-funcao">
                        <input type="radio" id="option-funcao" class="mdl-radio__button" name="tipo_options" value="FUNCAO" onchange="setExtras(this.value)" required>
                        <span class="mdl-radio__label">Função</span>
                    </label>
                    <label class="mdl-radio mdl-js-radio mdl-js-ripple-effect" for="option-evento">
                        <input type="radio" id="option-evento" class="mdl-radio__button" name="tipo_options" value="EVENTO" onchange="setExtras(this.value)" required>
                        <span class="mdl-radio__label">Evento</span>
                    </label>
                    <label class="mdl-radio mdl-js-radio mdl-js-ripple-effect" for="option-projeto">
                        <input type="radio" id="option-projeto" class="mdl-radio__button" name="tipo_options" value="PROJETO" onchange="setExtras(this.value)" required>
                        <span class="mdl-radio__label">Projeto</span>
                    </label>
                    </div>

                    <div style="display: none" class="extra-section" id="area-funcao">
                        <div class="mdl-cell mdl-cell--12-col mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                            <label class="mdl-color-text--grey-700 mdl-textfield__label" for="duracao_media">Duração média (horas)</label>
                            <input id="func-req-1" class="input-field-custom mdl-textfield__input" type="text" name="duracao_media" id="duracao_media" pattern="-?[0-9]*(\.[0-9]+)?" required>
                            <span class="mdl-textfield__error">Insira um número!</span>
                        </div>
                        <div class="mdl-cell mdl-cell--12-col mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                            Dias em que ocorre
                            <label class="mdl-checkbox mdl-js-checkbox mdl-js-ripple-effect" for="segunda">
                              <input type="checkbox" id="segunda" value="SEGUNDA" name="segunda" class="mdl-checkbox__input">
                              <span class="mdl-checkbox__label">Segunda</span>
                            </label>
                            <label class="mdl-checkbox mdl-js-checkbox mdl-js-ripple-effect" for="terca">
                              <input type="checkbox" id="terca" value="TERCA" name="terca" class="mdl-checkbox__input">
                              <span class="mdl-checkbox__label">Terça</span>
                            </label>
                            <label class="mdl-checkbox mdl-js-checkbox mdl-js-ripple-effect" for="quarta">
                              <input type="checkbox" id="quarta" value="QUARTA" name="quarta" class="mdl-checkbox__input">
                              <span class="mdl-checkbox__label">Quarta</span>
                            </label>
                            <label class="mdl-checkbox mdl-js-checkbox mdl-js-ripple-effect" for="quinta">
                              <input type="checkbox" id="quinta" value="QUINTA" name="quinta" class="mdl-checkbox__input">
                              <span class="mdl-checkbox__label">Quinta</span>
                            </label>
                            <label class="mdl-checkbox mdl-js-checkbox mdl-js-ripple-effect" for="sexta">
                              <input type="checkbox" id="sexta" value="SEXTA" name="sexta" class="mdl-checkbox__input">
                              <span class="mdl-checkbox__label">Sexta</span>
                            </label>
                            <label class="mdl-checkbox mdl-js-checkbox mdl-js-ripple-effect" for="sabado">
                              <input type="checkbox" id="sabado" value="SABADO" name="sabado" class="mdl-checkbox__input">
                              <span class="mdl-checkbox__label">Sábado</span>
                            </label>
                            <label class="mdl-checkbox mdl-js-checkbox mdl-js-ripple-effect" for="domingo">
                              <input type="checkbox" id="domingo" value="DOMINGO" name="domingo" class="mdl-checkbox__input">
                              <span class="mdl-checkbox__label">Domingo</span>
                            </label>
                        </div>
                    </div>

                    <div style="display: none" class="extra-section" id="area-evento">
                        <div class="mdl-cell mdl-cell--12-col mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                            <label class="mdl-color-text--grey-700 mdl-textfield__label" for="duracao">Duração (horas)</label>
                            <input id="eve-req-1" class="input-field-custom mdl-textfield__input" type="text" name="duracao" id="duracao" pattern="-?[0-9]*(\.[0-9]+)?" required>
                            <span class="mdl-textfield__error">Insira um número!</span>
                        </div>
                        <div class="mdl-cell mdl-cell--12-col mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                            <label class="mdl-color-text--grey-700 mdl-textfield__label" for="diaEHora">Data (dd/mm/aaaa)</label>
                            <input id="eve-req-2" class="input-field-custom mdl-textfield__input" type="text" name="diaEHora" id="diaEHora" pattern="(0[1-9]|1[0-9]|2[0-9]|3[01])/(0[1-9]|1[012])/[0-9]{4}" required>
                            <span class="mdl-textfield__error">Data em formato errado!</span>
                        </div>
                    </div>

                    <div style="display: none" class="extra-section" id="area-projeto">
                        <div class="mdl-cell mdl-cell--12-col mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                            <label class="mdl-color-text--grey-700 mdl-textfield__label" for="dt_inicio">Data Início (dd/mm/aaaa)</label>
                            <input id="proj-req-1" class="input-field-custom mdl-textfield__input" type="text" name="dt_inicio" id="dt_inicio" pattern="(0[1-9]|1[0-9]|2[0-9]|3[01])/(0[1-9]|1[012])/[0-9]{4}" required>
                            <span class="mdl-textfield__error">Data em formato errado!</span>
                        </div>
                        <div class="mdl-cell mdl-cell--12-col mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                            <label class="mdl-color-text--grey-700 mdl-textfield__label" for="dt_termino">Data Término (dd/mm/aaaa)</label>
                            <input id="proj-req-2" class="input-field-custom mdl-textfield__input" type="text" name="dt_termino" id="dt_termino" pattern="(0[1-9]|1[0-9]|2[0-9]|3[01])/(0[1-9]|1[012])/[0-9]{4}" required>
                            <span class="mdl-textfield__error">Data em formato errado!</span>
                        </div>
                        <div class="mdl-cell mdl-cell--12-col mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                            Dias em que ocorre
                            <label class="mdl-checkbox mdl-js-checkbox mdl-js-ripple-effect" for="segunda_p">
                              <input type="checkbox" id="segunda_p" value="SEGUNDA" name="segunda_p" class="mdl-checkbox__input">
                              <span class="mdl-checkbox__label">Segunda</span>
                            </label>
                            <label class="mdl-checkbox mdl-js-checkbox mdl-js-ripple-effect" for="terca_p">
                              <input type="checkbox" id="terca_p" value="TERCA" name="terca_p" class="mdl-checkbox__input">
                              <span class="mdl-checkbox__label">Terça</span>
                            </label>
                            <label class="mdl-checkbox mdl-js-checkbox mdl-js-ripple-effect" for="quarta_p">
                              <input type="checkbox" id="quarta_p" value="QUARTA" name="quarta_p" class="mdl-checkbox__input">
                              <span class="mdl-checkbox__label">Quarta</span>
                            </label>
                            <label class="mdl-checkbox mdl-js-checkbox mdl-js-ripple-effect" for="quinta_p">
                              <input type="checkbox" id="quinta_p" value="QUINTA" name="quinta_p" class="mdl-checkbox__input">
                              <span class="mdl-checkbox__label">Quinta</span>
                            </label>
                            <label class="mdl-checkbox mdl-js-checkbox mdl-js-ripple-effect" for="sexta_p">
                              <input type="checkbox" id="sexta_p" value="SEXTA" name="sexta_p" class="mdl-checkbox__input">
                              <span class="mdl-checkbox__label">Sexta</span>
                            </label>
                            <label class="mdl-checkbox mdl-js-checkbox mdl-js-ripple-effect" for="sabado_p">
                              <input type="checkbox" id="sabado_p" value="SABADO" name="sabado_p" class="mdl-checkbox__input">
                              <span class="mdl-checkbox__label">Sábado</span>
                            </label>
                            <label class="mdl-checkbox mdl-js-checkbox mdl-js-ripple-effect" for="domingo_p">
                              <input type="checkbox" id="domingo_p" value="DOMINGO" name="domingo_p" class="mdl-checkbox__input">
                              <span class="mdl-checkbox__label">Domingo</span>
                            </label>
                        </div>
                    </div>

                    <button class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect profile-page-buttons mdl-color--primary">
                      Cadastrar Atividade
                    </button>

                </form>
                <script>
                function setExtras(value){
                    if(value == "EVENTO"){
                        document.getElementById("area-funcao").style.display = "none";
                        document.getElementById("area-evento").style.display = "";
                        document.getElementById("area-projeto").style.display = "none";
                        document.getElementById("ativ-form").action = "/atividade/newEvento";
                        document.getElementById("proj-req-1").required = false;
                        document.getElementById("proj-req-2").required = false;
                        document.getElementById("eve-req-1").required = true;
                        document.getElementById("eve-req-2").required = true;
                        document.getElementById("func-req-1").required = false;
                    }else if(value == "FUNCAO"){
                        document.getElementById("area-funcao").style.display = "";
                        document.getElementById("area-evento").style.display = "none";
                        document.getElementById("area-projeto").style.display = "none";
                        document.getElementById("ativ-form").action = "/atividade/newFuncao";
                        document.getElementById("proj-req-1").required = false;
                        document.getElementById("proj-req-2").required = false;
                        document.getElementById("eve-req-1").required = false;
                        document.getElementById("eve-req-2").required = false;
                        document.getElementById("func-req-1").required = true;
                    }else{
                        document.getElementById("area-funcao").style.display = "none";
                        document.getElementById("area-evento").style.display = "none";
                        document.getElementById("area-projeto").style.display = "";
                        document.getElementById("ativ-form").action = "/atividade/newProjeto";
                        document.getElementById("proj-req-1").required = true;
                        document.getElementById("proj-req-2").required = true;
                        document.getElementById("eve-req-1").required = false;
                        document.getElementById("eve-req-2").required = false;
                        document.getElementById("func-req-1").required = false;
                    }
                }
                </script>
                </section>
                <!--///////////////////////////////////////-->
                <!--////////////////CONTENT////////////////-->
                <!--///////////////////////////////////////-->
            </main>

            <c:import url="/WEB-INF/views/footer.jsp" />

        </div>
        <script src="https://code.getmdl.io/1.2.1/material.min.js"></script>
    </body>
</html>
