<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="description" content="A front-end template that helps you build fast, modern mobile web apps.">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0">

        <link rel="shortcut icon" href="/img/favicon.ico">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:regular,bold,italic,thin,light,bolditalic,black,medium&amp;lang=en">
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
        <link rel="stylesheet" href="/css/material.css">
        <link rel="stylesheet" href="/css/styles.css">

        <title>Angelos Atividade</title>
    </head>

    <body class="mdl-demo mdl-color--grey-200 mdl-color-text--grey-700 mdl-base">
        <div class="mdl-layout mdl-js-layout mdl-layout--fixed-header">
            <c:import url="/WEB-INF/views/header.jsp" />
                <main class="mdl-layout__content">
<!--///////////////////////////////////////-->
<!--////////////////CONTENT////////////////-->
<!--///////////////////////////////////////-->
<c:forEach items="${listaAtivs}" var="atv">
<section class="section--center mdl-grid mdl-grid--no-spacing mdl-shadow--2dp">

    <c:choose>
        <c:when test="${atv.class.name == 'br.com.galhardi.backend.models.Funcao'}">
            <div class="mdl-card mdl-cell mdl-cell--12-col mdl-color--blue-grey-200">
        </c:when>
        <c:when test="${atv.class.name == 'br.com.galhardi.backend.models.Evento'}">
            <div class="mdl-card mdl-cell mdl-cell--12-col mdl-color--indigo-200">
        </c:when>
        <c:otherwise>
            <div class="mdl-card mdl-cell mdl-cell--12-col mdl-color--light-green-200">
        </c:otherwise>
    </c:choose>

    <div class="mdl-card__supporting-text mdl-grid mdl-grid--no-spacing custom-card">
        
        <h4 class="mdl-cell mdl-cell--12-col mdl-color-text--grey-900">${atv.titulo}</h4>

        <div class="section__text mdl-cell mdl-cell--8-col without-border">
            <div class="field-value3"><b>Apresentação:</b> ${atv.apresentacao}</div>
        </div>

        <div class="section__text mdl-cell mdl-cell--8-col without-border">
            <div class="field-value3">
                <b>Categoria:</b> ${atv.categoria}
            </div>
        </div>

        <div class="section__text mdl-cell mdl-cell--8-col without-border">
            <div class="field-value3">
                <b>Instituição:</b> <a href="/instituicao/get?id=${atv.inst_pertc.id}">
                ${atv.inst_pertc.nome}
                </a>
            </div>
        </div>
    </div>
    <div class="mdl-card__actions">
        <a href="/atividade/get?id=${atv.id}" class="mdl-button mdl-color-text--grey-900"> Leia Mais </a>
    </div>
    </div>
</section>
</c:forEach>
<!--///////////////////////////////////////-->
<!--////////////////CONTENT////////////////-->
<!--///////////////////////////////////////-->
                </main>
            <c:import url="/WEB-INF/views/footer.jsp" />
        </div>
        <c:if test="${usuarioAtivo.repInst || usuarioAtivo.memInst}">
        <a href="/atividade/insertForm">
            <button class="mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect mdl-button--colored mdl-shadow--4dp mdl-color--accent add-button">
                <i class="material-icons" role="presentation">add</i>
                <span class="visuallyhidden">Add</span>
            </button>
        </a>
        </c:if>
        <script src="https://code.getmdl.io/1.2.1/material.min.js"></script>
    </body>
</html>
