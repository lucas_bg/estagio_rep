<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="description" content="A front-end template that helps you build fast, modern mobile web apps.">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0">

        <link rel="shortcut icon" href="/img/favicon.ico">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:regular,bold,italic,thin,light,bolditalic,black,medium&amp;lang=en">
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
        <link rel="stylesheet" href="/css/material.css">
        <link rel="stylesheet" href="/css/styles.css">

        <title>Angelos Atividade</title>
    </head>

    <body class="mdl-demo mdl-color--grey-200 mdl-color-text--grey-700 mdl-base">
        <div class="mdl-layout mdl-js-layout mdl-layout--fixed-header">
            <c:import url="/WEB-INF/views/header.jsp" />
                <main class="mdl-layout__content">
<!--///////////////////////////////////////-->
<!--////////////////CONTENT////////////////-->
<!--///////////////////////////////////////-->
<section class="section--center2 mdl-grid mdl-grid--no-spacing mdl-shadow--2dp mdl-color--grey-50">
    <!--///BPOINT 1///-->
    <div class="mdl-cell mdl-cell--11-col field-value-main"> ${atv.titulo} </div>
    <div class="mdl-cell mdl-cell--11-col field-label"> Apresentação: </div>
    <div class="mdl-cell mdl-cell--11-col field-value"> ${atv.apresentacao} </div>
    <div class="mdl-cell mdl-cell--11-col field-label"> Idade Mínima: </div>
    <div class="mdl-cell mdl-cell--11-col field-value"> ${atv.idade_min} </div>
    <div class="mdl-cell mdl-cell--11-col field-label"> Número de voluntários atualmente: </div>
    <div class="mdl-cell mdl-cell--11-col field-value"> ${atv.num_volun_atual} </div>
    <div class="mdl-cell mdl-cell--11-col field-label"> Número de voluntários limite: </div>
    <div class="mdl-cell mdl-cell--11-col field-value"> ${atv.num_volun_limite} </div>
    <div class="mdl-cell mdl-cell--11-col field-label"> Descrição Detalhada: </div>
    <div class="mdl-cell mdl-cell--11-col field-value"> ${atv.descricao_detalhada} </div>
    <div class="mdl-cell mdl-cell--11-col field-label"> Categoria: </div>
    <div class="mdl-cell mdl-cell--11-col field-value"> ${atv.categoria} </div>
    <div class="mdl-cell mdl-cell--11-col field-label"> Instituição: </div>
    <div class="mdl-cell mdl-cell--11-col field-value"> 
        <a href="/instituicao/get?id=${atv.inst_pertc.id}">
            ${atv.inst_pertc.nome}
        </a>
    </div>
    <div class="mdl-cell mdl-cell--11-col field-label"> Contato Instituição: </div>
    <div class="mdl-cell mdl-cell--11-col field-value"> ${atv.inst_pertc.telefone} </div>
    <div class="mdl-cell mdl-cell--11-col field-label"> Nome Criador: </div>
    <div class="mdl-cell mdl-cell--11-col field-value"> ${atv.criador.nome} </div>
    <div class="mdl-cell mdl-cell--11-col field-label"> Contato Criador: </div>
    <div class="mdl-cell mdl-cell--11-col field-value"> ${atv.criador.celular} </div>

    <c:choose>
        <c:when test="${atv.class.name == 'br.com.galhardi.backend.models.Funcao'}">
            <div class="mdl-cell mdl-cell--11-col field-label"> Duração Média: </div>
            <div class="mdl-cell mdl-cell--11-col field-value"> ${atv.duracao_media} </div>
            <div class="mdl-cell mdl-cell--11-col field-label"> Realizado nos Dias: </div>
            <c:forEach items="${atv.dias_poss}" var="dia">
                <div class="mdl-cell mdl-cell--11-col field-value"> ${dia} </div>
            </c:forEach>
        </c:when>
        <c:when test="${atv.class.name == 'br.com.galhardi.backend.models.Evento'}">
            <div class="mdl-cell mdl-cell--11-col field-label"> Data do Evento: </div>
            <div class="mdl-cell mdl-cell--11-col field-value">
                <fmt:formatDate pattern="dd/MM/yyyy" value="${atv.diaEHora}" />
            </div>
            <div class="mdl-cell mdl-cell--11-col field-label"> Duração: </div>
            <div class="mdl-cell mdl-cell--11-col field-value"> ${atv.duracao} </div>
        </c:when>
        <c:otherwise>
            <div class="mdl-cell mdl-cell--11-col field-label"> Data de Início: </div>
            <div class="mdl-cell mdl-cell--11-col field-value">
                <fmt:formatDate pattern="dd/MM/yyyy" value="${atv.dt_inicio}" />
            </div>
            <div class="mdl-cell mdl-cell--11-col field-label"> Data de Término: </div>
            <div class="mdl-cell mdl-cell--11-col field-value">
                <fmt:formatDate pattern="dd/MM/yyyy" value="${atv.dt_termino}" />
            </div>
            <div class="mdl-cell mdl-cell--11-col field-label"> Realizado nos Dias: </div>
            <c:forEach items="${atv.dias_ocorr}" var="dia2">
                <div class="mdl-cell mdl-cell--11-col field-value"> ${dia2} </div>
            </c:forEach>
        </c:otherwise>
    </c:choose>

</section>
<!--///////////////////////////////////////-->
<!--////////////////CONTENT////////////////-->
<!--///////////////////////////////////////-->
                </main>
            <c:import url="/WEB-INF/views/footer.jsp" />
        </div>
        <script src="https://code.getmdl.io/1.2.1/material.min.js"></script>
    </body>
</html>
