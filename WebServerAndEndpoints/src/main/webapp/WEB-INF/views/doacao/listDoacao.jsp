<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="description" content="A front-end template that helps you build fast, modern mobile web apps.">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0">

        <link rel="shortcut icon" href="/img/favicon.ico">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:regular,bold,italic,thin,light,bolditalic,black,medium&amp;lang=en">
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
        <link rel="stylesheet" href="/css/material.css">
        <link rel="stylesheet" href="/css/styles.css">

        <title>Angelos Doação</title>
    </head>

    <body class="mdl-demo mdl-color--grey-200 mdl-color-text--grey-700 mdl-base">
        <div class="mdl-layout mdl-js-layout mdl-layout--fixed-header">

            <c:import url="/WEB-INF/views/header.jsp" />

            <main class="mdl-layout__content">
                    <!--///////////////////////////////////////-->
                    <!--////////////////CONTENT////////////////-->
                    <!--///////////////////////////////////////-->
                    <c:forEach items="${doacoesList}" var="doacao">
                        <section class="section--center mdl-grid mdl-grid--no-spacing mdl-shadow--2dp">
                        <c:choose>
                          <c:when test="${doacao.tipo == 'PEDIDO'}">
                                <div class="mdl-card mdl-cell mdl-cell--12-col mdl-color--amber-100">
                          </c:when>
                          <c:otherwise>
                                <div class="mdl-card mdl-cell mdl-cell--12-col mdl-color--cyan-100">
                          </c:otherwise>
                        </c:choose>
                                <div class="mdl-card__supporting-text mdl-grid mdl-grid--no-spacing custom-card">

                                    <h4 class="mdl-cell mdl-cell--12-col mdl-color-text--grey-900"><b>${doacao.tipo}</b> - ${doacao.titulo}</h4>

                                    <div class="section__text mdl-cell mdl-cell--8-col without-border">
                                        <div class="field-value3"><b>Descrição:</b> ${doacao.descricao}</div>
                                    </div>

                                    <div class="section__text mdl-cell mdl-cell--8-col without-border">
                                        <div class="field-value3">
                                            <c:choose>
                                              <c:when test="${doacao.tipo == 'PEDIDO'}">
                                                    <b>Quem Pede:</b> ${doacao.criador.nome}
                                              </c:when>
                                              <c:otherwise>
                                                    <b>Quem Oferece:</b> ${doacao.criador.nome}
                                              </c:otherwise>
                                            </c:choose>
                                        </div>
                                    </div>

                                    <c:if test="${doacao.criador.telefone != ''}">
                                        <div class="section__text mdl-cell mdl-cell--8-col without-border">
                                        <div class="field-value3">
                                            <b>Telefone:</b> ${doacao.criador.telefone}
                                        </div>
                                        </div>
                                    </c:if>

                                    <c:if test="${doacao.criador.celular != ''}">
                                        <div class="section__text mdl-cell mdl-cell--8-col without-border">
                                        <div class="field-value3">
                                            <b>Celular:</b> ${doacao.criador.celular}
                                        </div>
                                        </div>
                                    </c:if>

                                    <fmt:formatDate pattern="dd/MM/yyyy" value="${doacao.data_criacao}" var="strDate" />

                                    <div class="section__text mdl-cell mdl-cell--8-col without-border">
                                        <div class="field-value3">
                                            <b>Data de Criação:</b> ${strDate}
                                        </div>
                                    </div>

                                </div>

                                <c:if test="${usuarioAtivo.id == doacao.criador.id}">
                                   <div class="mdl-card__actions">
                                        <a href="/doacao/delete?id=${doacao.id}" class="mdl-button mdl-color-text--grey-900"> Cancelar Oferta/Pedido </a>
                                        <a href="/doacao/realizar?id=${doacao.id}" class="mdl-button mdl-color-text--grey-900"> Oferta/Pedido Já Realizado! </a>
                                    </div>
                                </c:if>
                            </div>
                        </section>
                    </c:forEach>
                    <!--///////////////////////////////////////-->
                    <!--////////////////CONTENT////////////////-->
                    <!--///////////////////////////////////////-->
            </main>

            <c:import url="/WEB-INF/views/footer.jsp" />

        </div>
        <a href="/doacao/insertForm">
            <button class="mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect mdl-button--colored mdl-shadow--4dp mdl-color--accent add-button">
                <i class="material-icons" role="presentation">add</i>
                <span class="visuallyhidden">Add</span>
            </button>
        </a>
        <script src="https://code.getmdl.io/1.2.1/material.min.js"></script>
    </body>
</html>
