<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<html>
    <head>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>

        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="description" content="A front-end template that helps you build fast, modern mobile web apps.">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0">

        <link rel="shortcut icon" href="/img/favicon.ico">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:regular,bold,italic,thin,light,bolditalic,black,medium&amp;lang=en">
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
        <link rel="stylesheet" href="/css/material.css">
        <link rel="stylesheet" href="/css/styles.css">

        <title>Angelos Doação</title>
    </head>

    <body class="mdl-demo mdl-color--grey-200 mdl-color-text--grey-700 mdl-base">
        <div class="mdl-layout mdl-js-layout mdl-layout--fixed-header">

            <c:import url="/WEB-INF/views/header.jsp" />

            <main class="mdl-layout__content">
                <!--///////////////////////////////////////-->
                <!--////////////////CONTENT////////////////-->
                <!--///////////////////////////////////////-->
                <section class="section--center mdl-grid mdl-grid--no-spacing mdl-shadow--2dp mdl-color--grey-50">

                <h3 class="h3-upd-cad-tit">Cadastrar Doação</h3>

                    <form class="common-form" method="post" action="/doacao">

                        <!--///BPOINT 2///-->
                        <div class="mdl-cell mdl-cell--12-col mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                            <label class="mdl-color-text--grey-700 mdl-textfield__label" for="titulo">Título *</label>
                            <input class="input-field-custom mdl-textfield__input" type="text" name="titulo" id="titulo" required>
                        </div>

                        <div class="mdl-cell mdl-cell--12-col mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                            <label class="mdl-color-text--grey-700 mdl-textfield__label" for="descricao">Descrição (até 500 caracteres) *</label>
                            <textarea class="mdl-textfield__input" type="text" rows= "5" name="descricao" id="descricao" maxlength="500" required></textarea>
                        </div>

                        <div class="mdl-cell mdl-cell--12-col tipo-label">
                            Tipo *
                        </div>

                        <div class="mdl-cell mdl-cell--12-col">
                        <label class="mdl-radio mdl-js-radio mdl-js-ripple-effect" for="opt1">
                            <input type="radio" id="opt1" class="mdl-radio__button" name="tipo" value="OFERTA" checked>
                            <span class="mdl-radio__label">Oferta</span>
                        </label>
                        <label class="mdl-radio mdl-js-radio mdl-js-ripple-effect custom-radio" for="opt2">
                            <input type="radio" id="opt2" class="mdl-radio__button" name="tipo" value="PEDIDO">
                            <span class="mdl-radio__label">Pedido</span>
                        </label>
                        </div>

                        <button class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect profile-page-buttons mdl-color--primary">
                          Cadastrar Doação
                        </button>

                    </form>
                </section>
                <!--///////////////////////////////////////-->
                <!--////////////////CONTENT////////////////-->
                <!--///////////////////////////////////////-->
            </main>

            <c:import url="/WEB-INF/views/footer.jsp" />

        </div>
        <script src="https://code.getmdl.io/1.2.1/material.min.js"></script>
    </body>
</html>
