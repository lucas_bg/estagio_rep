<header class="mdl-layout__header mdl-layout__header--scroll mdl-color--primary">
    <!--PRIMEIRA PARTE DO SITE-->
    <div class="new-cima"> 
        <a href="/"><img src="/img/logo_2.png" alt="logo" class="logo-principal"></a>

        <a href="/logout" class="logout-btn mdl-button">Sair</a>
    </div>
    <!--PRIMEIRA PARTE DO SITE-->

    <!--MENU-->
    <div class="mdl-layout__tab-bar mdl-js-ripple-effect mdl-color--primary-dark">
        <!--<a href="/" class="mdl-layout__tab">Index</a>-->
        <!--<a href="/features" class="mdl-layout__tab">Features</a>-->
        <a href="/instituicao" class="mdl-layout__tab">
            <img src="/img/instituicao_icone4.png" alt="icone instituicao" class="icones-menu-principal"/>
        </a>
        <a href="/atividade" class="mdl-layout__tab">
            <img src="/img/atividade_icone4.png" alt="icone atividades" class="icones-menu-principal"/>
        </a>
        <a href="/doacao" class="mdl-layout__tab">
            <img src="/img/doacao_icone4.png" alt="icone doacao" class="icones-menu-principal"/>
        </a>
        <a href="/perfil" class="mdl-layout__tab">
            <img src="/img/usuario_icone4.png" alt="icone usuario" class="icones-menu-principal"/>
        </a>
    </div>
    <!--MENU-->
</header>