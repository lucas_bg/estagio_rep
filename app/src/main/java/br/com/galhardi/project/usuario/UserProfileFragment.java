package br.com.galhardi.project.usuario;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.api.client.extensions.android.http.AndroidHttp;
import com.google.api.client.googleapis.services.AbstractGoogleClientRequest;
import com.google.api.client.googleapis.services.GoogleClientRequestInitializer;
import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpRequestInitializer;
import com.google.api.client.json.gson.GsonFactory;

import java.io.IOException;

import br.com.galhardi.instituicao.api.instituicaoEndpoint.InstituicaoEndpoint;
import br.com.galhardi.project.R;
import br.com.galhardi.project.auxiliar.Constantes;
import br.com.galhardi.project.auxiliar.MyDateUtils;
import br.com.galhardi.project.instituicao.AddInstituicaoActivity;
import br.com.galhardi.project.instituicao.InstituicaoDetailsActivity;
import br.com.galhardi.project.localDB.Database;
import br.com.galhardi.voluntarios.api.voluntarioEndpoint.VoluntarioEndpoint;
import br.com.galhardi.voluntarios.api.voluntarioEndpoint.model.Instituicao;
import br.com.galhardi.voluntarios.api.voluntarioEndpoint.model.Response;
import br.com.galhardi.voluntarios.api.voluntarioEndpoint.model.Voluntario;

public class UserProfileFragment extends Fragment implements View.OnClickListener, GoogleApiClient.OnConnectionFailedListener {

    private static final String TAG = "UserProfileFragment";
    public final int REQUEST_CODE_ADD_INSTITUICAO = 3;
    public final int REQUEST_CODE_READ_INSTITUICAO = 4;
    View rootView;
    Button delBt;
    Button readUpdBt;
    EditText senhaEdtTxt;
    ProgressDialog progressDialog;
    Button verInst;
    Button delInst;
    Button updtInst;
    Button cadInst;

    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.usuario_profile_frag, container, false);

        senhaEdtTxt = (EditText) rootView.findViewById(R.id.usu_pro_senha_edt_txt);
        delBt = (Button) rootView.findViewById(R.id.usu_pro_del_bt);
        readUpdBt = (Button) rootView.findViewById(R.id.usu_pro_read_upd_bt);
        verInst = (Button) rootView.findViewById(R.id.usu_pro_ver_inst_bt);
        delInst = (Button) rootView.findViewById(R.id.usu_pro_del_inst_bt);
        updtInst = (Button) rootView.findViewById(R.id.usu_pro_atz_inst_bt);
        cadInst = (Button) rootView.findViewById(R.id.usu_pro_cad_inst_bt);
        TextView txtview1 = (TextView) rootView.findViewById(R.id.txtview1);
        TextView txtview2 = (TextView) rootView.findViewById(R.id.txtview2);
        TextView txtview3 = (TextView) rootView.findViewById(R.id.txtview3);
        TextView txtview4 = (TextView) rootView.findViewById(R.id.txtview4);
        delBt.setOnClickListener(this);
        readUpdBt.setOnClickListener(this);
        verInst.setOnClickListener(this);
        delInst.setOnClickListener(this);
        updtInst.setOnClickListener(this);
        cadInst.setOnClickListener(this);

        Database db = new Database(getContext());
        db.open();
        Voluntario vv = Database.mUserDao.fetchUserById(1);
        db.close();
        Instituicao instituicao = null;
        if (vv != null)
            instituicao = vv.getInstAss();

        if (instituicao == null) {
            verInst.setVisibility(View.GONE);
            delInst.setVisibility(View.GONE);
            updtInst.setVisibility(View.GONE);
            txtview1.setVisibility(View.GONE);
            txtview2.setVisibility(View.GONE);
            txtview3.setVisibility(View.GONE);
        } else {
            txtview4.setVisibility(View.GONE);
            cadInst.setVisibility(View.GONE);
        }

        return rootView;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        dismissProgressDialog();
    }

    private void showProgressDialog() {
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(rootView.getContext());
            progressDialog.setCancelable(false);
            progressDialog.setMessage(getString(R.string.loading));
            progressDialog.setIndeterminate(true);
        }

        progressDialog.show();
    }

    private void dismissProgressDialog() {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.usu_pro_del_bt: {
                String email;
                String senha = senhaEdtTxt.getText().toString();
                if (senha.equals("")) {
                    Toast.makeText(getContext(), "Digite a senha por favor.", Toast.LENGTH_SHORT).show();
                    break;
                }
                Database db = new Database(getContext());
                db.open();
                email = Database.mUserDao.getEmailFromLocalBD();
                db.close();
                showProgressDialog();
                new DeleteUserByEmailAsyncTask(getActivity(), email, senha).execute();
                break;
            }
            case R.id.usu_pro_read_upd_bt: {
                startActivity(new Intent(getActivity(), ReadUpdateActivity.class));
                break;
            }
            case R.id.usu_pro_cad_inst_bt: {
                cadastrarInstituicao();
                break;
            }
            case R.id.usu_pro_atz_inst_bt: {
                atualizarInstituicao();
                break;
            }
            case R.id.usu_pro_ver_inst_bt: {
                verInstituicao();
                break;
            }
            case R.id.usu_pro_del_inst_bt: {
                deletarInstituicao();
                break;
            }
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        dismissProgressDialog();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    private void cadastrarInstituicao() {
        Intent intent = new Intent(getActivity(), AddInstituicaoActivity.class);
        startActivity(intent);
    }

    private void atualizarInstituicao() {
        Database db = new Database(getContext());
        db.open();
        Voluntario vv = Database.mUserDao.fetchUserById(1);
        db.close();
        Instituicao instituicao = null;
        if (vv != null)
            instituicao = vv.getInstAss();

        if (instituicao != null) {
            Intent intent = new Intent(getContext(), AddInstituicaoActivity.class);
            intent.putExtra("UpdateMode", true);
            intent.putExtra("idForUp", instituicao.getId());
            intent.putExtra("nome", instituicao.getNome());
            intent.putExtra("apresentacao", instituicao.getApresentacao());
            intent.putExtra("endereco", instituicao.getEndereco());
            intent.putExtra("email", instituicao.getEmail());
            if (instituicao.getNumMem() != null)
                intent.putExtra("numMem", instituicao.getNumMem().toString());
            else
                intent.putExtra("numMem", "");
            if (instituicao.getNumFunc() != null)
                intent.putExtra("numFunc", instituicao.getNumFunc().toString());
            else
                intent.putExtra("numFunc", "");
            intent.putExtra("descricao_detalhada", instituicao.getDescricaoDetalhada());
            intent.putExtra("website", instituicao.getWebsite());
            intent.putExtra("facebook", instituicao.getFacebook());
            intent.putExtra("retornar", instituicao.getRetornar());
            intent.putExtra("telefone", instituicao.getTelefone());
            String str;
            if (instituicao.getDataFund() != null) {
                str = instituicao.getDataFund().toString();
                intent.putExtra("data_fund", MyDateUtils.RFC3339StrDateToCommonStrDate(str));
            } else
                intent.putExtra("data_fund", "");

            startActivity(intent);
        }
    }

    private void verInstituicao() {
        Database db = new Database(getContext());
        db.open();
        Voluntario vv = Database.mUserDao.fetchUserById(1);
        db.close();
        Instituicao instituicao = null;
        if (vv != null)
            instituicao = vv.getInstAss();

        Intent intent = new Intent(getContext(), InstituicaoDetailsActivity.class);
        intent.putExtra("id", instituicao.getId());
        intent.putExtra("nome", instituicao.getNome());
        intent.putExtra("apresentacao", instituicao.getApresentacao());
        intent.putExtra("endereco", instituicao.getEndereco());
        intent.putExtra("email", instituicao.getEmail());
        if (instituicao.getNumMem() != null)
            intent.putExtra("numMem", instituicao.getNumMem().toString());
        else
            intent.putExtra("numMem", "");
        if (instituicao.getNumFunc() != null)
            intent.putExtra("numFunc", instituicao.getNumFunc().toString());
        else
            intent.putExtra("numFunc", "");
        intent.putExtra("descricao_detalhada", instituicao.getDescricaoDetalhada());
        intent.putExtra("website", instituicao.getWebsite());
        intent.putExtra("facebook", instituicao.getFacebook());
        intent.putExtra("retornar", instituicao.getRetornar());
        intent.putExtra("telefone", instituicao.getTelefone());
        if (instituicao.getDataFund() != null)
            intent.putExtra("data_fund", instituicao.getDataFund().toString());
        else
            intent.putExtra("data_fund", "");

        startActivity(intent);
    }

    private void deletarInstituicao() {
        Database db = new Database(getContext());
        db.open();
        Voluntario vv = Database.mUserDao.fetchUserById(1);
        db.close();
        Instituicao instituicao = null;
        int instituicaoId = -1;
        if (vv != null)
            instituicao = vv.getInstAss();
        if (instituicao != null && instituicao.getId() != null)
            instituicaoId = instituicao.getId();
        if (instituicaoId != -1) {
            Integer[] vvv = {instituicaoId};
            showProgressDialog();
            new DeleteInstituicaoAsyncTask(getContext()).execute(vvv);
        } else {
            Toast.makeText(getContext(), "Erro ao deletar Instituição.", Toast.LENGTH_SHORT).show();
        }
    }

    private class DeleteUserByEmailAsyncTask extends AsyncTask<Void, Void, Boolean> {

        Context context;
        String email;
        String senha;

        public DeleteUserByEmailAsyncTask(Context ct, String em, String s) {
            this.context = ct;
            this.email = em;
            this.senha = s;
        }

        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            VoluntarioEndpoint service;
            try {
                VoluntarioEndpoint.Builder builder;
                builder = new VoluntarioEndpoint.Builder(AndroidHttp.newCompatibleTransport(), new GsonFactory(), null).setHttpRequestInitializer(new HttpRequestInitializer() {
                    @Override
                    public void initialize(HttpRequest request) throws IOException {
                        request.setConnectTimeout(60000);
                        request.setReadTimeout(60000);
                    }
                });
                builder.setApplicationName("Angelos");

                if (Constantes.IS_LOCAL_EXECUTION) {
                    builder.setRootUrl("http://10.0.2.2:8080/_ah/api/")
                            .setGoogleClientRequestInitializer(new GoogleClientRequestInitializer() {
                                @Override
                                public void initialize(AbstractGoogleClientRequest<?> abstractGoogleClientRequest) throws IOException {
                                    abstractGoogleClientRequest.setDisableGZipContent(true);
                                }
                            });
                }
                service = builder.build();
                Response response = service.deleteUserByEmail(email, senha).execute();
                return response.getBooleanResp();
            } catch (Exception e) {
                Log.d(TAG, e.getMessage());
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean resultado) {
            if (resultado) {
//                DatabaseHelper helper = new DatabaseHelper(getContext());
//                SQLiteDatabase db = helper.getWritableDatabase();
//                db.execSQL("DELETE FROM usuario;");
//                db.execSQL("INSERT INTO usuario(isCadastrado, isLogado) values(0, 0);");
//                helper.close();
                Database db = new Database(context);
                db.open();
                Database.mUserDao.logoutInDB();
                db.close();
                Toast.makeText(context, "Conta deletada com sucesso.", Toast.LENGTH_LONG).show();

                ((Activity) context).finish();
                startActivity(new Intent(getActivity(), LoginActivity.class));
            } else {
                dismissProgressDialog();
                Toast.makeText(context, "Erro na exclusão da conta.", Toast.LENGTH_LONG).show();
            }
        }
    }

    private class DeleteInstituicaoAsyncTask extends AsyncTask<Integer, Void, Boolean> {

        Context context;
        br.com.galhardi.instituicao.api.instituicaoEndpoint.model.Response response;

        public DeleteInstituicaoAsyncTask(Context context) {
            this.context = context;
            this.response = new br.com.galhardi.instituicao.api.instituicaoEndpoint.model.Response();
        }

        protected void onPreExecute() {
            super.onPreExecute();
        }

        protected Boolean doInBackground(Integer... params) {
            InstituicaoEndpoint service;
            try {
                InstituicaoEndpoint.Builder builder;
                builder = new InstituicaoEndpoint.Builder(AndroidHttp.newCompatibleTransport(), new GsonFactory(), null).setHttpRequestInitializer(new HttpRequestInitializer() {
                    @Override
                    public void initialize(HttpRequest request) throws IOException {
                        request.setConnectTimeout(60000);
                        request.setReadTimeout(60000);
                    }
                });
                builder.setApplicationName("Angelos");

                if (Constantes.IS_LOCAL_EXECUTION) {
                    builder.setRootUrl("http://10.0.2.2:8080/_ah/api/")
                            .setGoogleClientRequestInitializer(new GoogleClientRequestInitializer() {
                                @Override
                                public void initialize(AbstractGoogleClientRequest<?> abstractGoogleClientRequest) throws IOException {
                                    abstractGoogleClientRequest.setDisableGZipContent(true);
                                }
                            });
                }
                service = builder.build();
                response = service.removeInstituicao(params[0]).execute();
                return true;
            } catch (Exception e) {
                Log.d(TAG, "Erro no uso do Endpoint p excluir." + e.getMessage(), e);
                response.setBooleanResp(false);
                return false;
            }
        }

        protected void onPostExecute(Boolean result) {
            dismissProgressDialog();

            if (result) {
                if (response.getBooleanResp()) {
                    Toast.makeText(getContext(), "Deletado com sucesso", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getContext(), "Falha ao deletar", Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(getContext(), "Falha ao deletar", Toast.LENGTH_SHORT).show();
            }

            Database db = new Database(context);
            db.open();
            Database.mUserDao.logoutInDB();
            db.close();

            ((Activity) context).finish();
            startActivity(new Intent(getActivity(), LoginActivity.class));
        }
    }

}
