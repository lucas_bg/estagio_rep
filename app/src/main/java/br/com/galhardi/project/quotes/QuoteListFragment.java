package br.com.galhardi.project.quotes;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.api.client.extensions.android.http.AndroidHttp;
import com.google.api.client.googleapis.services.AbstractGoogleClientRequest;
import com.google.api.client.googleapis.services.GoogleClientRequestInitializer;
import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpRequestInitializer;
import com.google.api.client.json.gson.GsonFactory;

import java.io.IOException;
import java.util.List;

import br.com.galhardi.project.R;
import br.com.galhardi.project.auxiliar.ConnectionDetector;
import br.com.galhardi.project.auxiliar.Constantes;
import br.com.galhardi.project.auxiliar.RecyclerItemClickListener;
import br.com.galhardi.quotes.api.quoteEndpoint.QuoteEndpoint;
import br.com.galhardi.quotes.api.quoteEndpoint.model.Quote;
import br.com.galhardi.quotes.api.quoteEndpoint.model.QuoteCollection;

public class QuoteListFragment extends Fragment {

    public final int REQUEST_CODE_ADD_QUOTE = 2;
    public int contador;
    QuoteAdapter quoteAdapter;
    RecyclerView quotesRecView;
    SwipeRefreshLayout swipeContainer;
    View rootView;

    // Handles the result of activities called from this fragment
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUEST_CODE_ADD_QUOTE:
                if (resultCode == Activity.RESULT_OK)
                    Toast.makeText(getContext(), "Quote adicionada com sucesso.", Toast.LENGTH_SHORT).show();
                else
                    Toast.makeText(getContext(), "Inserção de nova quote cancelada.", Toast.LENGTH_SHORT).show();
                break;
            default:
                break;
        }
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.quote_list_reciew, container, false);

        // FloatingButton
        FloatingActionButton addButton = (FloatingActionButton) rootView.findViewById(R.id.add_quote_floatbt);
        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), AddQuoteActivity.class);
                startActivityForResult(intent, REQUEST_CODE_ADD_QUOTE);
            }
        });

        // SwipeRefresh
        swipeContainer = (SwipeRefreshLayout) rootView.findViewById(R.id.swipeContainer);
        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                new QuotesListAsyncTask(getActivity()).execute();
            }
        });
        swipeContainer.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);

        // RecyclerView
        quotesRecView = (RecyclerView) rootView.findViewById(R.id.quoteListRecView);
        quotesRecView.setLayoutManager(new LinearLayoutManager(getActivity()));
        quotesRecView.addOnItemTouchListener(
                new RecyclerItemClickListener(getContext(), quotesRecView, new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        Intent intent = new Intent(view.getContext(), QuoteDetailsActivity.class);
                        Quote quote = quoteAdapter.getQuoteByPosition(position);
                        intent.putExtra("who", quote.getWho());
                        intent.putExtra("what", quote.getWhat());
                        intent.putExtra("id", quote.getId());
                        view.getContext().startActivity(intent);
                    }

                    @Override
                    public void onLongItemClick(View view, int position) {
                    }
                })
        );

        contador = 0;
        ConnectionDetector cd = new ConnectionDetector(getContext());
        if (cd.isConnectingToInternet()) {
            new QuotesListAsyncTask(getActivity()).execute();
        } else {
            Toast.makeText(getContext(), "Sem conexão com a Internet", Toast.LENGTH_LONG).show();
        }

        return rootView;
    }

    // Class Holder, basically creates a box linking prog element to view element
    public static class ViewHolder extends RecyclerView.ViewHolder {

        protected TextView quoteWho;
        protected TextView quoteWhat;
        protected TextView quoteId;
        protected ImageView image;

        private ViewHolder(View itemView) {
            super(itemView);

            quoteWho = (TextView) itemView.findViewById(R.id.quote_who);
            quoteWhat = (TextView) itemView.findViewById(R.id.quote_what);
            quoteId = (TextView) itemView.findViewById(R.id.quote_id);
            image = (ImageView) itemView.findViewById(R.id.quote_imgExm);
        }

    }

    // Handles the bind between layout and list
    public static class QuoteAdapter extends RecyclerView.Adapter<ViewHolder> {

        private List<Quote> mQuotes;

        public QuoteAdapter(List<Quote> quotes) {
            mQuotes = quotes;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater
                    .from(parent.getContext())
                    .inflate(R.layout.quote_item_in_list, parent, false);
            return new ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, final int position) {
            Quote quote = mQuotes.get(position);

            holder.quoteId.setText(Integer.toString(quote.getId()));
            holder.quoteWhat.setText(quote.getWhat());
            holder.quoteWho.setText(quote.getWho());
        }

        @Override
        public int getItemCount() {
            return mQuotes.size();
        }

        public Quote getQuoteByPosition(int position) {
            return mQuotes.get(position);
        }

    }

    private class QuotesListAsyncTask extends AsyncTask<Void, Void, List<Quote>> {
        Context context;

        public QuotesListAsyncTask(Context context) {
            this.context = context;
        }

        protected void onPreExecute() {
            super.onPreExecute();
        }

        protected List<Quote> doInBackground(Void... unused) {
            QuoteCollection quotes;
            List<Quote> quoteList = null;
            QuoteEndpoint service;
            try {
                QuoteEndpoint.Builder builder;
                builder = new QuoteEndpoint.Builder(AndroidHttp.newCompatibleTransport(), new GsonFactory(), null).setHttpRequestInitializer(new HttpRequestInitializer() {
                    @Override
                    public void initialize(HttpRequest request) throws IOException {
                        request.setConnectTimeout(60000);
                        request.setReadTimeout(60000);
                    }
                });
                builder.setApplicationName("Angelos");

                if (Constantes.IS_LOCAL_EXECUTION) {
                    builder.setRootUrl("http://10.0.2.2:8080/_ah/api/")
                            .setGoogleClientRequestInitializer(new GoogleClientRequestInitializer() {
                                @Override
                                public void initialize(AbstractGoogleClientRequest<?> abstractGoogleClientRequest) throws IOException {
                                    abstractGoogleClientRequest.setDisableGZipContent(true);
                                }
                            });
                }
                service = builder.build();
                quotes = service.listQuote().execute();
                if (quotes != null)
                    quoteList = quotes.getItems();
            } catch (Exception e) {
                Log.d("QuoteList", e.getMessage(), e);
            }
            return quoteList;
        }

        protected void onPostExecute(List<Quote> quotes) {
            if (quotes == null) {
                contador++;
                if (contador < 5)
                    new QuotesListAsyncTask(getActivity()).execute();
            } else {
                quoteAdapter = new QuoteAdapter(quotes);
                quotesRecView.setAdapter(quoteAdapter);
                quoteAdapter.notifyDataSetChanged();
                swipeContainer.setRefreshing(false);
            }
//            int curSize = quoteAdapter.getItemCount();
//            quoteArrayList.addAll(quotes);
//            quoteAdapter.notifyItemRangeInserted(curSize, quotes.size());
        }

    }

}
