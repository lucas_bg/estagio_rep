package br.com.galhardi.project.auxiliar;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class MyDateUtils {

    public static String commonStrDateToRFC3339StrDate(String str) {
        String peds[] = str.split("/");
        return peds[2] + "-" + peds[1] + "-" + peds[0];
    }

    public static boolean isInCommonValidFormat(String value) {
        String format = "dd/MM/yyyy";
        Date date = null;
        Date maxDate = null;
        try {
            SimpleDateFormat sdf = new SimpleDateFormat(format);
            maxDate = sdf.parse("01/01/3000");
            date = sdf.parse(value);
            if (!value.equals(sdf.format(date))) {
                date = null;
            }
        } catch (ParseException ignored) {

        }
        return date != null && (!date.after(maxDate));
    }
    //1988-01-13T00:00:00.000Z

    public static String RFC3339StrDateToCommonStrDate(String data) {
        String temp[] = data.split("T");
        String onlyDate = temp[0];
        String divs[] = onlyDate.split("-");
        return divs[2] + "/" + divs[1] + "/" + divs[0];
    }

}
