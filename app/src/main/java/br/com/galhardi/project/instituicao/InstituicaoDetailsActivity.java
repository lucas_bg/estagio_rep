package br.com.galhardi.project.instituicao;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.api.client.util.DateTime;
import com.squareup.picasso.Picasso;

import br.com.galhardi.instituicao.api.instituicaoEndpoint.model.Instituicao;
import br.com.galhardi.project.R;
import br.com.galhardi.project.auxiliar.Constantes;
import br.com.galhardi.project.auxiliar.MyDateUtils;

public class InstituicaoDetailsActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = "InstituicaoDetails";

    public ProgressDialog progressDialog;
    private Integer instituicaoId;
    private TextView tvEmail;
    private TextView tvfacebook;
    private TextView tvwebsite;
    private TextView tvretornar;
    private TextView tvEndereco;
    private TextView tvtelefone;
    private Instituicao instituicao;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.instituicao_details);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setIcon(R.mipmap.logo_3);
        getSupportActionBar().setElevation(0);
        this.instituicao = new Instituicao();

        //POINT 6
        TextView tvNome = (TextView) findViewById(R.id.dtlInstNome);
        TextView tvApresentacao = (TextView) findViewById(R.id.dtlInstApresentacao);
        tvEmail = (TextView) findViewById(R.id.dtlInstEmail);
        tvEndereco = (TextView) findViewById(R.id.dtlInstEndereco);
        TextView tvnumMem = (TextView) findViewById(R.id.dtlInstnumMem);
        TextView tvnumFunc = (TextView) findViewById(R.id.dtlInstnumFunc);
        TextView tvdescricao_detalhada = (TextView) findViewById(R.id.dtlInstdescricao_detalhada);
        tvwebsite = (TextView) findViewById(R.id.dtlInstwebsite);
        tvfacebook = (TextView) findViewById(R.id.dtlInstfacebook);
        tvretornar = (TextView) findViewById(R.id.dtlInstretornar);
        tvtelefone = (TextView) findViewById(R.id.dtlInsttelefone);
        TextView tvdata_fund = (TextView) findViewById(R.id.dtlInstdata_fund);
        ImageView img = (ImageView) findViewById(R.id.inst_img_id);
        ImageView imgViewEmailIcon = (ImageView) findViewById(R.id.emailToIcon);
        imgViewEmailIcon.setOnClickListener(this);
        ImageView imgViewFaceIcon = (ImageView) findViewById(R.id.faceLogoIcon);
        imgViewFaceIcon.setOnClickListener(this);
        ImageView imgViewRetornar = (ImageView) findViewById(R.id.RetornarLogoIcon);
        imgViewRetornar.setOnClickListener(this);
        ImageView imgViewWebsite = (ImageView) findViewById(R.id.WebsiteLogoIcon);
        imgViewWebsite.setOnClickListener(this);
        ImageView imgViewMapLogo = (ImageView) findViewById(R.id.mapIcon);
        imgViewMapLogo.setOnClickListener(this);
        ImageView imgViewPhoneIcon = (ImageView) findViewById(R.id.phoneIcon);
        imgViewPhoneIcon.setOnClickListener(this);

        Intent intent = getIntent();
        instituicaoId = intent.getIntExtra("id", -1);

        Picasso.with(this)
                .load(Constantes.WEB_APPLICATION_ADDRESS + "/instituicao/getImg?id=" + instituicaoId)
                .placeholder(R.mipmap.default_img)
                .into(img);

        // POINT 7
        tvApresentacao.setText(intent.getStringExtra("apresentacao"));
        tvEmail.setText(intent.getStringExtra("email"));
        tvEndereco.setText(intent.getStringExtra("endereco"));
        tvNome.setText(intent.getStringExtra("nome"));
        tvnumMem.setText(intent.getStringExtra("numMem"));
        tvnumFunc.setText(intent.getStringExtra("numFunc"));
        tvdescricao_detalhada.setText(intent.getStringExtra("descricao_detalhada"));
        tvwebsite.setText(intent.getStringExtra("website"));
        tvfacebook.setText(intent.getStringExtra("facebook"));
        tvretornar.setText(intent.getStringExtra("retornar"));
        tvtelefone.setText(intent.getStringExtra("telefone"));
        String RFC3339StrDate = intent.getStringExtra("data_fund");
        tvdata_fund.setText(MyDateUtils.RFC3339StrDateToCommonStrDate(RFC3339StrDate));
        instituicao.setApresentacao(intent.getStringExtra("apresentacao"));
        instituicao.setNome(intent.getStringExtra("nome"));
        instituicao.setEndereco(intent.getStringExtra("endereco"));
        instituicao.setEmail(intent.getStringExtra("email"));
        instituicao.setNumMem(Integer.valueOf(intent.getStringExtra("numMem")));
        instituicao.setNumFunc(Integer.valueOf(intent.getStringExtra("numFunc")));
        instituicao.setDescricaoDetalhada(intent.getStringExtra("descricao_detalhada"));
        instituicao.setWebsite(intent.getStringExtra("website"));
        instituicao.setFacebook(intent.getStringExtra("facebook"));
        instituicao.setRetornar(intent.getStringExtra("retornar"));
        instituicao.setTelefone(intent.getStringExtra("telefone"));
        instituicao.setDataFund(DateTime.parseRfc3339(RFC3339StrDate));
        instituicao.setId(instituicaoId);
    }

    private void showProgressDialog() {
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(this);
            progressDialog.setCancelable(false);
            progressDialog.setMessage("Deletando Instituição...");
            progressDialog.setIndeterminate(true);
        }

        progressDialog.show();
    }

    private void dismissProgressDialog() {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }

    private void sendEmailTo(String destinyEmail) {
        Intent emailIntent = new Intent(Intent.ACTION_SEND);
        emailIntent.setData(Uri.parse("mailto:"));
        emailIntent.setType("text/plain");
        String[] emails = {destinyEmail};
        emailIntent.putExtra(Intent.EXTRA_EMAIL, emails);
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Achei sua instituição no Angelos! Quero saber mais.");

        try {
            startActivity(Intent.createChooser(emailIntent, "Enviando email..."));
            finish();
        } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(InstituicaoDetailsActivity.this, "Sem clientes de email instalados.", Toast.LENGTH_SHORT).show();
        }
    }

    public String getFacebookPageURL(Context context, String FACEBOOK_URL, String FACEBOOK_PAGE_ID) {
        PackageManager packageManager = context.getPackageManager();
        try {
            int versionCode = packageManager.getPackageInfo("com.facebook.katana", 0).versionCode;
            if (versionCode >= 3002850) { //newer versions of fb app
                return "fb://facewebmodal/f?href=" + FACEBOOK_URL;
            } else { //older versions of fb app
                return "fb://page/" + FACEBOOK_PAGE_ID;
            }
        } catch (PackageManager.NameNotFoundException e) {
            return FACEBOOK_URL; //normal web url
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
//            case R.id.inst_dtl_del_bt: {
//                if (instituicaoId != -1) {
//                    Integer[] vv = {instituicaoId};
//                    showProgressDialog();
//                    new DeleteInstituicaoAsyncTask(InstituicaoDetailsActivity.this).execute(vv);
//                } else {
//                    Toast.makeText(this, "Erro.", Toast.LENGTH_SHORT).show();
//                }
//                break;
//            }
//            case R.id.inst_dtl_updt_bt: {
//                Intent intent = new Intent(InstituicaoDetailsActivity.this,
//                        AddInstituicaoActivity.class);
//                intent.putExtra("UpdateMode", true);
//                intent.putExtra("idForUp", instituicao.getId());
//                intent.putExtra("nome", instituicao.getNome());
//                intent.putExtra("apresentacao", instituicao.getApresentacao());
//                intent.putExtra("endereco", instituicao.getEndereco());
//                intent.putExtra("email", instituicao.getEmail());
//                intent.putExtra("numMem", instituicao.getNumMem().toString());
//                intent.putExtra("numFunc", instituicao.getNumFunc().toString());
//                intent.putExtra("descricao_detalhada", instituicao.getDescricaoDetalhada());
//                intent.putExtra("website", instituicao.getWebsite());
//                intent.putExtra("facebook", instituicao.getFacebook());
//                intent.putExtra("retornar", instituicao.getRetornar());
//                intent.putExtra("telefone", instituicao.getTelefone());
//                String str = instituicao.getDataFund().toString();
//                intent.putExtra("data_fund", MyDateUtils.RFC3339StrDateToCommonStrDate(str));
//
//                startActivity(intent);
//                finish();
//                break;
//            }
            case R.id.emailToIcon: {
                sendEmailTo(tvEmail.getText().toString().trim());
                break;
            }
            case R.id.faceLogoIcon: {
//                Intent intent = new Intent(Intent.ACTION_VIEW,
//                        Uri.parse("http://" + tvfacebook.getText().toString().trim()));
//                startActivity(intent);
                Intent facebookIntent = new Intent(Intent.ACTION_VIEW);
                String base = tvfacebook.getText().toString().trim();
                String teemp[] = base.split("/");
                String part2 = teemp[teemp.length - 1];
                String facebookUrl = getFacebookPageURL(this, "https://" + base, part2);
                facebookIntent.setData(Uri.parse(facebookUrl));
                startActivity(facebookIntent);
                break;
            }
            case R.id.WebsiteLogoIcon: {
                Intent intent = new Intent(Intent.ACTION_VIEW,
                        Uri.parse("http://" + tvwebsite.getText().toString().trim()));
                startActivity(intent);
                break;
            }
            case R.id.RetornarLogoIcon: {
                Intent intent = new Intent(Intent.ACTION_VIEW,
                        Uri.parse("http://" + tvretornar.getText().toString().trim()));
                startActivity(intent);
                break;
            }
            case R.id.mapIcon: {
                Uri mapaUri = Uri.parse("geo:0,0?q=" + tvEndereco.getText().toString().trim() + ", Londrina");
                Intent mapIntent = new Intent(Intent.ACTION_VIEW, mapaUri);
                mapIntent.setPackage("com.google.android.apps.maps");
                startActivity(mapIntent);
                break;
            }
            case R.id.phoneIcon: {
                Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + tvtelefone.getText().toString().trim()));
                startActivity(intent);
                break;
            }
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        dismissProgressDialog();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        dismissProgressDialog();
    }

}
