package br.com.galhardi.project.localDB;

public interface IUserSchema {

    String USER_TABLE = "users";


    String COLUMN_ID = "_id";
    String COLUMN_USER_NAME = "user_name";
    String COLUMN_EMAIL = "email";
    String COLUMN_IS_CADASTRADO = "is_cadastrado";
    String COLUMN_IS_LOGADO = "is_logado";
    String COLUMN_NOME = "nome";
    String COLUMN_SOBRENOME = "sobrenome";
    String COLUMN_SENHA = "senha";
    String COLUMN_TELEFONE = "telefone";
    String COLUMN_CELULAR = "celular";
    String COLUMN_RG = "rg";
    String COLUMN_DATA_NASC = "data_nasc";
    String COLUMN_ID_GOOGLE = "id_google";
    String COLUMN_ID_USER = "id_user";
    String COLUMN_ID_INST = "id_inst";
    String COLUMN_NUM_MEM = "num_mem";
    String COLUMN_NUM_FUNC = "num_func";
    String COLUMN_IS_APROVADA = "is_aprovada";
    String COLUMN_EMAIL_INST = "email_inst";
    String COLUMN_TELEFONE_INST = "telefone_inst";
    String COLUMN_NOME_INST = "nome_inst";
    String COLUMN_APRESENTACAO = "apresentacao";
    String COLUMN_ENDERECO = "endereco";
    String COLUMN_DESCRICAO = "descricao";
    String COLUMN_FACEBOOK = "facebook";
    String COLUMN_WEBSITE = "website";
    String COLUMN_RETORNAR = "retornar";
    String COLUMN_IS_MEM = "is_mem";
    String COLUMN_IS_REP = "is_rep";
    String COLUMN_DATA_FUND = "data_fund";


    String USER_TABLE_CREATE = "CREATE TABLE IF NOT EXISTS "
            + USER_TABLE
            + " ("
            + COLUMN_ID
            + " INTEGER PRIMARY KEY, "
            + COLUMN_USER_NAME
            + " TEXT, "
            + COLUMN_IS_CADASTRADO
            + " INTEGER, "
            + COLUMN_IS_LOGADO
            + " INTEGER, "
            + COLUMN_NOME
            + " TEXT, "
            + COLUMN_SOBRENOME
            + " TEXT, "
            + COLUMN_SENHA
            + " TEXT, "
            + COLUMN_TELEFONE
            + " TEXT, "
            + COLUMN_CELULAR
            + " TEXT, "
            + COLUMN_RG
            + " TEXT, "
            + COLUMN_DATA_NASC
            + " TEXT, "
            + COLUMN_ID_GOOGLE
            + " INTEGER, "
            + COLUMN_ID_USER
            + " INTEGER, "
            + COLUMN_ID_INST
            + " INTEGER, "
            + COLUMN_NUM_MEM
            + " INTEGER, "
            + COLUMN_NUM_FUNC
            + " INTEGER, "
            + COLUMN_IS_APROVADA
            + " INTEGER, "
            + COLUMN_EMAIL_INST
            + " TEXT, "
            + COLUMN_TELEFONE_INST
            + " TEXT, "
            + COLUMN_NOME_INST
            + " TEXT, "
            + COLUMN_APRESENTACAO
            + " TEXT, "
            + COLUMN_ENDERECO
            + " TEXT, "
            + COLUMN_DESCRICAO
            + " TEXT, "
            + COLUMN_FACEBOOK
            + " TEXT, "
            + COLUMN_WEBSITE
            + " TEXT, "
            + COLUMN_RETORNAR
            + " TEXT, "
            + COLUMN_IS_MEM
            + " INTEGER, "
            + COLUMN_IS_REP
            + " INTEGER, "
            + COLUMN_DATA_FUND
            + " TEXT, "
            + COLUMN_EMAIL
            + " TEXT "
            + ")";


    String[] USER_COLUMNS = new String[]{
            COLUMN_ID,
            COLUMN_USER_NAME,
            COLUMN_IS_CADASTRADO,
            COLUMN_IS_LOGADO,
            COLUMN_NOME,
            COLUMN_SOBRENOME,
            COLUMN_SENHA,
            COLUMN_TELEFONE,
            COLUMN_CELULAR,
            COLUMN_RG,
            COLUMN_DATA_NASC,
            COLUMN_ID_GOOGLE,
            COLUMN_ID_USER,
            COLUMN_ID_INST,
            COLUMN_NUM_MEM,
            COLUMN_NUM_FUNC,
            COLUMN_IS_APROVADA,
            COLUMN_EMAIL_INST,
            COLUMN_TELEFONE_INST,
            COLUMN_NOME_INST,
            COLUMN_APRESENTACAO,
            COLUMN_ENDERECO,
            COLUMN_DESCRICAO,
            COLUMN_FACEBOOK,
            COLUMN_WEBSITE,
            COLUMN_RETORNAR,
            COLUMN_IS_MEM,
            COLUMN_IS_REP,
            COLUMN_DATA_FUND,
            COLUMN_EMAIL
    };

}
