package br.com.galhardi.project.localDB;

import br.com.galhardi.voluntarios.api.voluntarioEndpoint.model.Voluntario;

public interface IUserDao {

    Voluntario fetchUserById(int userId);

    boolean addUser(Voluntario user, boolean isCadastrado, boolean isLogado, int id);

    boolean logoutInDB();

    String getEmailFromLocalBD();

    CadELog cadELogMet();

}
