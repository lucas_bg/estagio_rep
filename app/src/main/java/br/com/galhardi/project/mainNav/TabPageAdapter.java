package br.com.galhardi.project.mainNav;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import br.com.galhardi.project.ZTempPageFragment;
import br.com.galhardi.project.instituicao.InstituicaoListFragment;
import br.com.galhardi.project.usuario.UserProfileFragment;

public class TabPageAdapter extends FragmentStatePagerAdapter {

    private static final int NUM_TABS = 4;

    public TabPageAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new InstituicaoListFragment();
            case 1:
                return ZTempPageFragment.newInstance(position + 1);
            case 2:
                return ZTempPageFragment.newInstance(position + 1);
            case 3:
                return new UserProfileFragment();
            default:
                return ZTempPageFragment.newInstance(position + 1);
        }
    }

    @Override
    public int getCount() {
        return NUM_TABS;
    }

//    @Override
//    public CharSequence getPageTitle(int position) {
//        switch (position) {
//            case 0:
//                return "Inst";
//            case 1:
//                return "Ativ";
//            case 2:
//                return "Doação";
//            case 3:
//                return "Perfil";
//            default:
//                return "";
//        }
//    }

}
