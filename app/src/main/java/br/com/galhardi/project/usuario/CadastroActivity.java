package br.com.galhardi.project.usuario;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.api.client.extensions.android.http.AndroidHttp;
import com.google.api.client.googleapis.services.AbstractGoogleClientRequest;
import com.google.api.client.googleapis.services.GoogleClientRequestInitializer;
import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpRequestInitializer;
import com.google.api.client.json.gson.GsonFactory;
import com.google.api.client.util.DateTime;

import java.io.IOException;

import br.com.galhardi.project.R;
import br.com.galhardi.project.auxiliar.Constantes;
import br.com.galhardi.project.auxiliar.MyDateUtils;
import br.com.galhardi.project.auxiliar.TextValidator;
import br.com.galhardi.voluntarios.api.voluntarioEndpoint.VoluntarioEndpoint;
import br.com.galhardi.voluntarios.api.voluntarioEndpoint.model.Response;
import br.com.galhardi.voluntarios.api.voluntarioEndpoint.model.Voluntario;

//import br.com.galhardi.project.localDB.DatabaseHelper;

public class CadastroActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = "CADASTRO_ACTIVITY";
    ProgressDialog progressDialog;
    private EditText cad_usu_nome;
    private EditText cad_usu_sobrenome;
    private EditText cad_usu_cel;
    private EditText cad_usu_tel;
    private EditText cad_usu_email;
    private EditText cad_usu_dt_nasc;
    private EditText cad_usu_senha;
    private EditText cad_usu_senha_confirm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.usuario_cadastro_actv);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setIcon(R.mipmap.logo_3);
        getSupportActionBar().setElevation(0);

        setResult(Constantes.RESULT_ACTION_CANCELED);

        findViewById(R.id.bt_Cadastrar).setOnClickListener(this);

        cad_usu_nome = (EditText) findViewById(R.id.cad_usu_nome_e);
        cad_usu_sobrenome = (EditText) findViewById(R.id.cad_usu_sobrenome_e);
        cad_usu_cel = (EditText) findViewById(R.id.cad_usu_cel_e);
        cad_usu_tel = (EditText) findViewById(R.id.cad_usu_tel_e);
        cad_usu_email = (EditText) findViewById(R.id.cad_usu_email_e);
        cad_usu_dt_nasc = (EditText) findViewById(R.id.cad_usu_dt_nasc_e);
        cad_usu_senha = (EditText) findViewById(R.id.cad_usu_senha_e);
        cad_usu_senha_confirm = (EditText) findViewById(R.id.cad_usu_senha_confirm_e);

        setValidators();
    }

    private void showProgressDialog() {
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(this);
            progressDialog.setCancelable(false);
            progressDialog.setMessage(getString(R.string.loading));
            progressDialog.setIndeterminate(true);
        }

        progressDialog.show();
    }

    private void dismissProgressDialog() {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        dismissProgressDialog();
    }

    @Override
    protected void onPause() {
        super.onPause();
        dismissProgressDialog();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.bt_Cadastrar: {
                if (!isAllFieldsOk()) {
                    Toast.makeText(this, "Preencha todos os campos corretamente.", Toast.LENGTH_LONG).show();
                    return;
                }

                Voluntario voluntario = new Voluntario();

                voluntario.setSenha(cad_usu_senha.getText().toString());
                voluntario.setCelular(cad_usu_cel.getText().toString());
                voluntario.setEmail(cad_usu_email.getText().toString());
                voluntario.setNome(cad_usu_nome.getText().toString());
                voluntario.setSNome(cad_usu_sobrenome.getText().toString());
                voluntario.setTelefone(cad_usu_tel.getText().toString());

                if (voluntario.getTelefone().equals("") || voluntario.getTelefone().length() == 0)
                    voluntario.setTelefone(null);

                String txtData_nasc = cad_usu_dt_nasc.getText().toString().trim();
                if (MyDateUtils.isInCommonValidFormat(txtData_nasc)) {
                    String dt = MyDateUtils.commonStrDateToRFC3339StrDate(txtData_nasc);
                    voluntario.setDataNasc(DateTime.parseRfc3339(dt));
                }

                showProgressDialog();
                new CadastroAsyncTask(CadastroActivity.this, voluntario).execute();
                break;
            }
        }
    }

    public void setValidators() {
        cad_usu_nome.setError("Campo obrigatório.");
        cad_usu_nome.addTextChangedListener(new TextValidator(cad_usu_nome) {
            @Override
            public void validate(TextView textView, String text) {
                if (text.length() == 0) {
                    textView.setError("Campo obrigatório.");
                }
            }
        });
        cad_usu_sobrenome.setError("Campo obrigatório.");
        cad_usu_sobrenome.addTextChangedListener(new TextValidator(cad_usu_sobrenome) {
            @Override
            public void validate(TextView textView, String text) {
                if (text.length() == 0) {
                    textView.setError("Campo obrigatório.");
                }
            }
        });
        cad_usu_cel.setError("Campo obrigatório.");
        cad_usu_cel.addTextChangedListener(new TextValidator(cad_usu_cel) {
            @Override
            public void validate(TextView textView, String text) {
                if (text.length() == 0) {
                    textView.setError("Campo obrigatório.");
                }
            }
        });
        cad_usu_email.setError("Campo obrigatório.");
        cad_usu_email.addTextChangedListener(new TextValidator(cad_usu_email) {
            @Override
            public void validate(TextView textView, String text) {
                if (text.length() == 0) {
                    textView.setError("Campo obrigatório.");
                }
            }
        });
        cad_usu_dt_nasc.setError("Insira uma data válida. Campo obrigatório.");
        cad_usu_dt_nasc.addTextChangedListener(new TextValidator(cad_usu_dt_nasc) {
            @Override
            public void validate(TextView textView, String text) {
                if ((!MyDateUtils.isInCommonValidFormat(text)) || text.length() == 0) {
                    textView.setError("Insira uma data válida. Campo obrigatório.");
                }
            }
        });
        cad_usu_senha.setError("Campo obrigatório.");
        cad_usu_senha.addTextChangedListener(new TextValidator(cad_usu_senha) {
            @Override
            public void validate(TextView textView, String text) {
                if (text.length() == 0) {
                    textView.setError("Campo obrigatório.");
                }
            }
        });
        cad_usu_senha_confirm.setError("Campo obrigatório.");
        cad_usu_senha_confirm.addTextChangedListener(new TextValidator(cad_usu_senha_confirm) {
            @Override
            public void validate(TextView textView, String text) {
                if (text.length() == 0) {
                    textView.setError("Campo obrigatório.");
                } else {
                    String senha = cad_usu_senha.getText().toString().trim();
                    if (!text.equals(senha)) {
                        textView.setError("Senhas não estão iguais.");
                    }
                }
            }
        });
    }

    private boolean isAllFieldsOk() {
        boolean isOk = true;
        isOk = isOk && cad_usu_nome.getError() == null;
        isOk = isOk && cad_usu_sobrenome.getError() == null;
        isOk = isOk && cad_usu_cel.getError() == null;
        isOk = isOk && cad_usu_email.getError() == null;
        isOk = isOk && cad_usu_dt_nasc.getError() == null;
        isOk = isOk && cad_usu_senha.getError() == null;
        isOk = isOk && cad_usu_senha_confirm.getError() == null;
        return isOk;
    }

    private class CadastroAsyncTask extends AsyncTask<Void, Void, Boolean> {

        Context context;
        Voluntario voluntario;

        public CadastroAsyncTask(Context context, Voluntario voluntario) {
            this.context = context;
            this.voluntario = voluntario;
        }

        protected void onPreExecute() {
            super.onPreExecute();
        }

        protected Boolean doInBackground(Void... params) {
            VoluntarioEndpoint service;
            try {
                VoluntarioEndpoint.Builder builder;
                builder = new VoluntarioEndpoint.Builder(AndroidHttp.newCompatibleTransport(), new GsonFactory(), null).setHttpRequestInitializer(new HttpRequestInitializer() {
                    @Override
                    public void initialize(HttpRequest request) throws IOException {
                        request.setConnectTimeout(60000);
                        request.setReadTimeout(60000);
                    }
                });
                builder.setApplicationName("Angelos");

                if (Constantes.IS_LOCAL_EXECUTION) {
                    builder.setRootUrl("http://10.0.2.2:8080/_ah/api/")
                            .setGoogleClientRequestInitializer(new GoogleClientRequestInitializer() {
                                @Override
                                public void initialize(AbstractGoogleClientRequest<?> abstractGoogleClientRequest) throws IOException {
                                    abstractGoogleClientRequest.setDisableGZipContent(true);
                                }
                            });
                }
                service = builder.build();
                Response response = service.insertNewVolunteer(voluntario).execute();
                return response.getBooleanResp();
            } catch (Exception e) {
                Log.d(TAG, e.getMessage());
                return false;
            }
        }

        protected void onPostExecute(Boolean responseFromReq) {
            if (responseFromReq) {

                    setResult(Activity.RESULT_OK);
                    finish();

            } else {
                Log.d(TAG, "FALHA CADASTRO USER.");
                setResult(Activity.RESULT_CANCELED);
                finish();
            }
        }

    }
}
