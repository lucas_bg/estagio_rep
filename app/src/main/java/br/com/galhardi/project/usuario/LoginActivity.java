package br.com.galhardi.project.usuario;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.api.client.extensions.android.http.AndroidHttp;
import com.google.api.client.googleapis.services.AbstractGoogleClientRequest;
import com.google.api.client.googleapis.services.GoogleClientRequestInitializer;
import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpRequestInitializer;
import com.google.api.client.json.gson.GsonFactory;

import java.io.IOException;

import br.com.galhardi.project.R;
import br.com.galhardi.project.auxiliar.Constantes;
import br.com.galhardi.project.localDB.Database;
import br.com.galhardi.project.mainNav.TabActivity;
import br.com.galhardi.voluntarios.api.voluntarioEndpoint.VoluntarioEndpoint;
import br.com.galhardi.voluntarios.api.voluntarioEndpoint.model.Response;
import br.com.galhardi.voluntarios.api.voluntarioEndpoint.model.Voluntario;

//import br.com.galhardi.project.localDB.DatabaseHelper;

//import com.google.android.gms.auth.api.Auth;
//import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
//import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
//import com.google.android.gms.auth.api.signin.GoogleSignInResult;
//import com.google.android.gms.common.api.OptionalPendingResult;
//import com.google.android.gms.common.api.ResultCallback;
//import com.google.android.gms.common.api.Status;

public class LoginActivity extends AppCompatActivity implements
        GoogleApiClient.OnConnectionFailedListener,
        View.OnClickListener, View.OnFocusChangeListener {

    private static final String TAG = "LOGIN_ACTIVITY";
    private static final int RC_SIGN_IN = 9001;
    private static final int CADASTRO_REQUEST_CODE = 3;
    public boolean fromCadAct = false;
    public boolean assignGoogleIdNow = false;
    public String userEmail;
    View decorView;
    private GoogleApiClient mGoogleApiClient;
    private ProgressDialog mProgressDialog;
    private EditText login_ed;
    private EditText senha_ed;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.usuario_sign_in_actv);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Window w = getWindow(); // in Activity's onCreate() for instance
            w.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
        }

        Intent intent = getIntent();
        fromCadAct = intent.getBooleanExtra("fromCadAct", false);

//        findViewById(R.id.sign_in_button).setOnClickListener(this);
        findViewById(R.id.bt_login_email).setOnClickListener(this);
        findViewById(R.id.bt_cadastre_se).setOnClickListener(this);
        findViewById(R.id.emailToLogin).setOnFocusChangeListener(this);
        findViewById(R.id.senhaToLogin).setOnFocusChangeListener(this);

        login_ed = (EditText) findViewById(R.id.emailToLogin);
        senha_ed = (EditText) findViewById(R.id.senhaToLogin);

//        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
//                .requestEmail()
//                .build();
//
//        mGoogleApiClient = new GoogleApiClient.Builder(this)
//                .enableAutoManage(this /* FragmentActivity */, this /* OnConnectionFailedListener */)
//                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
//                .build();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        dismissProgressDialog();
    }

    @Override
    protected void onPause() {
        super.onPause();
        dismissProgressDialog();
    }

    @Override
    public void onStart() {
        super.onStart();

//        OptionalPendingResult<GoogleSignInResult> opr = Auth.GoogleSignInApi.silentSignIn(mGoogleApiClient);
//        if (opr.isDone()) {
//            // If the user's cached credentials are valid, the OptionalPendingResult will be "done"
//            // and the GoogleSignInResult will be available instantly.
//            GoogleSignInResult result = opr.get();
//            handleSignInResult(result, false);
//        } else {
//            // If the user has not previously signed in on this device or the sign-in has expired,
//            // this asynchronous branch will attempt to sign in the user silently.  Cross-device
//            // single sign-on will occur in this branch.
//            showProgressDialog();
//            opr.setResultCallback(new ResultCallback<GoogleSignInResult>() {
//                @Override
//                public void onResult(@NonNull GoogleSignInResult googleSignInResult) {
//                    dismissProgressDialog();
//                    handleSignInResult(googleSignInResult, false);
//                }
//            });
//        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
//            case RC_SIGN_IN: {
//                GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
//                handleSignInResult(result, true);
//                break;
//            }
            case CADASTRO_REQUEST_CODE: {
                if (resultCode == Activity.RESULT_OK) {
                    Toast.makeText(this, "Cadastrado com sucesso! Login disponível.", Toast.LENGTH_SHORT).show();
                } else if (resultCode == Constantes.RESULT_ACTION_CANCELED) {
                    Toast.makeText(this, "Cadastrado cancelado.", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(this, "Algo deu errado. Tente novamente.", Toast.LENGTH_SHORT).show();
                }
                break;
            }
        }
    }

//    private void handleSignInResult(GoogleSignInResult result, boolean fromBt) {
//        if (result.isSuccess()) {
//            GoogleSignInAccount acct = result.getSignInAccount();
//            if (acct != null) {
//                if (fromCadAct || assignGoogleIdNow) {
//                    assignGoogleIdNow = false;
//                    firstTimeGoogleLogin(acct.getId());
//                } else {
//                    DatabaseHelper helper = new DatabaseHelper(this);
//                    SQLiteDatabase db = helper.getWritableDatabase();
//
//                    ContentValues values = new ContentValues();
//                    values.put("isLogado", 1);
//                    values.put("email", userEmail);
//                    db.update("usuario", values, null, null);
//
//                    db.close();
//                    helper.close();
//
//                    finish();
//                    startActivity(new Intent(this, TabActivity.class));
//                }
//            }
//        } else {
//            if (fromBt)
//                Toast.makeText(this, "O Google Login falhou", Toast.LENGTH_SHORT).show();
//            else
//                Log.d(TAG, "O Google Login falhou");
//        }
//    }

//    private void signIn() {
//        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
//        startActivityForResult(signInIntent, RC_SIGN_IN);
//    }
//
//    private void signOut() {
//        Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(
//                new ResultCallback<Status>() {
//                    @Override
//                    public void onResult(@NonNull Status status) {
//
//                    }
//                });
//    }
//
//    private void revokeAccess() {
//        // disconnect
//        Auth.GoogleSignInApi.revokeAccess(mGoogleApiClient).setResultCallback(
//                new ResultCallback<Status>() {
//                    @Override
//                    public void onResult(@NonNull Status status) {
//
//                    }
//                });
//    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.d(TAG, "onConnectionFailed:" + connectionResult);
    }

    private void showProgressDialog() {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(this);
            mProgressDialog.setCancelable(false);
            mProgressDialog.setMessage(getString(R.string.loading));
            mProgressDialog.setIndeterminate(true);
        }

        mProgressDialog.show();
    }

    private void dismissProgressDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }

    public void loginByEmail() {
        String email = login_ed.getText().toString();
        String senha = senha_ed.getText().toString();


        if (email.equals("") || senha.equals("")) {
            Toast.makeText(this, "Preencha o email e a senha.", Toast.LENGTH_LONG).show();
            return;
        }

        showProgressDialog();
        new LoginAsyncTask(LoginActivity.this, email, senha).execute();
    }

//    public void firstTimeGoogleLogin(String userId) {
//        userEmail = login_ed.getText().toString();
//        new AssignGoogleIdAsyncTask(LoginActivity.this, userId).execute();
//    }

//    public void primeiroVerfCadEmail() {
//        String email = login_ed.getText().toString();
//
//        if (email.equals("")) {
//            Toast.makeText(this, "Preencha o email.", Toast.LENGTH_LONG).show();
//            return;
//        }
//
//        showProgressDialog();
//        new LoginAsyncTask(LoginActivity.this, email, "  ", true).execute();
//    }

    public void irCadastrar() {
        Intent intent = new Intent(LoginActivity.this, CadastroActivity.class);
        startActivityForResult(intent, CADASTRO_REQUEST_CODE);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
//            case R.id.sign_in_button:
//                primeiroVerfCadEmail();
//                break;
//            case R.id.sign_out_button:
//                signOut();
//                break;
//            case R.id.disconnect_button:
//                revokeAccess();
//                break;
            case R.id.bt_login_email:
                loginByEmail();
                break;
            case R.id.bt_cadastre_se:
                irCadastrar();
                break;
        }
    }

//    @Override
//    public boolean onTouch(View v, MotionEvent event) {
//        switch (v.getId()) {
//            case R.id.emailToLogin:
//                login_ed.setText("");
//                return false;
//            case R.id.senhaToLogin:
//                senha_ed.setText("");
//                return false;
//            default:
//                return false;
//        }
//    }

    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        if (hasFocus) {
            switch (v.getId()) {
                case R.id.emailToLogin:
                    login_ed.setText("");
                    break;
                case R.id.senhaToLogin:
                    senha_ed.setText("");
                    break;
                default:
                    break;
            }
        }
    }

    private class LoginAsyncTask extends AsyncTask<Void, Void, Boolean> {

        Context context;
        String email;
        String senha;
        Voluntario voluntario;

        public LoginAsyncTask(Context context, String email, String senha) {
            this.context = context;
            this.email = email;
            this.senha = senha;
        }

        protected void onPreExecute() {
            super.onPreExecute();
        }

        protected Boolean doInBackground(Void... params) {
            Response response;
            VoluntarioEndpoint service;
            try {
                VoluntarioEndpoint.Builder builder;
                builder = new VoluntarioEndpoint.Builder(AndroidHttp.newCompatibleTransport(), new GsonFactory(), null).setHttpRequestInitializer(new HttpRequestInitializer() {
                    @Override
                    public void initialize(HttpRequest request) throws IOException {
                        request.setConnectTimeout(60000);
                        request.setReadTimeout(60000);
                    }
                });
                builder.setApplicationName("Angelos");
                if (Constantes.IS_LOCAL_EXECUTION) {
                    builder.setRootUrl("http://10.0.2.2:8080/_ah/api/")
                            .setGoogleClientRequestInitializer(new GoogleClientRequestInitializer() {
                                @Override
                                public void initialize(AbstractGoogleClientRequest<?> abstractGoogleClientRequest) throws IOException {
                                    abstractGoogleClientRequest.setDisableGZipContent(true);
                                }
                            });
                }
                service = builder.build();
                voluntario = service.authenticateVoluntario(email, senha).execute();
                if (voluntario != null) {
                    return true;
                } else {
                    return false;
                }
            } catch (Exception e) {
                Log.d(TAG, e.getMessage());
                return false;
            }
        }

        protected void onPostExecute(Boolean value) {
//            if (onlyEmail) {
//                if (value) {
//                    assignGoogleIdNow = true;
//                    signIn();
//                } else {
//                    dismissProgressDialog();
//                    Toast.makeText(context, "Antes de usar o Login Google, se cadastre no sistema.", Toast.LENGTH_LONG).show();
//                }
//            } else {
                if (value) {
                    Database db = new Database(context);
                    db.open();
                    Database.mUserDao.addUser(voluntario, true, true, 1);
                    db.close();
//                    DatabaseHelper helper = new DatabaseHelper(context);
//                    SQLiteDatabase db = helper.getWritableDatabase();
//
//                    ContentValues values = new ContentValues();
//                    values.put("isCadastrado", 1);
//                    values.put("isLogado", 1);
//                    values.put("nome", voluntario.getNome());
//                    values.put("sobrenome", voluntario.getSNome());
//                    values.put("email", voluntario.getEmail());
//                    values.put("senha", voluntario.getEmail());
//                    if (voluntario.getTelefone() != null)
//                        values.put("telefone", voluntario.getTelefone());
//                    values.put("celular", voluntario.getCelular());
//                    values.put("dataNasc", String.valueOf(voluntario.getDataNasc()));
//                    db.update("usuario", values, null, null);
//
//                    db.close();
//                    helper.close();

                    ((Activity) context).finish();
                    startActivity(new Intent(this.context, TabActivity.class));
                } else {
                    dismissProgressDialog();
                    Toast.makeText(context, "Erro.", Toast.LENGTH_SHORT).show();
                }
        }

    }

//    private class AssignGoogleIdAsyncTask extends AsyncTask<Void, Void, Boolean> {
//
//        Context context;
//        String userId;
//
//        public AssignGoogleIdAsyncTask(Context context, String id) {
//            this.context = context;
//            this.userId = id;
//        }
//
//        protected void onPreExecute() {
//            super.onPreExecute();
//        }
//
//        protected Boolean doInBackground(Void... params) {
//            Response response;
//            VoluntarioEndpoint service;
//            try {
//                VoluntarioEndpoint.Builder builder;
//                builder = new VoluntarioEndpoint.Builder(AndroidHttp.newCompatibleTransport(), new GsonFactory(), null).setHttpRequestInitializer(new HttpRequestInitializer() {
//                    @Override
//                    public void initialize(HttpRequest request) throws IOException {
//                        request.setConnectTimeout(60000);
//                        request.setReadTimeout(60000);
//                    }
//                });
//                builder.setApplicationName("Angelos");
//                if (Constantes.IS_LOCAL_EXECUTION) {
//                    builder.setRootUrl("http://10.0.2.2:8080/_ah/api/")
//                            .setGoogleClientRequestInitializer(new GoogleClientRequestInitializer() {
//                                @Override
//                                public void initialize(AbstractGoogleClientRequest<?> abstractGoogleClientRequest) throws IOException {
//                                    abstractGoogleClientRequest.setDisableGZipContent(true);
//                                }
//                            });
//                }
//                service = builder.build();
//                response = service.setGoogleId(userEmail, userId).execute();
//                if (response != null) {
//                    return response.getBooleanResp();
//                } else {
//                    return false;
//                }
//            } catch (Exception e) {
//                Log.d(TAG, e.getMessage());
//                return false;
//            }
//        }
//
//        protected void onPostExecute(Boolean value) {
//            if (value) {
//                DatabaseHelper helper = new DatabaseHelper(context);
//                SQLiteDatabase db = helper.getWritableDatabase();
//
//                ContentValues values = new ContentValues();
//                values.put("isLogado", 1);
//                values.put("email", userEmail);
//                db.update("usuario", values, null, null);
//
//                db.close();
//                helper.close();
//
//                ((Activity) context).finish();
//                startActivity(new Intent(this.context, TabActivity.class));
//            } else {
//                dismissProgressDialog();
//                Toast.makeText(context, "Erro.", Toast.LENGTH_SHORT).show();
//            }
//        }
//
//    }

}
