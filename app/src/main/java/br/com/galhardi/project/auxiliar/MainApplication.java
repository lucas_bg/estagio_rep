package br.com.galhardi.project.auxiliar;

import android.app.Application;

import br.com.galhardi.project.localDB.Database;

public class MainApplication extends Application {

    public static final String TAG = MainApplication.class.getSimpleName();
    public static Database mDb;

    @Override
    public void onCreate() {
        super.onCreate();
        mDb = new Database(this);
        mDb.open();
    }

    @Override
    public void onTerminate() {
        mDb.close();
        super.onTerminate();
    }

}
