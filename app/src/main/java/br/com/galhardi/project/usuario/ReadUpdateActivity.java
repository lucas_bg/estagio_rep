package br.com.galhardi.project.usuario;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.api.client.extensions.android.http.AndroidHttp;
import com.google.api.client.googleapis.services.AbstractGoogleClientRequest;
import com.google.api.client.googleapis.services.GoogleClientRequestInitializer;
import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpRequestInitializer;
import com.google.api.client.json.gson.GsonFactory;
import com.google.api.client.util.DateTime;

import java.io.IOException;

import br.com.galhardi.project.R;
import br.com.galhardi.project.auxiliar.Constantes;
import br.com.galhardi.project.auxiliar.MyDateUtils;
import br.com.galhardi.project.auxiliar.TextValidator;
import br.com.galhardi.project.localDB.Database;
import br.com.galhardi.voluntarios.api.voluntarioEndpoint.VoluntarioEndpoint;
import br.com.galhardi.voluntarios.api.voluntarioEndpoint.model.Response;
import br.com.galhardi.voluntarios.api.voluntarioEndpoint.model.Voluntario;

//import br.com.galhardi.project.localDB.DatabaseHelper;

public class ReadUpdateActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = "ReadUpdateActivity";
    ProgressDialog progressDialog;
    private EditText cad_usu_nome;
    private EditText cad_usu_sobrenome;
    private EditText cad_usu_cel;
    private EditText cad_usu_tel;
    private EditText cad_usu_email;
    private EditText cad_usu_dt_nasc;
    private EditText cad_usu_senha;
    private EditText cad_usu_senha_confirm;
    private Voluntario voluntarioRetrieved;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.usuario_cadastro_actv);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setIcon(R.mipmap.logo_3);
        getSupportActionBar().setElevation(0);

        Button updateBtt = (Button) findViewById(R.id.bt_Cadastrar);
        updateBtt.setOnClickListener(this);
        updateBtt.setText("Atualizar");

        cad_usu_nome = (EditText) findViewById(R.id.cad_usu_nome_e);
        cad_usu_sobrenome = (EditText) findViewById(R.id.cad_usu_sobrenome_e);
        cad_usu_cel = (EditText) findViewById(R.id.cad_usu_cel_e);
        cad_usu_tel = (EditText) findViewById(R.id.cad_usu_tel_e);
        cad_usu_email = (EditText) findViewById(R.id.cad_usu_email_e);
        cad_usu_dt_nasc = (EditText) findViewById(R.id.cad_usu_dt_nasc_e);
        cad_usu_senha = (EditText) findViewById(R.id.cad_usu_senha_e);
        cad_usu_senha_confirm = (EditText) findViewById(R.id.cad_usu_senha_confirm_e);

        setValidators();

        showProgressDialog();
        new ReadUserByEmailAsynkTask(ReadUpdateActivity.this, getEmailFromLocalBD()).execute();
    }

    public Voluntario getVoluntarioFromFields() {
        Voluntario v = new Voluntario();

        v.setSenha(cad_usu_senha.getText().toString());
        v.setCelular(cad_usu_cel.getText().toString());
        v.setEmail(cad_usu_email.getText().toString());
        v.setNome(cad_usu_nome.getText().toString());
        v.setSNome(cad_usu_sobrenome.getText().toString());
        v.setTelefone(cad_usu_tel.getText().toString());
        String txteditdata_fund = cad_usu_dt_nasc.getText().toString().trim();
        if (MyDateUtils.isInCommonValidFormat(txteditdata_fund)) {
            String dt = MyDateUtils.commonStrDateToRFC3339StrDate(txteditdata_fund);
            v.setDataNasc(DateTime.parseRfc3339(dt));
        }
        v.setId(voluntarioRetrieved.getId());

        return v;
    }

    public String getEmailFromLocalBD() {
        String email;

        Database db = new Database(this);
        db.open();
        email = Database.mUserDao.getEmailFromLocalBD();
        db.close();

        return email;
    }

    private void showProgressDialog() {
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(this);
            progressDialog.setCancelable(false);
            progressDialog.setMessage(getString(R.string.loading));
            progressDialog.setIndeterminate(true);
        }

        progressDialog.show();
    }

    private void dismissProgressDialog() {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        dismissProgressDialog();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        dismissProgressDialog();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.bt_Cadastrar: {
                if (!isAllFieldsOk()) {
                    Toast.makeText(this, "Preencha todos os campos corretamente.", Toast.LENGTH_LONG).show();
                    return;
                }

                Voluntario voluntario = getVoluntarioFromFields();

                if (voluntario.getTelefone().equals("") || voluntario.getTelefone().length() == 0)
                    voluntario.setTelefone(null);

                showProgressDialog();
                new UpdateUserAsyncTask(ReadUpdateActivity.this, voluntario).execute();
                break;
            }
        }
    }

    public void setValidators() {
        cad_usu_nome.setError("Campo obrigatório.");
        cad_usu_nome.addTextChangedListener(new TextValidator(cad_usu_nome) {
            @Override
            public void validate(TextView textView, String text) {
                if (text.length() == 0) {
                    textView.setError("Campo obrigatório.");
                }
            }
        });
        cad_usu_sobrenome.setError("Campo obrigatório.");
        cad_usu_sobrenome.addTextChangedListener(new TextValidator(cad_usu_sobrenome) {
            @Override
            public void validate(TextView textView, String text) {
                if (text.length() == 0) {
                    textView.setError("Campo obrigatório.");
                }
            }
        });
        cad_usu_cel.setError("Campo obrigatório.");
        cad_usu_cel.addTextChangedListener(new TextValidator(cad_usu_cel) {
            @Override
            public void validate(TextView textView, String text) {
                if (text.length() == 0) {
                    textView.setError("Campo obrigatório.");
                }
            }
        });
        cad_usu_email.setError("Campo obrigatório.");
        cad_usu_email.addTextChangedListener(new TextValidator(cad_usu_email) {
            @Override
            public void validate(TextView textView, String text) {
                if (text.length() == 0) {
                    textView.setError("Campo obrigatório.");
                }
            }
        });
        cad_usu_dt_nasc.setError("Insira uma data válida. Campo obrigatório.");
        cad_usu_dt_nasc.addTextChangedListener(new TextValidator(cad_usu_dt_nasc) {
            @Override
            public void validate(TextView textView, String text) {
                if ((!MyDateUtils.isInCommonValidFormat(text)) || text.length() == 0) {
                    textView.setError("Insira uma data válida. Campo obrigatório.");
                }
            }
        });
        cad_usu_senha.setError("Campo obrigatório.");
        cad_usu_senha.addTextChangedListener(new TextValidator(cad_usu_senha) {
            @Override
            public void validate(TextView textView, String text) {
                if (text.length() == 0) {
                    textView.setError("Campo obrigatório.");
                }
            }
        });
        cad_usu_senha_confirm.setError("Campo obrigatório.");
        cad_usu_senha_confirm.addTextChangedListener(new TextValidator(cad_usu_senha_confirm) {
            @Override
            public void validate(TextView textView, String text) {
                if (text.length() == 0) {
                    textView.setError("Campo obrigatório.");
                } else {
                    String senha = cad_usu_senha.getText().toString().trim();
                    if (!text.equals(senha)) {
                        textView.setError("Senhas não estão iguais.");
                    }
                }
            }
        });
    }

    public void manualCheck() {
        String text = cad_usu_nome.getText().toString().trim();
        if (text.length() == 0)
            cad_usu_nome.setError("Campo obrigatório.");
        else
            cad_usu_nome.setError(null);

        text = cad_usu_sobrenome.getText().toString().trim();
        if (text.length() == 0)
            cad_usu_sobrenome.setError("Campo obrigatório.");
        else
            cad_usu_sobrenome.setError(null);

        text = cad_usu_cel.getText().toString().trim();
        if (text.length() == 0)
            cad_usu_cel.setError("Campo obrigatório.");
        else
            cad_usu_cel.setError(null);

        text = cad_usu_email.getText().toString().trim();
        if (text.length() == 0)
            cad_usu_email.setError("Campo obrigatório.");
        else
            cad_usu_email.setError(null);

        text = cad_usu_dt_nasc.getText().toString().trim();
        if ((!MyDateUtils.isInCommonValidFormat(text)) || text.length() == 0)
            cad_usu_dt_nasc.setError("Insira uma data válida. Campo obrigatório.");
        else
            cad_usu_dt_nasc.setError(null);

        text = cad_usu_senha.getText().toString().trim();
        if (text.length() == 0)
            cad_usu_senha.setError("Campo obrigatório.");
        else
            cad_usu_senha.setError(null);

        text = cad_usu_senha_confirm.getText().toString().trim();
        if (text.length() == 0)
            cad_usu_senha_confirm.setError("Campo obrigatório.");
        else {
            String senha = cad_usu_senha.getText().toString().trim();
            if (!text.equals(senha)) {
                cad_usu_senha_confirm.setError("Senhas não estão iguais.");
            }
        }
    }

    private boolean isAllFieldsOk() {
        boolean isOk = true;
        isOk = isOk && cad_usu_nome.getError() == null;
        isOk = isOk && cad_usu_sobrenome.getError() == null;
        isOk = isOk && cad_usu_cel.getError() == null;
        isOk = isOk && cad_usu_email.getError() == null;
        isOk = isOk && cad_usu_dt_nasc.getError() == null;
        isOk = isOk && cad_usu_senha.getError() == null;
        isOk = isOk && cad_usu_senha_confirm.getError() == null;
        return isOk;
    }

    private class ReadUserByEmailAsynkTask extends AsyncTask<Void, Void, Boolean> {

        Voluntario voluntario;
        private Context context;
        private String userEmail;

        public ReadUserByEmailAsynkTask(Context ct, String email) {
            this.context = ct;
            this.userEmail = email;
        }

        protected Boolean doInBackground(Void... params) {
            VoluntarioEndpoint service;
            try {
                VoluntarioEndpoint.Builder builder;
                builder = new VoluntarioEndpoint.Builder(AndroidHttp.newCompatibleTransport(), new GsonFactory(), null).setHttpRequestInitializer(new HttpRequestInitializer() {
                    @Override
                    public void initialize(HttpRequest request) throws IOException {
                        request.setConnectTimeout(60000);
                        request.setReadTimeout(60000);
                    }
                });
                builder.setApplicationName("Angelos");
                if (Constantes.IS_LOCAL_EXECUTION) {
                    builder.setRootUrl("http://10.0.2.2:8080/_ah/api/")
                            .setGoogleClientRequestInitializer(new GoogleClientRequestInitializer() {
                                @Override
                                public void initialize(AbstractGoogleClientRequest<?> abstractGoogleClientRequest) throws IOException {
                                    abstractGoogleClientRequest.setDisableGZipContent(true);
                                }
                            });
                }

                service = builder.build();
                voluntario = service.readUserbyEmail(userEmail).execute();

                return voluntario != null;
            } catch (Exception e) {
                Log.d(TAG, e.getMessage());
                return false;
            }
        }

        protected void onPostExecute(Boolean value) {
            if (value) {
                voluntarioRetrieved = new Voluntario();
                voluntarioRetrieved = voluntario.clone();
                cad_usu_nome.setText(voluntario.getNome());
                cad_usu_sobrenome.setText(voluntario.getSNome());
                cad_usu_cel.setText(voluntario.getCelular());
                cad_usu_tel.setText(voluntario.getTelefone());
                cad_usu_email.setText(voluntario.getEmail());
                String RFC3339StrDate = voluntario.getDataNasc().toString();
                cad_usu_dt_nasc.setText(MyDateUtils.RFC3339StrDateToCommonStrDate(RFC3339StrDate));

                cad_usu_nome.setBackgroundColor(Color.parseColor("#ffffcc"));
                cad_usu_sobrenome.setBackgroundColor(Color.parseColor("#ffffcc"));
                cad_usu_cel.setBackgroundColor(Color.parseColor("#ffffcc"));
                cad_usu_tel.setBackgroundColor(Color.parseColor("#ffffcc"));
                cad_usu_email.setBackgroundColor(Color.parseColor("#ffffcc"));
                cad_usu_dt_nasc.setBackgroundColor(Color.parseColor("#ffffcc"));
                cad_usu_senha.setBackgroundColor(Color.parseColor("#ffffcc"));
                cad_usu_senha_confirm.setBackgroundColor(Color.parseColor("#ffffcc"));
                manualCheck();
                dismissProgressDialog();
            } else {
                dismissProgressDialog();
                Toast.makeText(context, "Erro.", Toast.LENGTH_SHORT).show();
            }
        }

    }

    private class UpdateUserAsyncTask extends AsyncTask<Void, Void, Boolean> {

        Context context;
        Voluntario voluntario;

        public UpdateUserAsyncTask(Context ct, Voluntario v) {
            this.context = ct;
            this.voluntario = v;
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            VoluntarioEndpoint service;
            try {
                VoluntarioEndpoint.Builder builder;
                builder = new VoluntarioEndpoint.Builder(AndroidHttp.newCompatibleTransport(), new GsonFactory(), null).setHttpRequestInitializer(new HttpRequestInitializer() {
                    @Override
                    public void initialize(HttpRequest request) throws IOException {
                        request.setConnectTimeout(60000);
                        request.setReadTimeout(60000);
                    }
                });
                builder.setApplicationName("Angelos");
                if (Constantes.IS_LOCAL_EXECUTION) {
                    builder.setRootUrl("http://10.0.2.2:8080/_ah/api/")
                            .setGoogleClientRequestInitializer(new GoogleClientRequestInitializer() {
                                @Override
                                public void initialize(AbstractGoogleClientRequest<?> abstractGoogleClientRequest) throws IOException {
                                    abstractGoogleClientRequest.setDisableGZipContent(true);
                                }
                            });
                }
                service = builder.build();
                Response response = service.updateVoluntario(voluntario).execute();

                return response.getBooleanResp();
            } catch (Exception e) {
                Log.d(TAG, e.getMessage());
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean response) {
            dismissProgressDialog();
            if (response) {
                Toast.makeText(context, "Informações atualizadas com sucesso.", Toast.LENGTH_LONG).show();
                finish();
            } else {
                Toast.makeText(context, "Erro na atualização de informações.", Toast.LENGTH_LONG).show();
            }
        }

    }

}
