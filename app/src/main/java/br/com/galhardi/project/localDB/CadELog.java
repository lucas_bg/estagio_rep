package br.com.galhardi.project.localDB;

public class CadELog {

    private boolean isCadastrado;

    private boolean isLogado;

    public boolean isCadastrado() {
        return isCadastrado;
    }

    public void setCadastrado(boolean cadastrado) {
        isCadastrado = cadastrado;
    }

    public boolean isLogado() {
        return isLogado;
    }

    public void setLogado(boolean logado) {
        isLogado = logado;
    }

}
