package br.com.galhardi.project.quotes;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.api.client.extensions.android.http.AndroidHttp;
import com.google.api.client.googleapis.services.AbstractGoogleClientRequest;
import com.google.api.client.googleapis.services.GoogleClientRequestInitializer;
import com.google.api.client.json.gson.GsonFactory;

import java.io.IOException;

import br.com.galhardi.project.R;
import br.com.galhardi.project.auxiliar.Constantes;
import br.com.galhardi.quotes.api.quoteEndpoint.QuoteEndpoint;

public class QuoteDetailsActivity extends AppCompatActivity implements View.OnClickListener {

    public ProgressDialog progressDialog;
    private Integer quoteId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.quote_details);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setIcon(R.mipmap.logo_3);
        getSupportActionBar().setElevation(0);

        Intent intent = getIntent();

        TextView tvWhat = (TextView) findViewById(R.id.dtlWhat);
        TextView tvWho = (TextView) findViewById(R.id.dtlWho);
        Button bt_del = (Button) findViewById(R.id.quo_dtl_del_bt);
        bt_del.setOnClickListener(this);

        quoteId = intent.getIntExtra("id", -1);
        tvWhat.setText(intent.getStringExtra("what"));
        tvWho.setText(intent.getStringExtra("who"));
    }

    private void showProgressDialog() {
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(this);
            progressDialog.setCancelable(false);
            progressDialog.setMessage(getString(R.string.loading));
            progressDialog.setIndeterminate(true);
        }

        progressDialog.show();
    }

    private void dismissProgressDialog() {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.quo_dtl_del_bt: {
                if (quoteId != -1) {
                    Integer[] vv = {quoteId};
                    showProgressDialog();
                    new DeleteQuoteAsyncTask(QuoteDetailsActivity.this).execute(vv);
                } else {
                    Toast.makeText(this, "Erro.", Toast.LENGTH_SHORT).show();
                }
                break;
            }
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        dismissProgressDialog();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        dismissProgressDialog();
    }

    private class DeleteQuoteAsyncTask extends AsyncTask<Integer, Void, Boolean> {
        Context context;

        public DeleteQuoteAsyncTask(Context context) {
            this.context = context;
        }

        protected void onPreExecute() {
            super.onPreExecute();
        }

        protected Boolean doInBackground(Integer... params) {
            try {
                QuoteEndpoint.Builder builder;
                if (Constantes.IS_LOCAL_EXECUTION) {
                    builder = new QuoteEndpoint.Builder(AndroidHttp.newCompatibleTransport(), new GsonFactory(), null)
                            .setRootUrl("http://10.0.2.2:8080/_ah/api/")
                            .setGoogleClientRequestInitializer(new GoogleClientRequestInitializer() {
                                @Override
                                public void initialize(AbstractGoogleClientRequest<?> abstractGoogleClientRequest) throws IOException {
                                    abstractGoogleClientRequest.setDisableGZipContent(true);
                                }
                            });
                } else {
                    builder = new QuoteEndpoint.Builder(AndroidHttp.newCompatibleTransport(), new GsonFactory(), null);
                }
                QuoteEndpoint service = builder.build();
                service.removeQuote(params[0]).execute();
                return true;
            } catch (Exception e) {
                Log.d("Could not Del Quote", e.getMessage(), e);
                return false;
            }
        }

        protected void onPostExecute(Boolean r) {
            if (r) {
                ((Activity) context).finish();
                Toast.makeText(getBaseContext(), "Deletado com sucesso", Toast.LENGTH_SHORT).show();
            } else {
                dismissProgressDialog();
                Toast.makeText(getBaseContext(), "Falha ao deletar", Toast.LENGTH_SHORT).show();
            }
        }
    }

}
