package br.com.galhardi.project.quotes;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.api.client.extensions.android.http.AndroidHttp;
import com.google.api.client.googleapis.services.AbstractGoogleClientRequest;
import com.google.api.client.googleapis.services.GoogleClientRequestInitializer;
import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpRequestInitializer;
import com.google.api.client.json.gson.GsonFactory;

import java.io.IOException;

import br.com.galhardi.project.R;
import br.com.galhardi.project.auxiliar.Constantes;
import br.com.galhardi.quotes.api.quoteEndpoint.QuoteEndpoint;
import br.com.galhardi.quotes.api.quoteEndpoint.model.Quote;

public class AddQuoteActivity extends AppCompatActivity {

    EditText editAuthorName;
    EditText editMessage;
    Boolean updateMode;
    int idForUpdate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.quote_add_actv);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setIcon(R.mipmap.logo_3);
        getSupportActionBar().setElevation(0);

        // Update Case
        Intent intent = getIntent();
        updateMode = intent.hasExtra("UpdateMode") && intent.getBooleanExtra("UpdateMode", false);
        if (intent.hasExtra("idForUp"))
            idForUpdate = intent.getIntExtra("idForUp", -1);

        // Linking
        editAuthorName = (EditText) findViewById(R.id.editAuthorName);
        editMessage = (EditText) findViewById(R.id.editMessage);

        // Botão Adicionar
        Button btnAddQuote = (Button) findViewById(R.id.btnAddQuote);
        btnAddQuote.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                String txtAuthorName = editAuthorName.getText().toString().trim();
                String txtMessage = editMessage.getText().toString().trim();

                if ((txtAuthorName.length() == 0) || (txtMessage.length() == 0)) {
                    Toast.makeText(AddQuoteActivity.this, "You need to provide values for Author and Message", Toast.LENGTH_SHORT).show();
                } else {
                    if (!updateMode) {
                        String[] params = {txtAuthorName, txtMessage};
                        new AddQuoteAsyncTask(AddQuoteActivity.this).execute(params);
                    } else {
                        String[] params = {txtAuthorName, txtMessage, String.valueOf(idForUpdate)};
                        new UpdateQuoteAsyncTask(AddQuoteActivity.this).execute(params);
                    }
                }
            }
        });
    }

    private class AddQuoteAsyncTask extends AsyncTask<String, Void, Void> {

        Context context;
        private ProgressDialog pd;

        public AddQuoteAsyncTask(Context context) {
            this.context = context;
        }

        protected void onPreExecute(){
            super.onPreExecute();
            pd = new ProgressDialog(context);
            pd.setMessage("Adding the Quote...");
            pd.show();
        }

        protected Void doInBackground(String... params) {
            QuoteEndpoint service;
            try {
                QuoteEndpoint.Builder builder;
                builder = new QuoteEndpoint.Builder(AndroidHttp.newCompatibleTransport(), new GsonFactory(), null).setHttpRequestInitializer(new HttpRequestInitializer() {
                    @Override
                    public void initialize(HttpRequest request) throws IOException {
                        request.setConnectTimeout(60000);
                        request.setReadTimeout(60000);
                    }
                });
                builder.setApplicationName("Angelos");

                if (Constantes.IS_LOCAL_EXECUTION) {
                    builder.setRootUrl("http://10.0.2.2:8080/_ah/api/")
                            .setGoogleClientRequestInitializer(new GoogleClientRequestInitializer() {
                                @Override
                                public void initialize(AbstractGoogleClientRequest<?> abstractGoogleClientRequest) throws IOException {
                                    abstractGoogleClientRequest.setDisableGZipContent(true);
                                }
                            });
                }
                service = builder.build();
                Quote quote = new Quote();
                quote.setWho(params[0]);
                quote.setWhat(params[1]);
                service.insertQuote(quote).execute();
            } catch (Exception e) {
                Log.d("Could not Add Quote", e.getMessage(), e);
                setResult(Activity.RESULT_CANCELED);
            }
            return null;
        }

        protected void onPostExecute(Void aVoid) {
            pd.dismiss();
            editMessage.setText("");
            editAuthorName.setText("");

            setResult(Activity.RESULT_OK);
            finish();
        }

    }

    private class UpdateQuoteAsyncTask extends AsyncTask<String, Void, Void> {
        Context context;
        private ProgressDialog pd;

        public UpdateQuoteAsyncTask(Context context) {
            this.context = context;
        }

        protected void onPreExecute(){
            super.onPreExecute();
            pd = new ProgressDialog(context);
            pd.setMessage("updating the Quote...");
            pd.show();
        }

        protected Void doInBackground(String... params) {
            QuoteEndpoint service;
            try {
                QuoteEndpoint.Builder builder;
                builder = new QuoteEndpoint.Builder(AndroidHttp.newCompatibleTransport(), new GsonFactory(), null).setHttpRequestInitializer(new HttpRequestInitializer() {
                    @Override
                    public void initialize(HttpRequest request) throws IOException {
                        request.setConnectTimeout(60000);
                        request.setReadTimeout(60000);
                    }
                });
                builder.setApplicationName("Angelos");

                if (Constantes.IS_LOCAL_EXECUTION) {
                    builder.setRootUrl("http://10.0.2.2:8080/_ah/api/")
                            .setGoogleClientRequestInitializer(new GoogleClientRequestInitializer() {
                                @Override
                                public void initialize(AbstractGoogleClientRequest<?> abstractGoogleClientRequest) throws IOException {
                                    abstractGoogleClientRequest.setDisableGZipContent(true);
                                }
                            });
                }
                service = builder.build();
                Quote quote = new Quote();
                quote.setId(Integer.valueOf(params[2]));
                quote.setWho(params[0]);
                quote.setWhat(params[1]);
                service.updateQuote(quote).execute();
            } catch (Exception e) {
                Log.d("Could not Upd Quote", e.getMessage(), e);
                setResult(Activity.RESULT_CANCELED);
            }
            return null;
        }

        protected void onPostExecute(Void aVoid) {
            pd.dismiss();
            editMessage.setText("");
            editAuthorName.setText("");

            setResult(Activity.RESULT_OK);
            finish();
        }

    }

}
