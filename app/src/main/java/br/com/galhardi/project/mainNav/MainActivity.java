package br.com.galhardi.project.mainNav;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import br.com.galhardi.project.localDB.CadELog;
import br.com.galhardi.project.localDB.Database;
import br.com.galhardi.project.usuario.LoginActivity;

//import br.com.galhardi.project.localDB.DatabaseHelper;

public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

//        DatabaseHelper helper = new DatabaseHelper(this);
//        SQLiteDatabase db = helper.getReadableDatabase();
//        Cursor cursor = db.rawQuery("SELECT isLogado FROM users", null);
//        cursor.moveToFirst();
//        int valor = 0;
//        for (int i = 0; i < cursor.getCount(); i++) {
//            valor = cursor.getInt(0);
//            cursor.moveToNext();
//        }
//        cursor.close();
//        helper.close();
//
//        if (valor == 1) {
//            finish();
//            startActivity(new Intent(this, TabActivity.class));
//        } else {
//            finish();
//            startActivity(new Intent(this, LoginActivity.class));
//        }

        Database db = new Database(this);
        db.open();
        CadELog cel = Database.mUserDao.cadELogMet();
        db.close();
        if (cel.isLogado()) {
            finish();
            startActivity(new Intent(this, TabActivity.class));
        } else {
            finish();
            startActivity(new Intent(this, LoginActivity.class));
        }
    }

}
