package br.com.galhardi.project.instituicao;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.api.client.extensions.android.http.AndroidHttp;
import com.google.api.client.googleapis.services.AbstractGoogleClientRequest;
import com.google.api.client.googleapis.services.GoogleClientRequestInitializer;
import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpRequestInitializer;
import com.google.api.client.json.gson.GsonFactory;
import com.google.api.client.util.DateTime;

import java.io.IOException;

import br.com.galhardi.instituicao.api.instituicaoEndpoint.InstituicaoEndpoint;
import br.com.galhardi.instituicao.api.instituicaoEndpoint.model.Instituicao;
import br.com.galhardi.instituicao.api.instituicaoEndpoint.model.Response;
import br.com.galhardi.instituicao.api.instituicaoEndpoint.model.Voluntario;
import br.com.galhardi.project.R;
import br.com.galhardi.project.auxiliar.Constantes;
import br.com.galhardi.project.auxiliar.MyDateUtils;
import br.com.galhardi.project.auxiliar.TextValidator;
import br.com.galhardi.project.localDB.Database;
import br.com.galhardi.project.usuario.LoginActivity;

public class AddInstituicaoActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = "AddInstituicao";
    //POINT 5
    EditText editdata_fund;
    EditText editnumMem;
    EditText editnumFunc;
    EditText editdescricao_detalhada;
    EditText editwebsite;
    EditText editfacebook;
    EditText editretornar;
    EditText edittelefone;
    EditText editApresentacao;
    EditText editEmail;
    EditText editEndereco;
    EditText editNome;
    ImageView imgView;
    Boolean updateMode;
    Bitmap choosenImg;
    int idForUpdate;
    private int PICK_IMAGE_REQUEST = 1;

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {

            Uri uri = data.getData();

            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);

                ImageView imageView = (ImageView) findViewById(R.id.add_inst_img);

                Bitmap other = Constantes.getResizedBitmapLessThanMaxSize(bitmap, 50);

                choosenImg = other;
                imageView.setImageBitmap(other);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.instituicao_add_actv);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setIcon(R.mipmap.logo_3);
        getSupportActionBar().setElevation(0);

        // Update Case
        Intent intent = getIntent();
        updateMode = intent.hasExtra("UpdateMode") && intent.getBooleanExtra("UpdateMode", false);
        if (intent.hasExtra("idForUp"))
            idForUpdate = intent.getIntExtra("idForUp", -1);

        // POINT 2
        editdata_fund = (EditText) findViewById(R.id.editInstituicaodata_fund);
        editnumMem = (EditText) findViewById(R.id.editInstituicaonumMem);
        editnumFunc = (EditText) findViewById(R.id.editInstituicaonumFunc);
        editdescricao_detalhada = (EditText) findViewById(R.id.editInstituicaodescricao_detalhada);
        editwebsite = (EditText) findViewById(R.id.editInstituicaowebsite);
        editfacebook = (EditText) findViewById(R.id.editInstituicaofacebook);
        editretornar = (EditText) findViewById(R.id.editInstituicaoretornar);
        edittelefone = (EditText) findViewById(R.id.editInstituicaotelefone);
        editApresentacao = (EditText) findViewById(R.id.editInstituicaoApresentacao);
        editEmail = (EditText) findViewById(R.id.editInstituicaoEmail);
        editEndereco = (EditText) findViewById(R.id.editInstituicaoEndereco);
        editNome = (EditText) findViewById(R.id.editInstituicaoNome);
        Button btnAddInstituicao = (Button) findViewById(R.id.btnAddInstituicao);
        btnAddInstituicao.setOnClickListener(this);
        imgView = (ImageView) findViewById(R.id.add_inst_img);
        imgView.setOnClickListener(this);

        setValidators();

        // POINT 2.2
        if (updateMode) {
            btnAddInstituicao.setText("Atualizar Instituição");
            editApresentacao.setText(intent.getStringExtra("apresentacao"));
            editEmail.setText(intent.getStringExtra("email"));
            editEndereco.setText(intent.getStringExtra("endereco"));
            editNome.setText(intent.getStringExtra("nome"));
            editnumMem.setText(intent.getStringExtra("numMem"));
            editnumFunc.setText(intent.getStringExtra("numFunc"));
            editdescricao_detalhada.setText(intent.getStringExtra("descricao_detalhada"));
            editwebsite.setText(intent.getStringExtra("website"));
            editfacebook.setText(intent.getStringExtra("facebook"));
            editretornar.setText(intent.getStringExtra("retornar"));
            edittelefone.setText(intent.getStringExtra("telefone"));
            editdata_fund.setText(intent.getStringExtra("data_fund"));
            manualCheck();
        }
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnAddInstituicao: {
                // POINT 1
                String txtApresentacao = editApresentacao.getText().toString().trim();
                String txtEmail = editEmail.getText().toString().trim();
                String txtEndereco = editEndereco.getText().toString().trim();
                String txtNome = editNome.getText().toString().trim();
                String txteditnumMem = editnumMem.getText().toString().trim();
                String txteditnumFunc = editnumFunc.getText().toString().trim();
                String txteditdescricao_detalhada = editdescricao_detalhada.getText().toString().trim();
                String txteditwebsite = editwebsite.getText().toString().trim();
                String txteditfacebook = editfacebook.getText().toString().trim();
                String txteditretornar = editretornar.getText().toString().trim();
                String txtedittelefone = edittelefone.getText().toString().trim();
                String txteditdata_fund = editdata_fund.getText().toString().trim();

                if (!checkFields()) {
                    Toast.makeText(this, "Há campos incorretos.", Toast.LENGTH_SHORT).show();
                    break;
                }

                //POINT 1.1
                Instituicao instituicao = new Instituicao();
                instituicao.setApresentacao(txtApresentacao);
                instituicao.setNome(txtNome);
                instituicao.setEndereco(txtEndereco);
                instituicao.setEmail(txtEmail);
                instituicao.setNumMem(Integer.valueOf(txteditnumMem));
                instituicao.setNumFunc(Integer.valueOf(txteditnumFunc));
                instituicao.setDescricaoDetalhada(txteditdescricao_detalhada);
                instituicao.setWebsite(txteditwebsite);
                instituicao.setFacebook(txteditfacebook);
                instituicao.setRetornar(txteditretornar);
                instituicao.setTelefone(txtedittelefone);
                if (MyDateUtils.isInCommonValidFormat(txteditdata_fund)) {
                    String dt = MyDateUtils.commonStrDateToRFC3339StrDate(txteditdata_fund);
                    instituicao.setDataFund(DateTime.parseRfc3339(dt));
                }
                if (choosenImg != null)
                    instituicao.setImgStrTEMP(Constantes.BitMapToString(choosenImg));
                else
                    instituicao.setImgStrTEMP("");

                Instituicao instt = prepareFieldsBeforeInsert(instituicao);

                if (!updateMode) {
                    Database db = new Database(this);
                    db.open();
                    br.com.galhardi.voluntarios.api.voluntarioEndpoint.model.Voluntario vv = Database.mUserDao.fetchUserById(1);
                    db.close();
                    if (vv != null)
                        instt.setUserIdTEMP(vv.getId());
                    new AddInstituicaoAsyncTask(AddInstituicaoActivity.this, instt).execute();
                } else {
                    instt.setId(idForUpdate);
                    new UpdateInstituicaoAsyncTask(AddInstituicaoActivity.this, instt).execute();
                }
                break;
            }
            case R.id.add_inst_img: {
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Selecione a Imagem."), PICK_IMAGE_REQUEST);
                break;
            }
        }
    }

    public void setValidators() {
        editdata_fund.setError("Insira uma data válida. Campo obrigatório.");
        editdata_fund.addTextChangedListener(new TextValidator(editdata_fund) {
            @Override
            public void validate(TextView textView, String text) {
                if ((!MyDateUtils.isInCommonValidFormat(text)) || text.length() == 0) {
                    textView.setError("Insira uma data válida. Campo obrigatório.");
                }
            }
        });
        editnumMem.setError("Insira um número. Campo obrigatório.");
        editnumMem.addTextChangedListener(new TextValidator(editnumMem) {
            @Override
            public void validate(TextView textView, String text) {
                if ((!TextUtils.isDigitsOnly(text)) || text.length() == 0) {
                    textView.setError("Insira um número. Campo obrigatório.");
                }
            }
        });
        editnumFunc.setError("Insira um número. Campo obrigatório.");
        editnumFunc.addTextChangedListener(new TextValidator(editnumFunc) {
            @Override
            public void validate(TextView textView, String text) {
                if ((!TextUtils.isDigitsOnly(text)) || text.length() == 0) {
                    textView.setError("Insira um número. Campo obrigatório.");
                }
            }
        });
        editdescricao_detalhada.setError("De 1 até 5000 caracteres por favor. Campo obrigatório.");
        editdescricao_detalhada.addTextChangedListener(new TextValidator(editdescricao_detalhada) {
            @Override
            public void validate(TextView textView, String text) {
                if (text.length() == 0 || text.length() > 5000) {
                    textView.setError("De 1 até 5000 caracteres por favor. Campo obrigatório.");
                }
            }
        });
        edittelefone.setError("Campo obrigatório.");
        edittelefone.addTextChangedListener(new TextValidator(edittelefone) {
            @Override
            public void validate(TextView textView, String text) {
                if (text.length() == 0) {
                    textView.setError("Campo obrigatório.");
                }
            }
        });
        editApresentacao.setError("De 1 até 800 caracteres por favor. Campo obrigatório.");
        editApresentacao.addTextChangedListener(new TextValidator(editApresentacao) {
            @Override
            public void validate(TextView textView, String text) {
                if (text.length() == 0 || text.length() > 800) {
                    textView.setError("De 1 até 800 caracteres por favor. Campo obrigatório.");
                }
            }
        });
        editNome.setError("Campo obrigatório.");
        editNome.addTextChangedListener(new TextValidator(editNome) {
            @Override
            public void validate(TextView textView, String text) {
                if (text.length() == 0) {
                    textView.setError("Campo obrigatório.");
                }
            }
        });
    }

    private void manualCheck() {
        String text = editdata_fund.getText().toString().trim();
        if ((!MyDateUtils.isInCommonValidFormat(text)) || text.length() == 0)
            editdata_fund.setError("Insira uma data válida. Campo obrigatório.");
        else
            editdata_fund.setError(null);

        text = editnumMem.getText().toString().trim();
        if ((!TextUtils.isDigitsOnly(text)) || text.length() == 0)
            editnumMem.setError("Insira um número. Campo obrigatório.");
        else
            editnumMem.setError(null);

        text = editnumFunc.getText().toString().trim();
        if ((!TextUtils.isDigitsOnly(text)) || text.length() == 0)
            editnumFunc.setError("Insira um número. Campo obrigatório.");
        else
            editnumFunc.setError(null);

        text = editdescricao_detalhada.getText().toString().trim();
        if (text.length() == 0 || text.length() > 5000)
            editdescricao_detalhada.setError("De 1 até 5000 caracteres por favor. Campo obrigatório.");
        else
            editdescricao_detalhada.setError(null);

        text = edittelefone.getText().toString().trim();
        if (text.length() == 0)
            edittelefone.setError("Campo obrigatório.");
        else
            edittelefone.setError(null);

        text = editApresentacao.getText().toString().trim();
        if (text.length() == 0 || text.length() > 800)
            editApresentacao.setError("De 1 até 800 caracteres por favor. Campo obrigatório.");
        else
            editApresentacao.setError(null);

        text = editNome.getText().toString().trim();
        if (text.length() == 0)
            editNome.setError("Campo obrigatório.");
        else
            editNome.setError(null);
    }

    private boolean checkFields() {
        boolean isOk = true;
        isOk = isOk && editdata_fund.getError() == null;
        isOk = isOk && editnumMem.getError() == null;
        isOk = isOk && editnumFunc.getError() == null;
        isOk = isOk && editdescricao_detalhada.getError() == null;
        isOk = isOk && edittelefone.getError() == null;
        isOk = isOk && editApresentacao.getError() == null;
        isOk = isOk && editNome.getError() == null;
        return isOk;
    }

    private Instituicao prepareFieldsBeforeInsert(Instituicao i) {
        if (i.getEndereco().equals("") || i.getEndereco().length() == 0)
            i.setEndereco(null);
        if (i.getEmail().equals("") || i.getEmail().length() == 0)
            i.setEmail(null);
        if (i.getWebsite().equals("") || i.getWebsite().length() == 0)
            i.setWebsite(null);
        if (i.getFacebook().equals("") || i.getFacebook().length() == 0)
            i.setFacebook(null);
        if (i.getRetornar().equals("") || i.getRetornar().length() == 0)
            i.setRetornar(null);
        return i;
    }

    private class AddInstituicaoAsyncTask extends AsyncTask<Void, Void, Void> {

        Context context;
        Instituicao instituicao;
        private ProgressDialog pd;
        private Response response;
        private Voluntario voluntario;

        public AddInstituicaoAsyncTask(Context context, Instituicao instituicao) {
            this.context = context;
            this.instituicao = instituicao;
            this.response = new Response();
        }

        protected void onPreExecute() {
            super.onPreExecute();
            pd = new ProgressDialog(context);
            pd.setCancelable(false);
            pd.setMessage("Adicionando Instituição...");
            pd.show();
        }

        protected Void doInBackground(Void... params) {
            InstituicaoEndpoint service;
            try {
                InstituicaoEndpoint.Builder builder;
                builder = new InstituicaoEndpoint.Builder(AndroidHttp.newCompatibleTransport(), new GsonFactory(), null).setHttpRequestInitializer(new HttpRequestInitializer() {
                    @Override
                    public void initialize(HttpRequest request) throws IOException {
                        request.setConnectTimeout(60000);
                        request.setReadTimeout(60000);
                    }
                });
                builder.setApplicationName("Angelos");

                if (Constantes.IS_LOCAL_EXECUTION) {
                    builder.setRootUrl("http://10.0.2.2:8080/_ah/api/")
                            .setGoogleClientRequestInitializer(new GoogleClientRequestInitializer() {
                                @Override
                                public void initialize(AbstractGoogleClientRequest<?> abstractGoogleClientRequest) throws IOException {
                                    abstractGoogleClientRequest.setDisableGZipContent(true);
                                }
                            });
                }
                service = builder.build();
                voluntario = service.insertInstituicao(instituicao).execute();
                response.setBooleanResp(true);
            } catch (Exception e) {
                Log.d(TAG, "Erro no uso do Endpoint p adicionar." + e.getMessage(), e);
                response.setBooleanResp(false);
                setResult(Activity.RESULT_CANCELED);
            }
            return null;
        }

        protected void onPostExecute(Void aVoid) {
            pd.dismiss();

            if (response.getBooleanResp()) {
                Toast.makeText(context, "Instituição adicionada com sucesso.", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(context, "Inserção de nova instituição foi cancelada ou falhou.", Toast.LENGTH_SHORT).show();
            }

            Database db = new Database(context);
            db.open();
            Database.mUserDao.logoutInDB();
            Database.mUserDao.addUser(voluntario, true, true, 1);
            db.close();

            finish();
            startActivity(new Intent(AddInstituicaoActivity.this, LoginActivity.class));
        }

    }

    private class UpdateInstituicaoAsyncTask extends AsyncTask<Void, Void, Void> {

        Context context;
        Instituicao instituicao;
        private ProgressDialog pd;
        private Response response;

        public UpdateInstituicaoAsyncTask(Context context, Instituicao instituicao) {
            this.context = context;
            this.instituicao = instituicao;
            this.response = new Response();
        }

        protected void onPreExecute() {
            super.onPreExecute();
            pd = new ProgressDialog(context);
            pd.setCancelable(false);
            pd.setMessage("Atualizando Instituição...");
            pd.show();
        }

        protected Void doInBackground(Void... params) {
            InstituicaoEndpoint service;
            try {
                InstituicaoEndpoint.Builder builder;
                builder = new InstituicaoEndpoint.Builder(AndroidHttp.newCompatibleTransport(), new GsonFactory(), null).setHttpRequestInitializer(new HttpRequestInitializer() {
                    @Override
                    public void initialize(HttpRequest request) throws IOException {
                        request.setConnectTimeout(60000);
                        request.setReadTimeout(60000);
                    }
                });
                builder.setApplicationName("Angelos");

                if (Constantes.IS_LOCAL_EXECUTION) {
                    builder.setRootUrl("http://10.0.2.2:8080/_ah/api/")
                            .setGoogleClientRequestInitializer(new GoogleClientRequestInitializer() {
                                @Override
                                public void initialize(AbstractGoogleClientRequest<?> abstractGoogleClientRequest) throws IOException {
                                    abstractGoogleClientRequest.setDisableGZipContent(true);
                                }
                            });
                }
                service = builder.build();
                response = service.updateInstituicao(instituicao).execute();
            } catch (Exception e) {
                Log.d(TAG, "Erro no uso do Endpoint p atualizar." + e.getMessage(), e);
                response.setBooleanResp(false);
                setResult(Activity.RESULT_CANCELED);
            }
            return null;
        }

        protected void onPostExecute(Void aVoid) {
            pd.dismiss();

            if (response.getBooleanResp())
                setResult(Activity.RESULT_OK);
            else
                setResult(Activity.RESULT_CANCELED);

            finish();
        }

    }

}
