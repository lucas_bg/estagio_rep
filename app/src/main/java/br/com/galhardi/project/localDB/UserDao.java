package br.com.galhardi.project.localDB;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteConstraintException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.google.api.client.util.DateTime;

import br.com.galhardi.voluntarios.api.voluntarioEndpoint.model.Instituicao;
import br.com.galhardi.voluntarios.api.voluntarioEndpoint.model.Voluntario;

public class UserDao extends DbContentProvider implements IUserSchema, IUserDao {

    String COLUMN_ID = "_id";

    int DEFAULT_ID = 1;

    private ContentValues initialValues;

    public UserDao(SQLiteDatabase db) {
        super(db);
    }

    private static boolean intForBoolean(int value) {
        return value == 1;
    }

    private static String booleanForString(boolean value) {
        if (value)
            return "1";
        else
            return "0";
    }

    @Override
    public Voluntario fetchUserById(int userId) {
        final String selectionArgs[] = {String.valueOf(userId)};
        final String selection = COLUMN_ID + " = ?";
        Voluntario v = new Voluntario();
        Cursor cursor = super.query(USER_TABLE, USER_COLUMNS, selection,
                selectionArgs, COLUMN_ID);
        if (cursor != null) {
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                v = cursorToEntity(cursor);
                cursor.moveToNext();
            }
            cursor.close();
        }

        return v;
    }

    public boolean addUser(Voluntario user, boolean isCadastrado, boolean isLogado, int id) {
        setContentValue(user, isCadastrado, isLogado, id);
        try {
            return super.insert(USER_TABLE, getContentValue()) > 0;
        } catch (SQLiteConstraintException ex) {
            Log.w("Database", ex.getMessage());
            return false;
        }
    }

    public boolean addUser(br.com.galhardi.instituicao.api.instituicaoEndpoint.model.Voluntario user, boolean isCadastrado, boolean isLogado, int id) {
        setContentValue(user, isCadastrado, isLogado, id);
        try {
            return super.insert(USER_TABLE, getContentValue()) > 0;
        } catch (SQLiteConstraintException ex) {
            Log.w("Database", ex.getMessage());
            return false;
        }
    }

    @Override
    public boolean logoutInDB() {
        final String selectionArgs[] = {String.valueOf(DEFAULT_ID)};
        final String selection = COLUMN_ID + " = ?";
        try {
            int r = super.delete(USER_TABLE, selection, selectionArgs);
            return r > 0;
        } catch (SQLiteConstraintException ex) {
            Log.w("Database", ex.getMessage());
            return false;
        }
    }

    @Override
    public String getEmailFromLocalBD() {
        final String selectionArgs[] = {String.valueOf(DEFAULT_ID)};
        final String selection = COLUMN_ID + " = ?";
        String[] USER_COLUMNS2 = new String[]{
                COLUMN_EMAIL
        };

        String email = "";

        Cursor cursor = super.query(USER_TABLE, USER_COLUMNS2, selection,
                selectionArgs, COLUMN_ID);

        if (cursor != null) {
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                if (cursor.getColumnIndex(COLUMN_EMAIL) != -1) {
                    int temp1 = cursor.getColumnIndexOrThrow(COLUMN_EMAIL);
                    email = cursor.getString(temp1);
                }
                cursor.moveToNext();
            }
            cursor.close();
        }

        return email;
    }

    @Override
    public CadELog cadELogMet() {
        final String selectionArgs[] = {String.valueOf(DEFAULT_ID)};
        final String selection = COLUMN_ID + " = ?";
        String[] USER_COLUMNS2 = new String[]{
                COLUMN_IS_CADASTRADO,
                COLUMN_IS_LOGADO
        };
        CadELog cel = new CadELog();
        Cursor cursor = super.query(USER_TABLE, USER_COLUMNS2, selection,
                selectionArgs, COLUMN_ID);
        if (cursor != null) {
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                if (cursor.getColumnIndex(COLUMN_IS_CADASTRADO) != -1) {
                    int temp1 = cursor.getColumnIndexOrThrow(COLUMN_IS_CADASTRADO);
                    cel.setCadastrado(UserDao.intForBoolean(cursor.getInt(temp1)));
                }
                if (cursor.getColumnIndex(COLUMN_IS_LOGADO) != -1) {
                    int temp2 = cursor.getColumnIndexOrThrow(COLUMN_IS_LOGADO);
                    cel.setLogado(UserDao.intForBoolean(cursor.getInt(temp2)));
                    Log.d("USERDAO", "VALOR LOGADO: " + cel.isLogado());
                }
                cursor.moveToNext();
            }
            cursor.close();
        }

        return cel;
    }

    protected Voluntario cursorToEntity(Cursor cursor) {
        Voluntario v = new Voluntario();
        Instituicao i = new Instituicao();

        int idIndex;
        int userNameIndex;
        int emailIndex;
        int INT_COLUMN_IS_CADASTRADO;
        int INT_COLUMN_IS_LOGADO;
        int INT_COLUMN_NOME;
        int INT_COLUMN_SOBRENOME;
        int INT_COLUMN_SENHA;
        int INT_COLUMN_TELEFONE;
        int INT_COLUMN_CELULAR;
        int INT_COLUMN_RG;
        int INT_COLUMN_DATA_NASC;
        int INT_COLUMN_ID_GOOGLE;
        int INT_COLUMN_ID_USER;
        int INT_COLUMN_ID_INST;
        int INT_COLUMN_NUM_MEM;
        int INT_COLUMN_NUM_FUNC;
        int INT_COLUMN_IS_APROVADA;
        int INT_COLUMN_EMAIL_INST;
        int INT_COLUMN_TELEFONE_INST;
        int INT_COLUMN_NOME_INST;
        int INT_COLUMN_APRESENTACAO;
        int INT_COLUMN_ENDERECO;
        int INT_COLUMN_DESCRICAO;
        int INT_COLUMN_FACEBOOK;
        int INT_COLUMN_WEBSITE;
        int INT_COLUMN_RETORNAR;
        int INT_COLUMN_IS_MEM;
        int INT_COLUMN_IS_REP;
        int INT_COLUMN_DATA_FUND;

        if (cursor != null) {
            if (cursor.getColumnIndex(COLUMN_EMAIL) != -1) {
                emailIndex = cursor.getColumnIndexOrThrow(COLUMN_EMAIL);
                v.setEmail(cursor.getString(emailIndex));
            }
            if (cursor.getColumnIndex(COLUMN_NOME) != -1) {
                INT_COLUMN_NOME = cursor.getColumnIndexOrThrow(COLUMN_NOME);
                v.setNome(cursor.getString(INT_COLUMN_NOME));
            }
            if (cursor.getColumnIndex(COLUMN_SOBRENOME) != -1) {
                INT_COLUMN_SOBRENOME = cursor.getColumnIndexOrThrow(COLUMN_SOBRENOME);
                v.setSNome(cursor.getString(INT_COLUMN_SOBRENOME));
            }
            if (cursor.getColumnIndex(COLUMN_SENHA) != -1) {
                INT_COLUMN_SENHA = cursor.getColumnIndexOrThrow(COLUMN_SENHA);
                v.setSenha(cursor.getString(INT_COLUMN_SENHA));
            }
            if (cursor.getColumnIndex(COLUMN_TELEFONE) != -1) {
                INT_COLUMN_TELEFONE = cursor.getColumnIndexOrThrow(COLUMN_TELEFONE);
                v.setTelefone(cursor.getString(INT_COLUMN_TELEFONE));
            }
            if (cursor.getColumnIndex(COLUMN_CELULAR) != -1) {
                INT_COLUMN_CELULAR = cursor.getColumnIndexOrThrow(COLUMN_CELULAR);
                v.setCelular(cursor.getString(INT_COLUMN_CELULAR));
            }
            if (cursor.getColumnIndex(COLUMN_DATA_NASC) != -1) {
                INT_COLUMN_DATA_NASC = cursor.getColumnIndexOrThrow(COLUMN_DATA_NASC);

                String str = cursor.getString(INT_COLUMN_DATA_NASC);
                if (str != null)
                    v.setDataNasc(DateTime.parseRfc3339(str));
                else
                    v.setDataNasc(null);
            }
            if (cursor.getColumnIndex(COLUMN_ID_USER) != -1) {
                INT_COLUMN_ID_USER = cursor.getColumnIndexOrThrow(COLUMN_ID_USER);
                v.setId(cursor.getInt(INT_COLUMN_ID_USER));
            }
            if (cursor.getColumnIndex(COLUMN_IS_MEM) != -1) {
                INT_COLUMN_IS_MEM = cursor.getColumnIndexOrThrow(COLUMN_IS_MEM);
                v.setIsMemInst(UserDao.intForBoolean(cursor.getInt(INT_COLUMN_IS_MEM)));
            }
            if (cursor.getColumnIndex(COLUMN_IS_REP) != -1) {
                INT_COLUMN_IS_REP = cursor.getColumnIndexOrThrow(COLUMN_IS_REP);
                v.setIsRepInst(UserDao.intForBoolean(cursor.getInt(INT_COLUMN_IS_REP)));
            }
            if (cursor.getColumnIndex(COLUMN_ID_INST) != -1) {
                INT_COLUMN_ID_INST = cursor.getColumnIndexOrThrow(COLUMN_ID_INST);
                if (!cursor.isNull(INT_COLUMN_ID_INST)) {
                    i.setId(cursor.getInt(INT_COLUMN_ID_INST));
                    if (cursor.getColumnIndex(COLUMN_NUM_MEM) != -1) {
                        INT_COLUMN_NUM_MEM = cursor.getColumnIndexOrThrow(COLUMN_NUM_MEM);
                        i.setNumMem(cursor.getInt(INT_COLUMN_NUM_MEM));
                    }
                    if (cursor.getColumnIndex(COLUMN_NUM_FUNC) != -1) {
                        INT_COLUMN_NUM_FUNC = cursor.getColumnIndexOrThrow(COLUMN_NUM_FUNC);
                        i.setNumFunc(cursor.getInt(INT_COLUMN_NUM_FUNC));
                    }
                    if (cursor.getColumnIndex(COLUMN_IS_APROVADA) != -1) {
                        INT_COLUMN_IS_APROVADA = cursor.getColumnIndexOrThrow(COLUMN_IS_APROVADA);
                        i.setAprovada(UserDao.intForBoolean(cursor.getInt(INT_COLUMN_IS_APROVADA)));
                    }
                    if (cursor.getColumnIndex(COLUMN_EMAIL_INST) != -1) {
                        INT_COLUMN_EMAIL_INST = cursor.getColumnIndexOrThrow(COLUMN_EMAIL_INST);
                        i.setEmail(cursor.getString(INT_COLUMN_EMAIL_INST));
                    }
                    if (cursor.getColumnIndex(COLUMN_TELEFONE_INST) != -1) {
                        INT_COLUMN_TELEFONE_INST = cursor.getColumnIndexOrThrow(COLUMN_TELEFONE_INST);
                        i.setTelefone(cursor.getString(INT_COLUMN_TELEFONE_INST));
                    }
                    if (cursor.getColumnIndex(COLUMN_NOME_INST) != -1) {
                        INT_COLUMN_NOME_INST = cursor.getColumnIndexOrThrow(COLUMN_NOME_INST);
                        i.setNome(cursor.getString(INT_COLUMN_NOME_INST));
                    }
                    if (cursor.getColumnIndex(COLUMN_APRESENTACAO) != -1) {
                        INT_COLUMN_APRESENTACAO = cursor.getColumnIndexOrThrow(COLUMN_APRESENTACAO);
                        i.setApresentacao(cursor.getString(INT_COLUMN_APRESENTACAO));
                    }
                    if (cursor.getColumnIndex(COLUMN_ENDERECO) != -1) {
                        INT_COLUMN_ENDERECO = cursor.getColumnIndexOrThrow(COLUMN_ENDERECO);
                        i.setEndereco(cursor.getString(INT_COLUMN_ENDERECO));
                    }
                    if (cursor.getColumnIndex(COLUMN_DESCRICAO) != -1) {
                        INT_COLUMN_DESCRICAO = cursor.getColumnIndexOrThrow(COLUMN_DESCRICAO);
                        i.setDescricaoDetalhada(cursor.getString(INT_COLUMN_DESCRICAO));
                    }
                    if (cursor.getColumnIndex(COLUMN_FACEBOOK) != -1) {
                        INT_COLUMN_FACEBOOK = cursor.getColumnIndexOrThrow(COLUMN_FACEBOOK);
                        i.setFacebook(cursor.getString(INT_COLUMN_FACEBOOK));
                    }
                    if (cursor.getColumnIndex(COLUMN_WEBSITE) != -1) {
                        INT_COLUMN_WEBSITE = cursor.getColumnIndexOrThrow(COLUMN_WEBSITE);
                        i.setWebsite(cursor.getString(INT_COLUMN_WEBSITE));
                    }
                    if (cursor.getColumnIndex(COLUMN_RETORNAR) != -1) {
                        INT_COLUMN_RETORNAR = cursor.getColumnIndexOrThrow(COLUMN_RETORNAR);
                        i.setRetornar(cursor.getString(INT_COLUMN_RETORNAR));
                    }
                    if (cursor.getColumnIndex(COLUMN_DATA_FUND) != -1) {
                        INT_COLUMN_DATA_FUND = cursor.getColumnIndexOrThrow(COLUMN_DATA_FUND);

                        String str = cursor.getString(INT_COLUMN_DATA_FUND);
                        if (str != null)
                            i.setDataFund(DateTime.parseRfc3339(str));
                        else
                            i.setDataFund(null);
                    }
                } else {
                    i = null;
                }
            }
        }
        v.setInstAss(i);
        return v;
    }

    private void setContentValue(Voluntario v, boolean isCadastrado, boolean isLogado, int id) {
        if (v == null) {
            Log.d("PORRA", "NAO MANDA ESSA PORRA NULL AQUI CARAI");
            return;
        }
        Instituicao i = v.getInstAss();
        initialValues = new ContentValues();

        initialValues.put(COLUMN_ID, id);
        initialValues.put(COLUMN_IS_CADASTRADO, isCadastrado);
        initialValues.put(COLUMN_IS_LOGADO, isLogado);

        if (v.getDataNasc() != null) {
            String str = v.getDataNasc().toString();
            initialValues.put(COLUMN_DATA_NASC, str);
        } else {
            initialValues.put(COLUMN_DATA_NASC, (String) null);
        }

        initialValues.put(COLUMN_ID_USER, v.getId());
        initialValues.put(COLUMN_CELULAR, v.getCelular());
        initialValues.put(COLUMN_EMAIL, v.getEmail());
        initialValues.put(COLUMN_NOME, v.getNome());
        initialValues.put(COLUMN_SOBRENOME, v.getSNome());
        initialValues.put(COLUMN_SENHA, v.getSenha());
        initialValues.put(COLUMN_TELEFONE, v.getTelefone());
        initialValues.put(COLUMN_IS_REP, v.getIsRepInst());
        initialValues.put(COLUMN_IS_MEM, v.getIsMemInst());

        if (i != null) {
            if (i.getDataFund() != null) {
                String str2 = i.getDataFund().toString();
                initialValues.put(COLUMN_DATA_FUND, str2);
            } else {
                initialValues.put(COLUMN_DATA_FUND, (String) null);
            }
            initialValues.put(COLUMN_ID_INST, i.getId());
            initialValues.put(COLUMN_NUM_MEM, i.getNumMem());
            initialValues.put(COLUMN_NUM_FUNC, i.getNumFunc());
            initialValues.put(COLUMN_IS_APROVADA, i.getAprovada());
            initialValues.put(COLUMN_EMAIL_INST, i.getEmail());
            initialValues.put(COLUMN_TELEFONE_INST, i.getTelefone());
            initialValues.put(COLUMN_NOME_INST, i.getNome());
            initialValues.put(COLUMN_APRESENTACAO, i.getApresentacao());
            initialValues.put(COLUMN_ENDERECO, i.getEndereco());
            initialValues.put(COLUMN_DESCRICAO, i.getDescricaoDetalhada());
            initialValues.put(COLUMN_FACEBOOK, i.getFacebook());
            initialValues.put(COLUMN_WEBSITE, i.getWebsite());
            initialValues.put(COLUMN_RETORNAR, i.getRetornar());
        }

    }

    private void setContentValue(br.com.galhardi.instituicao.api.instituicaoEndpoint.model.Voluntario v, boolean isCadastrado, boolean isLogado, int id) {
        if (v == null) {
            Log.d("PORRA", "NAO MANDA ESSA PORRA NULL AQUI CARAI");
            return;
        }
        br.com.galhardi.instituicao.api.instituicaoEndpoint.model.Instituicao i = v.getInstAss();
        initialValues = new ContentValues();

        initialValues.put(COLUMN_ID, id);
        initialValues.put(COLUMN_IS_CADASTRADO, isCadastrado);
        initialValues.put(COLUMN_IS_LOGADO, isLogado);

        if (v.getDataNasc() != null) {
            String str = v.getDataNasc().toString();
            initialValues.put(COLUMN_DATA_NASC, str);
        } else {
            initialValues.put(COLUMN_DATA_NASC, (String) null);
        }

        initialValues.put(COLUMN_ID_USER, v.getId());
        initialValues.put(COLUMN_CELULAR, v.getCelular());
        initialValues.put(COLUMN_EMAIL, v.getEmail());
        initialValues.put(COLUMN_NOME, v.getNome());
        initialValues.put(COLUMN_SOBRENOME, v.getSNome());
        initialValues.put(COLUMN_SENHA, v.getSenha());
        initialValues.put(COLUMN_TELEFONE, v.getTelefone());
        initialValues.put(COLUMN_IS_REP, v.getIsRepInst());
        initialValues.put(COLUMN_IS_MEM, v.getIsMemInst());

        if (i != null) {
            if (i.getDataFund() != null) {
                String str2 = i.getDataFund().toString();
                initialValues.put(COLUMN_DATA_FUND, str2);
            } else {
                initialValues.put(COLUMN_DATA_FUND, (String) null);
            }
            initialValues.put(COLUMN_ID_INST, i.getId());
            initialValues.put(COLUMN_NUM_MEM, i.getNumMem());
            initialValues.put(COLUMN_NUM_FUNC, i.getNumFunc());
            initialValues.put(COLUMN_IS_APROVADA, i.getAprovada());
            initialValues.put(COLUMN_EMAIL_INST, i.getEmail());
            initialValues.put(COLUMN_TELEFONE_INST, i.getTelefone());
            initialValues.put(COLUMN_NOME_INST, i.getNome());
            initialValues.put(COLUMN_APRESENTACAO, i.getApresentacao());
            initialValues.put(COLUMN_ENDERECO, i.getEndereco());
            initialValues.put(COLUMN_DESCRICAO, i.getDescricaoDetalhada());
            initialValues.put(COLUMN_FACEBOOK, i.getFacebook());
            initialValues.put(COLUMN_WEBSITE, i.getWebsite());
            initialValues.put(COLUMN_RETORNAR, i.getRetornar());
        }

    }

    private void setContentValue2(boolean isCadastrado, boolean isLogado, int id) {
        initialValues = new ContentValues();

        initialValues.put(COLUMN_ID, id);
        initialValues.put(COLUMN_IS_CADASTRADO, isCadastrado);
        initialValues.put(COLUMN_IS_LOGADO, isLogado);
    }

    private ContentValues getContentValue() {
        return initialValues;
    }

}
