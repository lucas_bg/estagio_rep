package br.com.galhardi.project;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.api.client.util.IOUtils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

public class ZTempPageFragment extends Fragment implements View.OnClickListener {

    private static final String ARG_PAGE_NUMBER = "page_number";

    public ZTempPageFragment() {

    }

    public static ZTempPageFragment newInstance(int page) {
        ZTempPageFragment fragment = new ZTempPageFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_PAGE_NUMBER, page);
        fragment.setArguments(args);
        return fragment;
    }

    public static File stream2file(InputStream in) throws IOException {
        final File tempFile = File.createTempFile("okkk", null);
        tempFile.deleteOnExit();
        FileOutputStream out = new FileOutputStream(tempFile);
        IOUtils.copy(in, out);
        return tempFile;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.ztemp_fragment_page_layout, container, false);

//        TextView txt = (TextView) rootView.findViewById(R.id.page_number_label);
//        rootView.findViewById(R.id.botao_get_arquivo).setOnClickListener(this);
//        rootView.findViewById(R.id.botao_upload_arquivo).setOnClickListener(this);
        int page = getArguments().getInt(ARG_PAGE_NUMBER, -1);
//        txt.setText(String.format(Locale.getDefault(), "Page %d", page));

        return rootView;
    }

    @Override
    public void onClick(View v) {

    }

}
