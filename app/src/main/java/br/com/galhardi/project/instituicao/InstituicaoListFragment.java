package br.com.galhardi.project.instituicao;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.api.client.extensions.android.http.AndroidHttp;
import com.google.api.client.googleapis.services.AbstractGoogleClientRequest;
import com.google.api.client.googleapis.services.GoogleClientRequestInitializer;
import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpRequestInitializer;
import com.google.api.client.json.gson.GsonFactory;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.util.List;

import br.com.galhardi.instituicao.api.instituicaoEndpoint.InstituicaoEndpoint;
import br.com.galhardi.instituicao.api.instituicaoEndpoint.model.Instituicao;
import br.com.galhardi.instituicao.api.instituicaoEndpoint.model.InstituicaoCollection;
import br.com.galhardi.project.R;
import br.com.galhardi.project.auxiliar.ConnectionDetector;
import br.com.galhardi.project.auxiliar.Constantes;
import br.com.galhardi.project.auxiliar.RecyclerItemClickListener;

public class InstituicaoListFragment extends Fragment {

    private static final String TAG = "InstituicaoList";
    public final int REQUEST_CODE_ADD_INSTITUICAO = 3;
    public final int REQUEST_CODE_READ_INSTITUICAO = 4;

    public int contador;
    InstAdapter instAdapter;
    RecyclerView instRecView;
    SwipeRefreshLayout swipeContainer;
    View rootView;

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUEST_CODE_ADD_INSTITUICAO: {
                if (resultCode == Activity.RESULT_OK) {
                    new InstituicaoListAsyncTask(getActivity()).execute();
                    Toast.makeText(getContext(), "Instituição adicionada com sucesso.", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getContext(), "Inserção de nova instituição foi cancelada ou falhou.", Toast.LENGTH_SHORT).show();
                }
                break;
            }
            case REQUEST_CODE_READ_INSTITUICAO: {
                if (resultCode == Activity.RESULT_OK) {
                    new InstituicaoListAsyncTask(getActivity()).execute();
                }
                break;
            }
            default: {
                break;
            }
        }
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.inst_list_reciew, container, false);

//        FloatingActionButton addButton = (FloatingActionButton) rootView.findViewById(R.id.add_inst_floatbt);
//        addButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent = new Intent(getActivity(), AddInstituicaoActivity.class);
//                startActivityForResult(intent, REQUEST_CODE_ADD_INSTITUICAO);
//            }
//        });

        swipeContainer = (SwipeRefreshLayout) rootView.findViewById(R.id.swipeContainerInst);
        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                new InstituicaoListAsyncTask(getActivity()).execute();
            }
        });
        swipeContainer.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);

        instRecView = (RecyclerView) rootView.findViewById(R.id.instListRecView);
        instRecView.setLayoutManager(new LinearLayoutManager(getActivity()));
        instRecView.addOnItemTouchListener(
                new RecyclerItemClickListener(getContext(), instRecView, new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        //POINT 9
                        Intent intent = new Intent(view.getContext(), InstituicaoDetailsActivity.class);
                        Instituicao instituicao = instAdapter.getInstByPosition(position);
                        intent.putExtra("id", instituicao.getId());
                        intent.putExtra("nome", instituicao.getNome());
                        intent.putExtra("apresentacao", instituicao.getApresentacao());
                        intent.putExtra("endereco", instituicao.getEndereco());
                        intent.putExtra("email", instituicao.getEmail());
                        intent.putExtra("numMem", instituicao.getNumMem().toString());
                        intent.putExtra("numFunc", instituicao.getNumFunc().toString());
                        intent.putExtra("descricao_detalhada", instituicao.getDescricaoDetalhada());
                        intent.putExtra("website", instituicao.getWebsite());
                        intent.putExtra("facebook", instituicao.getFacebook());
                        intent.putExtra("retornar", instituicao.getRetornar());
                        intent.putExtra("telefone", instituicao.getTelefone());
                        intent.putExtra("data_fund", instituicao.getDataFund().toString());

                        startActivityForResult(intent, REQUEST_CODE_READ_INSTITUICAO);
                    }

                    @Override
                    public void onLongItemClick(View view, int position) {
                    }
                })
        );

        contador = 0;
        ConnectionDetector cd = new ConnectionDetector(getContext());
        if (cd.isConnectingToInternet()) {
            new InstituicaoListAsyncTask(getActivity()).execute();
        } else {
            Toast.makeText(getContext(), "Sem conexão com a Internet", Toast.LENGTH_LONG).show();
        }

        return rootView;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        //POINT 11
        protected TextView instNome;
        protected TextView instApresentacao;
        protected TextView instEmail;
        protected TextView instEndereco;
        protected TextView instId;
        protected ImageView instImgExm;
        protected TextView instnumMem;
        protected TextView instnumFunc;
        protected TextView instdescricao_detalhada;
        protected TextView instwebsite;
        protected TextView instfacebook;
        protected TextView instretornar;
        protected TextView insttelefone;
        protected TextView instdata_fund;

        private ViewHolder(View itemView) {
            super(itemView);

            //POINT 10

            instImgExm = (ImageView) itemView.findViewById(R.id.inst_imgExm);

            instNome = (TextView) itemView.findViewById(R.id.inst_nome);
            instApresentacao = (TextView) itemView.findViewById(R.id.inst_apresentacao);
//            instEmail = (TextView) itemView.findViewById(R.id.inst_email);
//            instEndereco = (TextView) itemView.findViewById(R.id.inst_endereco);
//            instId = (TextView) itemView.findViewById(R.id.inst_id);
//            instnumMem = (TextView) itemView.findViewById(R.id.inst_numMem);
//            instnumFunc = (TextView) itemView.findViewById(R.id.inst_numFunc);
//            instdescricao_detalhada = (TextView) itemView.findViewById(R.id.inst_descricao_detalhada);
//            instwebsite = (TextView) itemView.findViewById(R.id.inst_website);
//            instfacebook = (TextView) itemView.findViewById(R.id.inst_facebook);
//            instretornar = (TextView) itemView.findViewById(R.id.inst_retornar);
//            insttelefone = (TextView) itemView.findViewById(R.id.inst_telefone);
//            instdata_fund = (TextView) itemView.findViewById(R.id.inst_data_fund);
        }

    }

    public static class InstAdapter extends RecyclerView.Adapter<ViewHolder> {

        private List<Instituicao> mInsts;
        private Context mContext;

        public InstAdapter(List<Instituicao> instituicoes, Context context) {
            mInsts = instituicoes;
            mContext = context;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater
                    .from(parent.getContext())
                    .inflate(R.layout.inst_item_in_list, parent, false);
            return new ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, final int position) {
            Instituicao inst = mInsts.get(position);

            Picasso.with(mContext)
                    .load(Constantes.WEB_APPLICATION_ADDRESS + "/instituicao/getImg?id=" + inst.getId())
                    .fit()
                    .placeholder(R.mipmap.default_img)
                    .centerInside()
                    .into(holder.instImgExm);

            //POINT 8
            holder.instNome.setText(inst.getNome());
            holder.instApresentacao.setText(inst.getApresentacao());
//            holder.instEmail.setText(inst.getEmail());
//            holder.instEndereco.setText(inst.getEndereco());
//            holder.instId.setText(Integer.toString(inst.getId()));
//            holder.instnumMem.setText(inst.getNumMem());
//            holder.instnumFunc.setText(inst.getNumFunc());
//            holder.instdescricao_detalhada.setText(inst.getDescricao_detalhada());
//            holder.instwebsite.setText(inst.getWebsite());
//            holder.instfacebook.setText(inst.getFacebook());
//            holder.instretornar.setText(inst.getRetornar());
//            holder.insttelefone.setText(inst.getTelefone());
//            holder.instdata_fund.setText(inst.getData_fund());
        }

        @Override
        public int getItemCount() {
            return (null != mInsts ? mInsts.size() : 0);
        }

        public Instituicao getInstByPosition(int position) {
            return mInsts.get(position);
        }

    }

    private class InstituicaoListAsyncTask extends AsyncTask<Void, Void, List<Instituicao>> {

        Context context;

        public InstituicaoListAsyncTask(Context context) {
            this.context = context;
        }

        protected void onPreExecute() {
            super.onPreExecute();
        }

        protected List<Instituicao> doInBackground(Void... unused) {
            InstituicaoCollection instituicoes;
            List<Instituicao> instituicaoList = null;
            InstituicaoEndpoint service;
            try {
                InstituicaoEndpoint.Builder builder;
                builder = new InstituicaoEndpoint.Builder(AndroidHttp.newCompatibleTransport(), new GsonFactory(), null).setHttpRequestInitializer(new HttpRequestInitializer() {
                    @Override
                    public void initialize(HttpRequest request) throws IOException {
                        request.setConnectTimeout(60000);
                        request.setReadTimeout(60000);
                    }
                });
                builder.setApplicationName("Angelos");

                if (Constantes.IS_LOCAL_EXECUTION) {
                    builder.setRootUrl("http://10.0.2.2:8080/_ah/api/")
                            .setGoogleClientRequestInitializer(new GoogleClientRequestInitializer() {
                                @Override
                                public void initialize(AbstractGoogleClientRequest<?> abstractGoogleClientRequest) throws IOException {
                                    abstractGoogleClientRequest.setDisableGZipContent(true);
                                }
                            });
                }
                service = builder.build();
                instituicoes = service.listInstituicao().execute();
                if (instituicoes != null)
                    instituicaoList = instituicoes.getItems();
            } catch (Exception e) {
                Log.d(TAG, "Erro no uso do Endpoint p listar." + e.getMessage(), e);
            }
            return instituicaoList;
        }

        protected void onPostExecute(List<Instituicao> instituicoes) {
            if (instituicoes == null) {
                contador++;
                if (contador < 5)
                    new InstituicaoListAsyncTask(getActivity()).execute();
            } else {
                instAdapter = new InstAdapter(instituicoes, context);
                instRecView.setAdapter(instAdapter);
                instAdapter.notifyDataSetChanged();
                swipeContainer.setRefreshing(false);
            }
        }

    }

}
